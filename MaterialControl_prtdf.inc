;
RECORD_FORMAT
;
;
;; External Supply Data - Purchase Orders, Stock Information etc.
; ----------------------------------------------------------------
Supply,ASSOCIATE (Orders),0,5000:
   Number,-1,INTEGER,
      PRIMARY KEY
      DIALOG ONLY
      HIDDEN
      SUPPLY TABLE:
   ID,-1,STRING,
      ACC_IO(VW_IO_FORNECIMENTO,id_registro,NOT_RELATION,UPDATE)
      FREE FORMAT 
      DIALOG ONLY
      UNIQUE
      FILTER
      LOCATE
      SORT DISPLAY:
   Order No.,-1,STRING,
      ACC_IO(VW_IO_FORNECIMENTO,ordem_num,NOT_RELATION,UPDATE)
      HELPPOPUPID (25400) 
      FREE FORMAT
      SORT DISPLAY
      LOCATE
      FILTER
	  HEADER TIP DISPLAY
      MATERIAL CONTROL ORDER NO:
   Order Type,1,STRING,
      ACC_IO(VW_IO_FORNECIMENTO,tipo,NOT_RELATION,UPDATE)
      HELPPOPUPID (25410)
      DATABASE(Order Types(Name))
      EXCLUDE "(({#Number}==0)||({#Number}==2))" 
      FILTER
      LOCATE
      ORDER TYPE:
   Material Source,-1,STRING,
      DATABASE(Material Source View(Name))
      ACC_IO(VW_IO_FORNECIMENTO,identificador_suprimento,NOT_RELATION,UPDATE)
      LOCATE
      FILTER
      ALLOW UNSPECIFIED
	  HEADER TIP DISPLAY:
   Company,-1,STRING,
      HELPPOPUPID (25420)
      FREE FORMAT
      ALLOW UNSPECIFIED
      FILTER
	  HIDDEN
      LOCATE
      HEADER TIP DISPLAY:
   Part No.,-1,STRING,
      ACC_IO(VW_IO_FORNECIMENTO,item_codigo,NOT_RELATION,UPDATE)
      HELPPOPUPID (25430)
      FREE FORMAT
      FILTER
      SORT DISPLAY
      LOCATE
      HEADER TIP DISPLAY
      MATERIAL CONTROL PART NO:
   Description,-1,STRING,
      ACC_IO(VW_IO_FORNECIMENTO,descricao_material,NOT_RELATION,UPDATE)
      FILTER
      FREE FORMAT:
   Supply Date,-1,TIME,
      ACC_IO(VW_IO_FORNECIMENTO,data,NOT_RELATION,UPDATE)
      HELPPOPUPID (25450)
      SUPPLY DATE
      GLOBAL ASSIGN
      SORT DISPLAY
      SELECTIVE ASSIGN
      HEADER TIP DISPLAY:
   Priority,0,INTEGER,
      HELPPOPUPID (80)
      GLOBAL ASSIGN
      SORT DISPLAY
      HIDDEN
      SELECTIVE ASSIGN
      PRIORITY:
   Quantity,0,REAL,
      ACC_IO(VW_IO_FORNECIMENTO,quantidade,NOT_RELATION,UPDATE)
      HELPPOPUPID (260)
      QUANTITY
      HEADER TIP DISPLAY:
   Import Marker,-1,INTEGER,
      DIALOG ONLY
      HIDDEN
      IMPORT MARKER;
;.
;; External Demand - Sales Orders.
; ----------------------------------------------------
Demand,ASSOCIATE (Orders),0,1000:
   Number,-1,INTEGER,
      PRIMARY KEY  
      DIALOG ONLY
      HIDDEN
      DEMAND TABLE:
   ID,-1,STRING,
      ACC_IO(VIW_IO_DEMANDA,id_registro,NOT_RELATION,UPDATE)
      FREE FORMAT
      FILTER
      LOCATE
	  HIDDEN
      DIALOG ONLY
      SORT DISPLAY:
   Order No.,-1,STRING,
      ACC_IO(VIW_IO_DEMANDA,num_ordem,NOT_RELATION,UPDATE)
      HELPPOPUPID (25510) 
      FREE FORMAT
      FILTER
      SORT DISPLAY
      LOCATE
	  TIP DISPLAY
      MATERIAL CONTROL ORDER NO:
   Order Type,2,STRING,
      ACC_IO(VIW_IO_DEMANDA,tipo_ordem,NOT_RELATION,UPDATE)
      HELPPOPUPID (25520)
      DATABASE(Order Types(Name))
      EXCLUDE "(({#Number}==0)||({#Number}==1))" 
      DIALOG ONLY
      LOCATE
      ORDER TYPE:
;   Order Line,1,INTEGER,
;      HELPPOPUPID (25530)
;      UNIQUE FOR FAMILY
;      TIP DISPLAY:
;   Company,-1,STRING,
;      HELPPOPUPID (25540)
;      FREE FORMAT
;      ALLOW UNSPECIFIED
;      LIST TIPS
;      LOCATE
;      FILTER
;      INHERIT FROM PARENT
;      SUBSTITUTE(-1 -> "Unspecified")
;      HEADER TIP DISPLAY:
   Part No.,-1,STRING,
      ACC_IO(VIW_IO_DEMANDA,codigo,NOT_RELATION,UPDATE)
      HELPPOPUPID (25430)
      FREE FORMAT
      SORT DISPLAY
      FILTER
      LOCATE
      MATERIAL CONTROL PART NO
      TIP DISPLAY:
   Description,-1,STRING,
      ACC_IO(VIW_IO_DEMANDA,descricao,NOT_RELATION,UPDATE)
      HELPPOPUPID (25440)
      FREE FORMAT
      FILTER
      ALLOW EMPTY
      TIP DISPLAY: 
   Demand Date,-1,TIME,
      ACC_IO(VIW_IO_DEMANDA,data_demanda,NOT_RELATION,UPDATE)
      HELPPOPUPID (25550)
      SHOW DATE
      DEMAND DATE
      SORT DISPLAY
      GLOBAL ASSIGN
      SELECTIVE ASSIGN
      INHERIT FROM PARENT
      HEADER TIP DISPLAY:
   Priority,0,INTEGER,
      HELPPOPUPID (80)
      SORT DISPLAY
      PRIORITY:
   Quantity,0,REAL,
      ACC_IO(VIW_IO_DEMANDA,quantidade,NOT_RELATION,UPDATE)
      HELPPOPUPID (260)
      QUANTITY
      GLOBAL ASSIGN
      SELECTIVE ASSIGN
      TIP DISPLAY:
   Multiple Quantity,-1,INTEGER,
      HELPPOPUPID (25560)
      ALLOW UNSPECIFIED
      GLOBAL ASSIGN
      SELECTIVE ASSIGN
      SUBSTITUTE(-1 -> "Unspecified")
      PEGGING MULTIPLE:
   Import Marker,-1,INTEGER,
      DIALOG ONLY
      HIDDEN
      IMPORT MARKER;
;.
;;  PBX.
; ----------------------------------------------------
PBX,"PBX.prtbl",0,1:
   Number,-1,INTEGER,
      PRIMARY KEY  
      DIALOG ONLY
      HIDDEN:
   Order No.,-1,STRING,
      UNIQUE
      HELPPOPUPID (25510) 
      FREE FORMAT
      FILTER
      SORT DISPLAY
      LOCATE:
   Part No.,-1,STRING,
      HELPPOPUPID (25430)
      DATABASE(Products(Part No.))
      EXCLUDE "(({#Parent Part}>0))"
      SORT DISPLAY
      FILTER
      LOCATE
      TIP DISPLAY:
   Demand Date,-1,TIME,
      HELPPOPUPID (25550)
      SHOW DATE
      SORT DISPLAY
      GLOBAL ASSIGN
      SELECTIVE ASSIGN:
   Priority,0,INTEGER,
      HELPPOPUPID (80)
      SORT DISPLAY:
   Quantity,0,REAL,
      HELPPOPUPID (260)
      GLOBAL ASSIGN
      SELECTIVE ASSIGN
      TIP DISPLAY:
   Controle PBX,-1,INTEGER,
      DIALOG ONLY
      HIDDEN:
   Import Marker,-1,INTEGER,
      DIALOG ONLY
      HIDDEN
      IMPORT MARKER;
;.
;; Bill of Materials - Details of materials required for each Operation within and Order or Part
;  --------------------------------------------------------------------------------------------
Bill of Materials,ASSOCIATE (Orders),0,15000:
   Number,-1,INTEGER,
      PRIMARY KEY
      BILL OF MATERIALS TABLE
      HIDDEN:
   ID,-1,STRING,
      ACC_IO(VW_IO_BOM,id_registro,NOT_RELATION,UPDATE)
      FREE FORMAT
      DIALOG ONLY
      UNIQUE
      FILTER
	  HIDDEN
      LOCATE
      SORT DISPLAY:
   BoM ID,-1,STRING, 
      ACC_IO(VW_IO_BOM,id_pai,NOT_RELATION,UPDATE)
      FREE FORMAT 
      SORT DISPLAY
      FILTER
      LOCATE
      SMC ORDER MATCH(BoM ID):
;   Lot,-1,STRING,
;      FREE FORMAT
;      DIALOG ONLY
;      ALLOW UNSPECIFIED
;      ALLOW EMPTY:
   Op. No.,10,INTEGER,
      HELPPOPUPID (25610)
	  ACC_IO(VW_IO_BOM,operacao,NOT_RELATION,UPDATE)
      ORDER MATCH(Op. No.):
   Required Part No.,-1,STRING,
      ACC_IO(VW_IO_BOM,id_filho,NOT_RELATION,UPDATE)
      HELPPOPUPID (25620)
      FILTER
      FREE FORMAT 
      REQUIRED PART NO:
;   Required Lot,-1,STRING,
;      FREE FORMAT
;      DIALOG ONLY
;      ALLOW UNSPECIFIED
;      ALLOW EMPTY:
   Required Part Description,-1,STRING,
      ACC_IO(VW_IO_BOM,desc_filho,NOT_RELATION,UPDATE)
      FILTER
      FREE FORMAT: 
   Required Quantity,0,REAL,
      ACC_IO(VW_IO_BOM,quantidade,NOT_RELATION,UPDATE)
      HELPPOPUPID (25630) 
      GLOBAL ASSIGN
      SELECTIVE ASSIGN
      REQUIRED QUANTITY:
   Order Type,0,STRING,
      HELPPOPUPID (25520)
      DATABASE(Order Types(Name))
      EXCLUDE "({#Number}>1)" 
      LOCATE
      ORDER TYPE:      
   Multiply by order quantity,0,TOGGLE,
      HELPPOPUPID (25640)
      GLOBAL ASSIGN
      SELECTIVE ASSIGN
      QUANTITY PER UNIT:
   Ignore Shortages,0,TOGGLE,
      HELPPOPUPID (25650)
      IGNORE SHORTAGE
      SELECTIVE ASSIGN
      GLOBAL ASSIGN:
   Multiple Quantity,-1,INTEGER,
      HELPPOPUPID (25560)
      GLOBAL ASSIGN
      SELECTIVE ASSIGN
      PEGGING MULTIPLE
      SUBSTITUTE(-1 -> "Unspecified"):
   Lead Time,-1,INTEGER,
      HELPPOPUPID (25560)
      SUBSTITUTE(-1 -> "Unspecified"):
   Import Marker,-1,INTEGER,
      HIDDEN
      IMPORT MARKER;
;
;
;.
;; Details of Co-Products and By-Products produced by each Operation of an Order or Part
;  -------------------------------------------------------------------------------------
Co-products,ASSOCIATE (Orders),0,1000:
   Number,-1,INTEGER,
      PRIMARY KEY  
      COPRODUCT TABLE
      HIDDEN:
   ID,-1,STRING,
      FREE FORMAT 
      DIALOG ONLY
      READ ONLY
      SORT DISPLAY:
; Campo Comentado: Usando como padr�o Part No.
;   Order No.,-1,STRING, 
;      FREE FORMAT
;      FILTER
;      ORDER MATCH(BoM Match Order No.):
   Part No.,-1,STRING, 
      FREE FORMAT 
      SORT RECORD
      SMC ORDER MATCH(Part No.):
   Op. No.,0,INTEGER,
      HELPPOPUPID (25710)
      ORDER MATCH(Op. No.):
   Co-product,-1,STRING,
      HELPPOPUPID (25720)
      FREE FORMAT
      FILTER
      COPRODUCT PART NO: 
   Quantity,0,REAL,
      HELPPOPUPID (25730)
      COPRODUCT QUANTITY:
   Multiply by order quantity,1,TOGGLE,
      HELPPOPUPID (25740)
      QUANTITY PER UNIT:
   Import Marker,-1,INTEGER,
      HIDDEN
      IMPORT MARKER;
;.
;; Ignore Shortages
; -------------------------------------------------------------
Ignore Shortages,ASSOCIATE (Orders),0,5000:
   Number,-1,INTEGER,
      PRIMARY KEY
      IGNORE SHORTAGES TABLE:
   External Demand Order,-1,INTEGER, 
      EXTERNAL DEMAND KEY
      ALLOW UNSPECIFIED:
   Internal Demand Order,-1,INTEGER, 
      INTERNAL DEMAND KEY
      ALLOW UNSPECIFIED:
   Part No.,-1,STRING,
      FREE FORMAT 
      MATERIAL CONTROL PART NO: 
   Ignore Shortages,0,TOGGLE, 
      SHORTAGE IGNORED;
;.
;; Order Links
;
Order Links,ASSOCIATE (Orders),0,30000:
   Number,-1,INTEGER,
      PRIMARY KEY 
      LINKS TABLE:
   From External Supply Order,-1,INTEGER,
      EXTERNAL SUPPLY KEY
      ALLOW UNSPECIFIED:
   From Internal Supply Order,-1,INTEGER, 
      INTERNAL SUPPLY KEY 
      ALLOW UNSPECIFIED:
   To External Demand Order,-1,INTEGER, 
      EXTERNAL DEMAND KEY
      ALLOW UNSPECIFIED:
   To Internal Demand Order,-1,INTEGER,
      INTERNAL DEMAND KEY
      ALLOW UNSPECIFIED:
   Part No.,-1,STRING,
      FREE FORMAT 
      MATERIAL CONTROL PART NO: 
   Quantity,0,REAL, 
      QUANTITY:
   Pegging Rule Used,-1,STRING,
      FREE FORMAT 
      PEGGING RULE USED:
   Verification Code,0,INTEGER,
      HIDDEN
      MATERIAL CONTROL VERIFICATION CODE:
   Locked,0,TOGGLE, 
      LINK LOCKED;
;
;.
;; Material Control Configuration
;
Material Control Configuration,,0,1:
   Number,0,INTEGER,
      MATERIAL CONTROL CONFIGURATION TABLE
      HELPPOPUPID (7010)
      PRIMARY KEY
      HIDDEN:
   Selected Pegging Rule Set,-1,STRING,
      HELPPOPUPID (24500)
      SELECTED RULE SET
      LIST TIPS
      DATABASE(Pegging Rules(Rule Set)):
   Ignore Supply Date,0,TOGGLE,
     HELPPOPUPID(24502)
     DIALOG ONLY
     GLOBAL ASSIGN:
   Allow Backward Links,0,TOGGLE,
      HELPPOPUPID (0)
      HIDDEN
      ALLOW BACKWARD LINKS:
   Always use this Rule Set,1,TOGGLE,
      HELPPOPUPID (24510)
      FORCE DEFAULT RULE;
;.
;; Pegging Rule Set
;
Pegging Rule Set,,0,100:
   Number,0,INTEGER,
      PEGGING RULE SET TABLE
      PRIMARY KEY
      HIDDEN:
   Selected Pegging Rule Set,-1,STRING,
      HELPPOPUPID (24500)
      SELECTED RULE SET
      LIST TIPS
      DATABASE(Pegging Rules(Rule Set)):
   Rule Description,-1,STRING,
      HELPPOPUPID (24501)
      DATABASE(Pegging Rules(Rule Description))
      READ ONLY
      HIDDEN
      MULTILINE
      TIP DISPLAY
      MAX LENGTH(300)
      ALWAYS UPDATE
      SECONDARY PICK
      ADDITIONAL REFERENCE(Selected Pegging Rule Set):
   Ignore Supply Date,0,TOGGLE,
      HELPPOPUPID(24502)
      DIALOG ONLY
      GLOBAL ASSIGN:
   Allow Backward Links,0,TOGGLE,
      HELPPOPUPID (0)
      DIALOG LEVEL 1
      HIDDEN
      ALLOW BACKWARD LINKS:
   Always use this Rule Set,1,TOGGLE,
      HELPPOPUPID (24510)
      DIALOG LEVEL 1
      FORCE DEFAULT RULE;
;
;.
;; Pegging Rules
;
Pegging Rules,,0,100:
   Belongs to Rule Set,-1,STRING,
      PEGGING RULES TABLE
      HELPPOPUPID (24000)
      FAMILY(Rule Set)
      DIALOG ONLY:
   Number,0,INTEGER,
      PRIMARY KEY
      HIDDEN:
   Rule Set,0,STRING,
      HELPPOPUPID (24010)
      SORT RECORD
      RULE SET
      FREE FORMAT(20)
      INHERIT FROM PARENT:
   Rule Description,0,STRING,
      HELPPOPUPID (24011)
      FREE FORMAT(20)
      MAX LENGTH(300)
      TIP DISPLAY
      MULTILINE
      INHERIT FROM PARENT:
   Clear Current Links,1,TOGGLE,
      HELPPOPUPID (24020)
      DIALOG ONLY
      INHERIT FROM PARENT
      CLEAR LINKS:
   First Pass Clear,0,TOGGLE,EVALUATE"({#Belongs to Rule Set}==-1)&&({#Clear Current Links}==1)"
      SWITCH OFF(Internal Supply Queue Filter)
      SWITCH OFF(Internal Supply Only)
      SWITCH OFF(Rule Type)
      SWITCH OFF(Retain Partial And Complete Allocations)
      SWITCH OFF(Retain Complete Allocations)
      SWITCH OFF(Allocate Multiples Only)
      ALWAYS UPDATE DIALOG ONLY DIALOG LEVEL 99:
   Rule,0,STRING,
      HELPPOPUPID (24030)
      SORT RECORD
      RULE NAME
      FREE FORMAT(30):
   Enabled,1,TOGGLE,
      HELPPOPUPID (24040)
      RULE ENABLED:
   Debug This Rule,0,TOGGLE,
      HELPPOPUPID (24050)
      DEBUG ENABLED:
   Internal Supply Only,0,TOGGLE,
      HELPPOPUPID (24160)
      INTERNAL SUPPLY ONLY 
      DIALOG ONLY:
   Internal Supply Queue Filter,"ALL",STRING,
      HELPPOPUPID (24060)
      FREE FORMAT(50)
      INTERNAL SUPPLY FILTER
      MULTILINE
      MAX LENGTH (500)
      DIALOG ONLY:
   External Supply Queue Filter Toggle,0,TOGGLE,
      DIALOG ONLY DIALOG LEVEL 99 
      EVALUATE"(({#Internal Supply Only}==0)&&({#First Pass Clear}==0))" 
      SWITCH ON(External Supply Queue Filter):
   External Supply Queue Filter,"ALL",STRING,
      HELPPOPUPID (24170)
      FREE FORMAT(50)
      EXTERNAL SUPPLY FILTER
      MULTILINE
      MAX LENGTH (500)
      DIALOG ONLY:
   Supply Queue Ranking Toggle,0,TOGGLE,
      DIALOG ONLY DIALOG LEVEL 99 
      EVALUATE"(({#Internal Supply Only}==1)&&({#First Pass Clear}==0))" 
      SWITCH ON(Supply Queue Ranking):
   Supply Queue Ranking,-1,MATRIX,
      HELPPOPUPID (24070)
      INTERNAL SUPPLY RANKING
      FIELD RANKING LIST
      REMOTE(Orders(Due Date))
      REMOTE(Orders(Earliest Start Date))
      REMOTE(Orders(Priority))
      REMOTE(Orders(Setup Start))
      REMOTE(Orders(End Time))
      REMOTE(Orders(Weighting)):
   Inherit From Supply,0,TOGGLE,
      HELPPOPUPID (24180)
      ENABLE INHERIT FROM SUPPLY
      DIALOG ONLY:
   Internal Demand Only,0,TOGGLE,
      HELPPOPUPID (24200)
      INTERNAL DEMAND ONLY
      DIALOG ONLY 
      SWITCH OFF(External Demand Queue Filter):
   Include Scheduled Orders in Demand Queue,0,TOGGLE,
      HELPPOPUPID (24150)
      DIALOG ONLY
      GLOBAL ASSIGN
      PEG SCHEDULED ORDERS:
   Internal Demand Queue Filter,"ALL",STRING,
      HELPPOPUPID (24080)
      FREE FORMAT(50)
      INTERNAL DEMAND FILTER
      MULTILINE
      MAX LENGTH (500)
      DIALOG ONLY:
   External Demand Queue Filter,"ALL",STRING,
      HELPPOPUPID (24080)
      FREE FORMAT(50)
      EXTERNAL DEMAND FILTER
      MULTILINE
      MAX LENGTH (500)
      DIALOG ONLY:
   Demand Queue Ranking Toggle,0,TOGGLE,
      DIALOG ONLY DIALOG LEVEL 99 
      EVALUATE"(({#Internal Demand Only}==1)&&({#First Pass Clear}==0))" 
      SWITCH ON(Demand Queue Ranking):
   Demand Queue Ranking,-1,MATRIX,
      HELPPOPUPID (24090)
      INTERNAL DEMAND RANKING
      FIELD RANKING LIST
      REMOTE(Orders(Due Date))
      REMOTE(Orders(Earliest Start Date))
      REMOTE(Orders(Priority))
      REMOTE(Orders(Setup Start))
      REMOTE(Orders(End Time))
      REMOTE(Orders(Weighting)):
   Inherit From Demand,0,TOGGLE,
      HELPPOPUPID (24190)
      ENABLE INHERIT FROM DEMAND
      DIALOG ONLY:
   Inherit From Demand Toggle,0,TOGGLE,
      DIALOG ONLY DIALOG LEVEL 99 
      EVALUATE"(({#Inherit From Supply}==0)&&({#First Pass Clear}==0))" 
      SWITCH ON(Inherit From Demand):
   Inherit From Supply Toggle,0,TOGGLE,
      DIALOG ONLY DIALOG LEVEL 99 
      EVALUATE"(({#Inherit From Demand}==0)&&({#First Pass Clear}==0))" 
      SWITCH ON(Inherit From Supply):
   Rule Type,99,STRING,
      HELPPOPUPID (24100)
      RULE TYPE
      LIST TIPS
      EXCLUDE "(~{Name}~==~User Function~)" 
      DATABASE(SMC Rule Types Lookup(Name)):
   User Defined Rule Toggle,0,TOGGLE,
      HELPPOPUPID (1)
      DIALOG ONLY
      DIALOG LEVEL 99
      SWITCH ON(Expression)
      EVALUATE "({#Rule Type}==0)||({#Rule Type}==100)"
      ALWAYS UPDATE:
   Expression,-1,STRING,
      HELPPOPUPID (24110)
      USER DEFINED EXPRESSION
      HIDE WHEN DISABLED
      FREE FORMAT(50)
      MULTILINE
      MAX LENGTH (500)
      DIALOG ONLY:
   PESP Script Toggle,0,TOGGLE,
      HELPPOPUPID (1)
      DIALOG ONLY
      DIALOG LEVEL 99
      SWITCH ON(PESP Script)
      EVALUATE "({#Rule Type}==110)"
      ALWAYS UPDATE:
   PESP Script,-1,STRING,
      HELPPOPUPID (24120)
      USER DEFINED PESP SCRIPT
      OVERWRITE PREVIOUS
      HIDE WHEN DISABLED
      FREE FORMAT(50)
      MULTILINE
      MAX LENGTH (500)
      DIALOG ONLY:
   Allocate Multiples Only,0,TOGGLE,
      HELPPOPUPID (24210)
      MULTIPLES ONLY
      DIALOG ONLY:
   Retain Partial And Complete Allocations,0,TOGGLE,
      PREVIOUS NAME "Retain Partial Allocations"
      HELPPOPUPID (24130)
      DIALOG ONLY
      RETAIN PARTIAL:
   Retain Allocations Toggle,0,TOGGLE,
      DIALOG ONLY
      DIALOG LEVEL 99
      SWITCH ON(Retain Complete Allocations)
      EVALUATE "(({#First Pass Clear}==0)&&({#Retain Partial And Complete Allocations}==0))"
      ALWAYS UPDATE:
   Retain Complete Allocations,0,TOGGLE,
      HELPPOPUPID (24140)
      DIALOG ONLY
      RETAIN COMPLETE:
   Allow Backward Links,0,TOGGLE,
      HELPPOPUPID (0)
      DIALOG ONLY
      ALLOW BACKWARD LINKS;
;.
;
RECORD_END
;