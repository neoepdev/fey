ALTER PROCEDURE dbo.RPT_RESOURCE_LOAD_NEW

(
	@De DATETIME,
	@Ate DATETIME
)

AS

BEGIN



	--DECLARE @Dataset VARCHAR(200)
	--DECLARE @De DATETIME
	--DECLARE @Ate DATETIME

	--SET @Dataset = 'Atual'
	--SET @De = DATEADD(d, DATEDIFF(d,0,GETDATE()), 0) -360
	--SET @Ate = DATEADD(d, DATEDIFF(d,0,GETDATE()), 0) +900
	
IF OBJECT_ID ('tempdb..#RESOURCE_LOAD') IS NOT NULL DROP TABLE #RESOURCE_LOAD
SELECT 
	Dataset,
	ResourceName,
	ISNULL(WorkcenterName,'Recursos Sem Workcenter') AS WorkcenterName,
	count(ReferenceDay) * 24 as TotalHorizonHs,
	SUM(TotalAvailableHs) as TotalAvailableHs,
	SUM(TotalLoadHs) as TotalLoadHs,
	SUM(SetupLoadHs) as SetupLoadHs,
	SUM(OperationLoadHs) as OperationLoadHs,
	 CASE 
		WHEN SUM(TotalAvailableHs) = 0
		THEN 0
		ELSE((sum(TotalLoadHs)/ SUM(TotalAvailableHs))*100) 
	  END AS AvgOcupationPerc
	,COUNT (referencedate) as CONTADOR
INTO #RESOURCE_LOAD
--SELECT *
FROM VIW_RESOURCE_LOAD
WHERE 
Dataset = 'Atual'
AND
ReferenceDate >= @De
AND
ReferenceDate < @Ate
GROUP BY
Dataset,
WorkcenterName,
ResourceName

--select * from #RESOURCE_LOAD


--drop table #ORDERS_DATA
IF OBJECT_ID ('tempdb..#ORDERS_DATA') IS NOT NULL DROP TABLE #ORDERS_DATA
SELECT 
	Dataset,
	ResourceName,
	COUNT(orderno) as NoOrders,
	sum(WaitingTime) as TotalWaitingTime,
	SUM(Quantity) AS Quantity,
	SUM(OriginalQuantity) AS OriginalQuantity,
	SUM(KgQuantity) AS KgQuantity
INTO #ORDERS_DATA
--SELECT *
FROM VIW_ORDERS_DATA
WHERE
	Dataset = 'Atual'
	AND
	(
	SetupStart BETWEEN @De AND @Ate
	OR
	EndTime BETWEEN @De AND @Ate
	OR
	((@De between SetupStart AND EndTime) AND (@Ate between SetupStart AND EndTime) )
	)
GROUP BY
	Dataset,
	ResourceName

--select * from #ORDERS_DATA

IF OBJECT_ID ('tempdb..#QUANTIDADEPORPERIODO') IS NOT NULL DROP TABLE #QUANTIDADEPORPERIODO
SELECT 
	Recurso,
	SUM(Quantidade) as TotalQuantity
INTO #QUANTIDADEPORPERIODO
--SELECT *
FROM QUANTIDADEPORPERIODO
where 
cast(Inicio as DATE) >= @De
and
CAST(Fim as DATE) <= @Ate
GROUP BY Recurso

--select * FROM QUANTIDADEPORPERIODO

SELECT 
	RL.*,
	isnull(OD.NoOrders,0) as NoOrders,
	Q.TOTALQUANTITY,
	OD.OriginalQuantity,
	OD.Quantity,
	OD.KgQuantity,
	((OD.Quantity * OD.KgQuantity)/OD.OriginalQuantity) AS QtdKgSaldo,
	(Q.TOTALQUANTITY * ((OD.Quantity * OD.KgQuantity)/OD.OriginalQuantity))/OD.Quantity AS QtdKgSaldoQuebra,
	isnull(OD.TotalWaitingTime,0) as TotalWaitingTime
FROM #RESOURCE_LOAD RL
LEFT JOIN #ORDERS_DATA OD 
	ON RL.Dataset = OD.Dataset 
	and RL.ResourceName = OD.ResourceName
LEFT JOIN #QUANTIDADEPORPERIODO Q 
	ON Q.RECURSO = RL.ResourceName

END




