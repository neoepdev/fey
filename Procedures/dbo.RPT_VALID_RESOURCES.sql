ALTER PROCEDURE dbo.RPT_VALID_RESOURCES
AS

IF OBJECT_ID('tempdb..#TMP_DADOS') IS NOT NULL DROP TABLE #TMP_DADOS
SELECT DISTINCT O.DatasetId
			  , D.NAME AS Dataset
			  , Orderno
			  , PartNo AS Produto
			  , OpNo
			  , RG.[Name] AS Grupo_Recursos
			  , R.[Name] AS Recurso
			  , isnull(A1.[Name], '') AS Atributo1
			  , isnull(A2.[Name], '') AS Atributo2
			  , isnull(A3.[Name], '') AS Atributo3
			  , isnull(A4.[Name], '') AS Atributo4
			  , isnull(A5.[Name], '') AS Atributo5
			  , isnull(A1VR.ValidResources, '1') AS ValidaAtributo1
			  , isnull(A2VR.ValidResources, '1') AS ValidaAtributo2
			  , isnull(A3VR.ValidResources, '1') AS ValidaAtributo3
			  , isnull(A4VR.ValidResources, '1') AS ValidaAtributo4
			  , isnull(A5VR.ValidResources, '1') AS ValidaAtributo5
			  ,AN1.Name AS NomeAtributo1
			  ,AN2.Name AS NomeAtributo2
			  ,AN3.Name AS NomeAtributo3
			  ,AN4.Name AS NomeAtributo4
			  ,AN5.Name AS NomeAtributo5
INTO
	#TMP_DADOS
FROM
	userdata.orders O
	INNER JOIN UserData.ResourceGroup RG
		ON O.ResourceGroup = RG.ResourceGroupId
	INNER JOIN UserData.ResourceGroupResources RGR
		ON O.ResourceGroup = RGR.ResourceGroupId
	INNER JOIN UserData.Resources R
		ON RGR.Resources = R.ResourcesId
	INNER JOIN Userdata.Orders_Dataset D
		ON O.DatasetID = D.DatasetId
	LEFT JOIN UserData.Attribute1 A1
		ON o.Attribute1 = A1.Attribute1Id
	LEFT JOIN UserData.Attribute2 A2
		ON o.Attribute2 = A2.Attribute2Id
	LEFT JOIN UserData.Attribute3 A3
		ON o.Attribute3 = A3.Attribute3Id
	LEFT JOIN UserData.Attribute4 A4
		ON o.Attribute4 = A4.Attribute4Id
	LEFT JOIN UserData.Attribute5 A5
		ON o.Attribute5 = A5.Attribute5Id
	LEFT JOIN UserData.Attributes AN1
		ON 1 = AN1.AttributesId
	LEFT JOIN UserData.Attributes AN2
		ON 2 = AN2.AttributesId
	LEFT JOIN UserData.Attributes AN3
		ON 3 = AN3.AttributesId
	LEFT JOIN UserData.Attributes AN4
		ON 4 = AN4.AttributesId
	LEFT JOIN UserData.Attributes AN5
		ON 5 = AN5.AttributesId
	LEFT JOIN UserData.Attribute1ValidResources A1VR
		ON A1.Attribute1Id = A1VR.XAxisAttribute1 AND R.ResourcesId = A1VR.YAxisResources
	LEFT JOIN UserData.Attribute2ValidResources A2VR
		ON A2.Attribute2Id = A2VR.XAxisAttribute2 AND R.ResourcesId = A2VR.YAxisResources
	LEFT JOIN UserData.Attribute3ValidResources A3VR
		ON A3.Attribute3Id = A3VR.XAxisAttribute3 AND R.ResourcesId = A3VR.YAxisResources
	LEFT JOIN UserData.Attribute4ValidResources A4VR
		ON A4.Attribute4Id = A4VR.XAxisAttribute4 AND R.ResourcesId = A4VR.YAxisResources
	LEFT JOIN UserData.Attribute5ValidResources A5VR
		ON A5.Attribute5Id = A5VR.XAxisAttribute5 AND R.ResourcesId = A5VR.YAxisResources
--OUT: SELECT * FROM #TMP_DADOS

IF OBJECT_ID('tempdb..#TMP_DADOS_TRATADOS') IS NOT NULL DROP TABLE #TMP_DADOS_TRATADOS
SELECT Dataset
	 , Orderno
	 , OpNo
	 , MIN(cast(ValidaAtributo1 AS INT)) AS ValidaAtributo1Agrupado
	 , MIN(cast(ValidaAtributo2 AS INT)) AS ValidaAtributo2Agrupado
	 , MIN(cast(ValidaAtributo3 AS INT)) AS ValidaAtributo3Agrupado
	 , MIN(cast(ValidaAtributo4 AS INT)) AS ValidaAtributo4Agrupado
	 , MIN(cast(ValidaAtributo5 AS INT)) AS ValidaAtributo5Agrupado
INTO
	#TMP_DADOS_TRATADOS
FROM
	#TMP_DADOS
GROUP BY
	Dataset
  , Orderno
  , OpNo
--OUT:select * from #TMP_DADOS_TRATADOS


IF OBJECT_ID('tempdb..##DADOS_STATUS_SIMPLES') IS NOT NULL DROP TABLE #DADOS_STATUS_SIMPLES
SELECT CASE
		   WHEN  (cast(ValidaAtributo1 AS INT) * cast(ValidaAtributo2 AS INT) * cast(ValidaAtributo3 AS INT) * cast(ValidaAtributo4 AS INT) * cast(ValidaAtributo5 AS INT)) > 0 THEN
			   1
		   ELSE
			   0
	   END AS Status
	 , TD.*
INTO #DADOS_STATUS_SIMPLES
FROM
	#TMP_DADOS TD
	LEFT JOIN #TMP_DADOS_TRATADOS TDT
		ON TD.Orderno = TDT.Orderno AND TD.Opno = TDT.Opno AND TD.Dataset = TDT.Dataset
-- SELECT * FROM #DADOS_STATUS_SIMPLES

		
IF OBJECT_ID('tempdb..#DADOS_STATUS_AGRUPADO') IS NOT NULL DROP TABLE #DADOS_STATUS_AGRUPADO
SELECT 
	DatasetId, 
	Orderno, 
	max(status) as status
INTO #DADOS_STATUS_AGRUPADO
FROM #DADOS_STATUS_SIMPLES
GROUP BY datasetid, orderno
-- SELECT * FROM #DADOS_STATUS_AGRUPADO



SELECT CASE
		   WHEN  dsa.status > 0 THEN
			   'OK'
		   ELSE
			   'Conflito Recurso Válido'
	   END AS Status
	 , TD.*
FROM
	#TMP_DADOS TD
	LEFT JOIN #TMP_DADOS_TRATADOS TDT
		ON TD.Orderno = TDT.Orderno AND TD.Opno = TDT.Opno AND TD.Dataset = TDT.Dataset
	LEFT JOIN #DADOS_STATUS_AGRUPADO DSA
		ON TD.Orderno = DSA.Orderno AND TD.Datasetid = DSA.Datasetid



