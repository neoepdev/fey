ALTER PROCEDURE dbo.PRO_ORDEM_CONSULTA

AS


if object_id ('tempdb..#CONSULTA') is not null DROP TABLE #CONSULTA
SELECT P.PartNo
, C.OrderNo 
, C.EarliestStartDate
, C.DueDate
, C.Quantity
INTO #CONSULTA
FROM UserData.Products P 
INNER JOIN UserData.Consulta C ON C.PartNo = P.ProductsId

--SELECT * FROM #CONSULTA


if object_id ('tempdb..#ROTEIRO') is not null DROP TABLE #ROTEIRO
SELECT 
dense_rank() OVER (ORDER BY ProductsId) AS SEQ
, *
INTO #ROTEIRO
FROM UserData.Products P
WHERE PartNo = (SELECT P.PartNo FROM UserData.Products P INNER JOIN UserData.Consulta C ON C.PartNo = P.ProductsId)
ORDER BY Opno

-- SELECT * FROM #ROTEIRO

DECLARE @CONT AS INTEGER

SET @CONT = 1

WHILE @CONT <= (SELECT max(SEQ) FROM #ROTEIRO)

BEGIN

INSERT INTO Userdata.Orders
(OrdersId,Datasetid,Orderno,BelongsToOrderNo,opno, partno,Product,Bomid, Producttype,StandardTime,StandardQuantity,RealOpTimePerItem
, Attribute1, Attribute2, Attribute3, Attribute4, Attribute5, OperationName
, ResourceGroup, EarliestStartDate, DueDate, Quantity, __seq__Orders)

SELECT 
isnull((SELECT max(OrdersID) FROM Userdata.Orders),0) + 1 AS OrdersId
, (SELECT Datasetid from Userdata.Orders_dataset where name = 'Atual') AS Datasetid
, C.Orderno 
, (CASE WHEN R.SEQ = 1 THEN -1 ELSE (isnull((SELECT max(OrdersID) FROM Userdata.Orders),0) + 2 - @CONT) END) AS BelongsToOrderNo
, R.Opno
, C.Partno
, R.Product
, R.Bomid
, R.ProductType
, R.StandardTime
, R.StandardQuantity
, R.StandardTime/R.StandardQuantity as RealOpTimePerItem
, R.Attribute1
, R.Attribute2
, R.Attribute3
, R.Attribute4
, R.Attribute5
, R.OperationName
, R.ResourceGroup
, C.EarliestStartDate
, C.DueDate
, C.Quantity
, CASE WHEN (SELECT max(__seq__Orders) FROM Userdata.Orders) IS NULL 
		THEN 0 
		ELSE ISNULL((SELECT max(__seq__Orders) FROM Userdata.Orders),0) + 1 END AS __seq__Orders
FROM #ROTEIRO R
	INNER JOIN #CONSULTA C
		ON R.Partno = C.Partno
WHERE R.SEQ = @CONT

SET @CONT = @CONT + 1

END
		
		
		
--SELECT * FROM USERDATA.ORDERS