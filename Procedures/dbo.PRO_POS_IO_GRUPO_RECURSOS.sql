ALTER PROCEDURE dbo.PRO_POS_IO_GRUPO_RECURSOS
AS

--CRIA DADOS BASE

IF OBJECT_ID('tempdb..#TMP_DADOS') IS NOT NULL DROP TABLE #TMP_DADOS
SELECT 
	RG.ResourceGroupId,
	RG.[Name] AS Grupo,
	COUNT(R.[Name]) AS Qtd_Recurso,
	MIN(R.FinitoInfinito) AS ID_MIN,
	MAX(R.FinitoInfinito) AS ID_MAX
INTO #TMP_Dados
FROM UserData.ResourceGroup RG
INNER JOIN UserData.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
INNER JOIN 
	(
	SELECT 
		ISNULL(finiteorinfinite, 99) AS FinitoInfinito,
		* FROM UserData.Resources
	) 
	R ON RGR.Resources = R.ResourcesId
GROUP BY RG.ResourceGroupId, RG.[Name]

SELECT * FROM #TMP_Dados
WHERE ID_MAX <> 1

--AJUSTA AUTO SEQ RESTRICT PARA QUE CONTENHAM TERCEIROS E PODEM PRODUZIR INTERNAMENTE
UPDATE UserData.ResourceGroupResources SET AutomaticSequencing = 1

UPDATE UserData.ResourceGroupResources SET AutomaticSequencing = 0
FROM UserData.ResourceGroupResources RGR
INNER JOIN
	(
		SELECT R.ResourcesId, t.*, R.[Name],AutomaticSequencing FROM #TMP_Dados T
		INNER JOIN UserData.ResourceGroup RG ON T.ResourceGroupId = RG.ResourceGroupId
		INNER JOIN UserData.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
		INNER JOIN UserData.Resources R ON RGR.Resources = R.ResourcesId AND FiniteOrInfinite IS NULL
		WHERE Qtd_Recurso > 1 AND ID_MIN < 99 AND ID_MAX = 99
	)A ON RGR.ResourceGroupID = A.ResourceGroupId AND RGR.Resources = A.ResourcesId


