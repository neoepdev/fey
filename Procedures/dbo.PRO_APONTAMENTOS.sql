ALTER PROCEDURE dbo.PRO_APONTAMENTOS
AS

IF OBJECT_ID('tempdb..#TMP_OPERACOES') IS NOT NULL DROP TABLE #TMP_OPERACOES
select 
	b.DatasetId,
	b.OrdersId,
	o.ordem_producao, 
	O.SEQUENCIA_OPERACAO, 
	--R.UNIDADE_CALCULO, 
	QUANTIDADE_KG, 
	o.QUANTIDADE_PCS, 
	sum(a.QUANTIDADE_PRODUZIDA) as QUANTIDADE_PRODUZIDA,
	o.QUANTIDADE_PCS - sum(a.QUANTIDADE_PRODUZIDA) as saldo
INTO #TMP_OPERACOES
from io.apontamento A
inner join io.ordem_producao O on a.ORDEM_PRODUCAO = o.ORDEM_PRODUCAO and a.SEQUENCIA_OPERACAO = o.SEQUENCIA_OPERACAO
--inner join io.RECURSO R on A.RECURSO_APONTADO = R.RECURSO
INNER JOIN UserData.Orders B ON A.ORDEM_PRODUCAO = B.OrderNo AND a.SEQUENCIA_OPERACAO = B.OpNo
INNER JOIN UserData.Orders_Dataset D ON B.DatasetId = D.DATASETID AND D.NAME = 'Atual'
group by
	b.DatasetId,
	b.OrdersId,
	o.ordem_producao, 
	O.SEQUENCIA_OPERACAO, 
	--R.UNIDADE_CALCULO, 
	QUANTIDADE_KG, 
	o.QUANTIDADE_PCS

--select * from #TMP_OPERACOES
--where ordem_producao = '60314-22'

------------------------------------------------
--DELETA ORDENS CONCLUIDAS
------------------------------------------------

DELETE UserData.Orders
WHERE  ordersid IN 
            (
			SELECT distinct ordersid FROM #TMP_OPERACOES
			WHERE saldo <= 0
            )


------------------------------------------------
--AJUSTA ORDENS APONTADAS PARCIALMENTE
------------------------------------------------

UPDATE USERDATA.Orders SET Quantity = quantity_corrig
FROM UserData.Orders O
INNER JOIN
	(SELECT 
		O.Ordersid,
		O.quantity, 
		OriginalQuantity, 
		PcQuantity, 
		KgQuantity, 
		UnidadeMedida,
		--case 
		--	when unidademedida = 'PC' 
		--	then saldo 
		--	when unidademedida = 'KG' and originalquantity > 0
		--	then saldo*(KgQuantity/pcquantity)
		--	else saldo
		--end as quantity_corrig, 
		t.saldo as quantity_corrig, 
		t.saldo --em PC
	from userdata.orders O
	INNER JOIN #TMP_OPERACOES T on o.OrdersId = T.OrdersId
	where saldo > 0 
)B ON O.OrdersId = B.OrdersId




------------------------------------------------
--EXCLUI TABELAS RELACIONADAS
------------------------------------------------


--CRIA TABELA COM IDS DE ORDENS E SUPPLY
IF OBJECT_ID('tempdb..#TMP_ID_ORDENS') IS NOT NULL DROP TABLE #TMP_ID_ORDENS
SELECT DISTINCT CAST(datasetid AS VARCHAR)+'.'+CAST(ordersid AS VARCHAR) AS ID
INTO #TMP_ID_ORDENS
FROM userdata.Orders

IF OBJECT_ID('tempdb..#TMP_ID_SUPPLY') IS NOT NULL DROP TABLE #TMP_ID_SUPPLY
SELECT DISTINCT CAST(datasetid AS VARCHAR)+'.'+CAST(SUPPLYID AS VARCHAR) AS ID
INTO #TMP_ID_SUPPLY
FROM userdata.Supply


DELETE userdata.OrdersExternalPeggingInformation
WHERE CAST(datasetid AS VARCHAR)+'.'+CAST(ordersid AS VARCHAR) NOT IN 
	(SELECT * FROM #TMP_ID_ORDENS)

DELETE userdata.OrdersExternalPeggingInformation
WHERE CAST(datasetid AS VARCHAR)+'.'+CAST(ExternalPeggingInformation AS VARCHAR) NOT IN 
	(SELECT * FROM #TMP_ID_ORDENS)

  
DELETE Userdata.orderlinks
where fromExternalSupplyOrder  is not NULL
	and CAST(DatasetId AS VARCHAR)+'.'+CAST(fromExternalSupplyOrder AS VARCHAR) not in (SELECT * FROM #TMP_ID_SUPPLY)
 
DELETE Userdata.orderlinks
where fromInternalSupplyOrder  is not null 
	and CAST(DatasetId AS VARCHAR)+'.'+CAST(fromInternalSupplyOrder AS VARCHAR)  not in (SELECT * FROM #TMP_ID_ORDENS)
 
DELETE Userdata.orderlinks
where toInternalDemandOrder  is not null 
	and CAST(DatasetId AS VARCHAR)+'.'+CAST(toInternalDemandOrder AS VARCHAR)  not in (SELECT * FROM #TMP_ID_ORDENS)
 
DELETE Userdata.orderlinks
where toExternalDemandOrder  is not null 
	and toExternalDemandOrder not in (select distinct DemandId from UserData.Demand)
 
  
--EXCLUI GRUPOS DE RECURSOS DUPLICADOS
DELETE UserData.ResourceGroupResources
WHERE [__seq__ResourceGroupResources] IN 
	(
	SELECT [__seq__ResourceGroupResources]
	FROM 
		(
		SELECT 
		RG.[Name] AS Grupo_Rec, R.[Name] AS Rec, RGR.[__seq__ResourceGroupResources],
		ROW_NUMBER() OVER(PARTITION BY RG.[Name],R.[Name] ORDER BY RG.[Name]) AS Contador
		FROM
		UserData.ResourceGroupResources RGR
		INNER JOIN UserData.ResourceGroup RG ON RGR.ResourceGroupId = RG.ResourceGroupId
		INNER JOIN UserData.Resources R ON R.ResourcesId = RGR.Resources
		)A
	WHERE Contador > 1
	)
	


------------------------------------------------
--REFAZ OS PAIS
------------------------------------------------

EXEC PRO_RECALCULA_PERTENCE_A_ORDEM



------------------------------------------------
--DELETA RELACIONAMENTOS DE OPERAÇÕES CONCLUIDAS, AS QUAIS JÁ CONSUMIRAM MATERIAL
------------------------------------------------

DELETE userdata.BillOfMaterials
FROM userdata.BillOfMaterials D
INNER JOIN
	(
	SELECT * FROM io.estrutura E
	LEFT JOIN 
		(
		SELECT DISTINCT 
			O.orderno, A.name as OP_Cod
		FROM userdata.orders O 
		INNER JOIN userdata.Orders_Dataset D on o.datasetid = d.datasetid and D.name = 'Atual'
		INNER JOIN userdata.attribute4 A on o.Attribute4 = A.Attribute4Id
		)B
	on e.ORDEM_ITEM_PAI = B.orderno and B.OP_Cod = e.codigo_operacao
	WHERE orderno is null
	)C
on D.BoMID = c.ORDEM_ITEM_PAI and d.requiredpartno = c.codigo_filho


