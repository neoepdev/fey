ALTER PROCEDURE dbo.PRO_ATUALIZA_VISUALIZACAO_ESPERA
AS

if object_id ('tempdb..#TMP_DADOS') is not null DROP TABLE #TMP_DADOS
SELECT DISTINCT
    DatasetId,
      OrdersID, 
      Orderno, 
      Opno, 
      DENSE_RANK ( ) OVER (PARTITION BY OrderNo ORDER BY Opno) AS RANK,
      StartTime, 
      EndTime, 
      Resource, 
      R.Name as ResourceDesc,
      MaxTimeBeforeNextOp,
      WC.AverageWaitingTime
INTO #TMP_DADOS
FROM userdata.orders O
LEFT JOIN UserData.Resources R 
      ON O.resource = R.ResourcesId
LEFT JOIN UserData.Workcenter WC 
      ON R.WorkCenter = WC.WorkcenterId
where Resource is not null
--select * from #TMP_DADOS



--DEFINE RECURSO POSTERIOR E ANTERIOR
if object_id ('tempdb..#TMP_NEXT') is not null DROP TABLE #TMP_NEXT
SELECT DatasetId, OrderNo, OpNo, ResourceDesc, Rank - 1 as RankNext 
INTO #TMP_NEXT
FROM #TMP_DADOS
order by datasetid, OrderNo, opno


if object_id ('tempdb..#TMP_BEFORE') is not null DROP TABLE #TMP_BEFORE
SELECT DatasetId, OrderNo, OpNo, ResourceDesc, Rank + 1 as RankBefore 
INTO #TMP_BEFORE
FROM #TMP_DADOS
order by datasetid, OrderNo, opno


UPDATE USERDATA.ORDERS
SET
ResourceBeforeDesc = '',
ResourceNextDesc = ''

UPDATE USERDATA.ORDERS
SET
ResourceBeforeDesc = ISNULL(B.ResourceDesc,''),
ResourceNextDesc = ISNULL(N.ResourceDesc,'')
--SELECT DISTINCT D.DatasetId, D.OrderNo, D.OpNo, D.ResourceDesc, B.ResourceDesc AS ResourceBefore, N.ResourceDesc AS ResourceNext
FROM USERDATA.ORDERS O
LEFT JOIN #TMP_DADOS D
ON O.DatasetId = D.DatasetId and O.OrderNo = D.OrderNo and O.OpNo = D.OpNo
LEFT JOIN #TMP_BEFORE B
ON D.DatasetId = B.DatasetId AND D.OrderNo = B.OrderNo AND D.Rank = B.RankBefore
LEFT JOIN #TMP_NEXT N
ON D.DatasetId = N.DatasetId AND D.OrderNo = N.OrderNo AND D.Rank = N.RankNext
WHERE  
D.Resource IS NOT NULL



--OUT:SELECT * FROM #TMP_DADOS order by datasetid, ordersid, opno, rank

--IDENTIFICA WIP INTERNO
if object_id ('tempdb..#WIP_INTERNO') is not null DROP TABLE #WIP_INTERNO
SELECT 
      F.DatasetID, 
      P.ordersId, 
      P.Resource,
      f.oRDERNO AS fILHO, 
      F.Endtime, 
      P.Starttime,
      p.oRDERNO AS pAI,
      F.MaxTimeBeforeNextOp,
      F.AverageWaitingTime,
      --Identifica qual parametro usar, se do workcenter ou tempo máximo até a próxima operação
      CASE 
            WHEN F.MaxTimeBeforeNextOp IS NOT NULL 
            THEN F.MaxTimeBeforeNextOp
            WHEN F.MaxTimeBeforeNextOp IS NULL AND F.AverageWaitingTime IS NOT NULL 
            THEN F.AverageWaitingTime
      END AS ParametroValido,
      CAST(DATEDIFF(hour, F.Endtime, P.Starttime)AS FLOAT)/24 AS WIP_Dias,
      --
      (
      CASE WHEN CAST(DATEDIFF(hour, F.Endtime, P.Starttime)AS FLOAT) <= 0
            THEN 0.01
            ELSE CAST(DATEDIFF(hour, F.Endtime, P.Starttime)AS FLOAT)/24
      END
      )
      /
      (
      CASE 
            WHEN F.MaxTimeBeforeNextOp IS NOT NULL and F.MaxTimeBeforeNextOp > 0
            THEN F.MaxTimeBeforeNextOp
            WHEN F.MaxTimeBeforeNextOp IS NULL AND F.AverageWaitingTime IS NOT NULL and F.AverageWaitingTime > 0
            THEN F.AverageWaitingTime
            ELSE 1
      END
      ) AS Dif_WIP
INTO #WIP_INTERNO
FROM #TMP_DADOS P 
INNER JOIN #TMP_DADOS F
ON F.DatasetId = P.DatasetId AND F.ORDERNO = P.ORDERNO AND F.RANK+1 = P.RANK
ORDER BY  f.orderno, f.OpnO
--OUT: SELECT * FROM #WIP_INTERNO

--IDENTIFICA WIP EXTERNO
if object_id ('tempdb..#WIP_EXTERNO') is not null DROP TABLE #WIP_EXTERNO
SELECT 
      F.DatasetID, 
      P.ordersId, 
      F.Resource, 
      Max(f.oRDERNO) AS fILHO,
      F.Endtime, 
      MAX(P.Starttime) AS Starttime,
      max(p.oRDERNO) AS pAI,
      P.MaxTimeBeforeNextOp,
      P.AverageWaitingTime,
      CASE 
            WHEN P.MaxTimeBeforeNextOp IS NOT NULL and P.MaxTimeBeforeNextOp > 0
            THEN P.MaxTimeBeforeNextOp
            WHEN P.MaxTimeBeforeNextOp IS NULL AND P.AverageWaitingTime IS NOT NULL and P.AverageWaitingTime > 0
            THEN P.AverageWaitingTime
      END AS ParametroValido,
      --
      MAX(CAST(DATEDIFF(hour, F.Endtime, P.Starttime)AS FLOAT)/24) AS WIP_Dias,
      --
      MAX(
            (
            CASE WHEN CAST(DATEDIFF(hour, F.Endtime, P.Starttime)AS FLOAT) <= 0
                  THEN 0.01
                  ELSE CAST(DATEDIFF(hour, F.Endtime, P.Starttime)AS FLOAT)/24
            END
            )
            /
            (
            CASE 
                  WHEN P.MaxTimeBeforeNextOp IS NOT NULL and P.MaxTimeBeforeNextOp > 0
                  THEN P.MaxTimeBeforeNextOp
                  WHEN P.MaxTimeBeforeNextOp IS NULL AND P.AverageWaitingTime IS NOT NULL and P.AverageWaitingTime > 0
                  THEN P.AverageWaitingTime
            ELSE 1
            END) 
      )AS Dif_WIP
INTO #WIP_EXTERNO
FROM UserData.OrdersExternalPeggingInformation EP
INNER JOIN #TMP_DADOS P ON EP.DatasetId = P.DatasetId AND EP.OrdersId = P.OrdersId
INNER JOIN #TMP_DADOS F ON EP.DatasetId = F.DatasetId AND EP.ExternalPeggingInformation = F.OrdersId
GROUP BY 
      F.DatasetID, 
      P.ordersId, 
      F.Resource, 
      F.Endtime, 
      P.MaxTimeBeforeNextOp,
      P.AverageWaitingTime,
      CASE 
            WHEN P.MaxTimeBeforeNextOp IS NOT NULL THEN P.MaxTimeBeforeNextOp
            WHEN P.MaxTimeBeforeNextOp IS NULL AND P.AverageWaitingTime IS NOT NULL THEN P.AverageWaitingTime
      END
--OUT: SELECT * FROM #WIP_EXTERNO



--CRIA FAIXAS VÁLIDAS PERCENTUAIS
if object_id ('tempdb..#TMP') is not null DROP TABLE #TMP
SELECT 
      WaitingViewId, 
      NAME, 
      FaixaWIPPercentual,
      ROW_NUMBER() OVER (PARTITION BY '1' ORDER BY FaixaWIPPercentual) AS RANK
INTO #TMP
FROM UserData.WaitingView W1
WHERE Ativo = '1'

--select * from #TMP

--ATUALIZA DADOS EM ORDENS
UPDATE UserData.Orders SET WaitingView = NULL

UPDATE UserData.Orders 
SET 
WaitingView = B.WaitingViewId,
WaitingTime = WIP_dias
--SELECT *
FROM userdata.Orders O
INNER JOIN 
(
SELECT * 
 FROM
      (SELECT * FROM #WIP_INTERNO
            UNION ALL 
        SELECT * FROM #WIP_EXTERNO
      )W
LEFT JOIN 
      (
      SELECT 
            T2.WaitingViewId, CAST(T.FaixaWIPPercentual AS FLOAT)/100 AS De, CAST(T2.FaixaWIPPercentual AS FLOAT)/100 AS Ate
      FROM #TMP T
      INNER JOIN #TMP T2 ON T.[RANK] = T2.[RANK]-1
      ) A ON W.Dif_WIP > A.De AND W.Dif_WIP <= a.Ate
)B ON O.DatasetId = B.DatasetId AND O.OrdersId = B.OrdersId




--ATUALIZA ITENS SEM CONSUMO
UPDATE UserData.Orders SET WaitingView = (SELECT WaitingViewId FROM UserData.WaitingView WHERE NAME LIKE '%Sem Consumo%')
WHERE WaitingView IS NULL


