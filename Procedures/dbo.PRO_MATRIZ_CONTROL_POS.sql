ALTER PROCEDURE dbo.PRO_MATRIZ_CONTROL_POS
AS

UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz1 SET SequenceDependentSetupTimeMatriz1 = NULL
UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz2 SET SequenceDependentSetupTimeMatriz2 = NULL
--UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz3 SET SequenceDependentSetupTimeMatriz3 = NULL
--UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz4 SET SequenceDependentSetupTimeMatriz4 = NULL
--UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz5 SET SequenceDependentSetupTimeMatriz5 = NULL


IF OBJECT_ID('tempdb..#TMP_DADOS') IS NOT NULL DROP TABLE #TMP_DADOS
select distinct 
	M.CONJUNTO_ORIGEM as De, 
	M.CONJUNTO_DESTINO as Para,
	(M.TEMPO_PADRAO_PROCESSO/60/24) as Tempo,
	A.RECURSO,
	R.resourcesid,
	a4.MatrizDeSetupRelacionada
INTO #TMP_DADOS
from io.conjunto_montagem M
inner join
	(
	select distinct CODIGO_PRODUTO, CODIGO_OPERACAO, RECURSO from io.GRUPO_RECURSO
		union all
	select distinct CODIGO_PRODUTO, CODIGO_OPERACAO, RECURSO from io.ordem_producao
	where ATRIBUTO_MATRIZ_SETUP is not null and RECURSO is not null
	)A
on m.CODIGO_OPERACAO = A.CODIGO_OPERACAO
inner join io.ORDEM_PRODUCAO O on A.CODIGO_OPERACAO = o.CODIGO_OPERACAO and a.CODIGO_PRODUTO = o.CODIGO_PRODUTO
inner join userdata.resources R on A.RECURSO = R.cdigorecurso
inner join UserData.Workcenter W on r.Workcenter = w.WorkcenterId
inner join userdata.Attribute4 A4 on a.CODIGO_OPERACAO = a4.Name
where o.ATRIBUTO_MATRIZ_SETUP is not null and w.Name <> 'TERCEIROS'

--MATRIZ 1
UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz1 SET SequenceDependentSetupTimeMatriz1 = Tempo
FROM UserData.ResourcesSequenceDependentSetupTimeMatriz1 RS
inner join
	(
	SELECT 
		ResourcesId, 
		Mde.Matriz1Id as De,
		Mpara.Matriz1Id as Para,
		Tempo 
	FROM #TMP_DADOS T
	INNER JOIN userdata.matriz1 Mde on T.de = Mde.name
	INNER JOIN userdata.matriz1 Mpara on T.Para = Mpara.name
	WHERE MatrizDeSetupRelacionada = 'Matriz1'
	)B
on RS.resourcesid = B.ResourcesId and RS.XAxisMatriz1 = B.De AND RS.YAxisMatriz1 = b.Para


--MATRIZ 2
UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz2 SET SequenceDependentSetupTimeMatriz2 = Tempo
FROM UserData.ResourcesSequenceDependentSetupTimeMatriz2 RS
inner join
	(
	SELECT 
		ResourcesId, 
		Mde.Matriz2Id as De,
		Mpara.Matriz2Id as Para,
		Tempo 
	FROM #TMP_DADOS T
	INNER JOIN userdata.matriz2 Mde on T.de = Mde.name
	INNER JOIN userdata.matriz2 Mpara on T.Para = Mpara.name
	WHERE MatrizDeSetupRelacionada = 'Matriz2'
	)B
on RS.resourcesid = B.ResourcesId and RS.XAxisMatriz2 = B.De AND RS.YAxisMatriz2 = b.Para

----MATRIZ 3
--UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz3 SET SequenceDependentSetupTimeMatriz3 = Tempo
--FROM UserData.ResourcesSequenceDependentSetupTimeMatriz3 RS
--inner join
--	(
--	SELECT 
--		ResourcesId, 
--		Mde.Matriz3Id as De,
--		Mpara.Matriz3Id as Para,
--		Tempo 
--	FROM #TMP_DADOS T
--	INNER JOIN userdata.matriz3 Mde on T.de = Mde.name
--	INNER JOIN userdata.matriz3 Mpara on T.Para = Mpara.name
--	WHERE MatrizDeSetupRelacionada = 'Matriz3'
--	)B
--on RS.resourcesid = B.ResourcesId and RS.XAxisMatriz3 = B.De AND RS.YAxisMatriz3 = b.Para

----MATRIZ 4
--UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz4 SET SequenceDependentSetupTimeMatriz4 = Tempo
--FROM UserData.ResourcesSequenceDependentSetupTimeMatriz4 RS
--inner join
--	(
--	SELECT 
--		ResourcesId, 
--		Mde.Matriz4Id as De,
--		Mpara.Matriz4Id as Para,
--		Tempo 
--	FROM #TMP_DADOS T
--	INNER JOIN userdata.matriz4 Mde on T.de = Mde.name
--	INNER JOIN userdata.matriz4 Mpara on T.Para = Mpara.name
--	WHERE MatrizDeSetupRelacionada = 'Matriz4'
--	)B
--on RS.resourcesid = B.ResourcesId and RS.XAxisMatriz4 = B.De AND RS.YAxisMatriz4 = b.Para

----MATRIZ 5
--UPDATE UserData.ResourcesSequenceDependentSetupTimeMatriz5 SET SequenceDependentSetupTimeMatriz5 = Tempo
--FROM UserData.ResourcesSequenceDependentSetupTimeMatriz5 RS
--inner join
--	(
--	SELECT 
--		ResourcesId, 
--		Mde.Matriz5Id as De,
--		Mpara.Matriz5Id as Para,
--		Tempo 
--	FROM #TMP_DADOS T
--	INNER JOIN userdata.matriz5 Mde on T.de = Mde.name
--	INNER JOIN userdata.matriz5 Mpara on T.Para = Mpara.name
--	WHERE MatrizDeSetupRelacionada = 'Matriz5'
--	)B
--on RS.resourcesid = B.ResourcesId and RS.XAxisMatriz5 = B.De AND RS.YAxisMatriz5 = b.Para














