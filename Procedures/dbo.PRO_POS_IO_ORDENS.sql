ALTER PROCEDURE dbo.PRO_POS_IO_ORDENS
AS

--CRIA DADOS BASE
IF OBJECT_ID('tempdb..#TMP_DADOS') IS NOT NULL DROP TABLE #TMP_DADOS
SELECT 
	RG.ResourceGroupId,
	RG.[Name] AS Grupo,
	COUNT(R.[Name]) AS Qtd_Recurso,
	MIN(R.FinitoInfinito) AS ID_MIN,
	MAX(R.FinitoInfinito) AS ID_MAX
INTO #TMP_Dados
FROM UserData.ResourceGroup RG
INNER JOIN UserData.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
INNER JOIN (SELECT ISNULL(finiteorinfinite, 99) AS FinitoInfinito, * FROM UserData.Resources) R ON RGR.Resources = R.ResourcesId
GROUP BY RG.ResourceGroupId, RG.[Name]

--AJUSTA POSSIBILIDADE DE TERCEIRIZAR OU NÃO PARA ORDENS
UPDATE UserData.Orders SET Podeterceirizar = 'Possível Terceirizar'
FROM UserData.Orders O
INNER JOIN
	(
		SELECT R.ResourcesId, t.*, R.[Name],AutomaticSequencing FROM #TMP_Dados T
		INNER JOIN UserData.ResourceGroup RG ON T.ResourceGroupId = RG.ResourceGroupId
		INNER JOIN UserData.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
		INNER JOIN UserData.Resources R ON RGR.Resources = R.ResourcesId AND FiniteOrInfinite IS NULL
		WHERE Qtd_Recurso > 1 AND ID_MIN <99 AND ID_MAX = 99
	)A ON O.ResourceGroup = A.ResourceGroupId

UPDATE UserData.Orders SET Podeterceirizar = 'Produz apenas em Terceiros'
FROM UserData.Orders O
INNER JOIN
	(
		SELECT R.ResourcesId, t.*, R.[Name],AutomaticSequencing FROM #TMP_Dados T
		INNER JOIN UserData.ResourceGroup RG ON T.ResourceGroupId = RG.ResourceGroupId
		INNER JOIN UserData.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
		INNER JOIN UserData.Resources R ON RGR.Resources = R.ResourcesId 
		WHERE ID_MIN = 99 AND ID_MAX = 99
	)A ON O.ResourceGroup = A.ResourceGroupId
	
UPDATE UserData.Orders SET Podeterceirizar = 'Produz apenas Internamente'
FROM UserData.Orders O
INNER JOIN
	(
		SELECT R.ResourcesId, t.*, R.[Name],AutomaticSequencing FROM #TMP_Dados T
		INNER JOIN UserData.ResourceGroup RG ON T.ResourceGroupId = RG.ResourceGroupId
		INNER JOIN UserData.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
		INNER JOIN UserData.Resources R ON RGR.Resources = R.ResourcesId AND FiniteOrInfinite IS NOT NULL
		WHERE ID_MIN <> 99 AND ID_MAX <> 99
	)A ON O.ResourceGroup = A.ResourceGroupId	


--ATUALIZA AUTO SEQ PARA ITENS COM E SEM ROTEIROS

UPDATE UserData.Orders SET SeqAuto = 1

UPDATE UserData.Orders SET SeqAuto = 0
WHERE ResourceGroup = '-1' OR ResourceGroup IS NULL


--ATUALIZA VISÃO SOBRE RECURSOS ALTERNATIVOS
UPDATE UserData.Orders SET possuirecursosalternativos = 'Sem Recursos Alternativos'

UPDATE UserData.Orders SET possuirecursosalternativos = 'Possui Recursos Alternativos'
FROM UserData.Orders O
INNER JOIN
	(
	select distinct RG.ResourceGroupId from
	UserData.ResourceGroup RG
	INNER JOIN UserData.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
	INNER JOIN UserData.Resources R ON RGR.Resources = R.ResourcesId
	group by RG.ResourceGroupId
	having count(R.Name) > 1
	) A on o.ResourceGroup = A.ResourceGroupId
	
--ATUALIZA AUTOMATICAMENTE RANKS DE COMPRIMENTO E BITOLA DE ENTRADA

update UserData.Attribute16  set rank = ROW
from UserData.Attribute16 A16
inner join
	(
	select Name, ROW_NUMBER() OVER(ORDER BY Name) AS Row
	from UserData.Attribute16 --bitola entrada
	)A
	on A16.Name = a.Name

update UserData.Attribute14 set rank = ROW
from UserData.Attribute14 A14
inner join
	(
	select Name, ROW_NUMBER() OVER(ORDER BY Name) AS Row
	from UserData.Attribute14 --COMPRIMENTO
	)A
	on A14.Name = a.Name
