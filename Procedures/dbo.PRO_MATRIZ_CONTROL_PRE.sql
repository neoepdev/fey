ALTER PROCEDURE dbo.PRO_MATRIZ_CONTROL_PRE
AS

--MARCA OPERAÇÕES VÁLIDAS COMO MATRIZES DE SETUP
update UserData.Attribute4 set PossuiMatrizDeSetup = '0'

update UserData.Attribute4 set PossuiMatrizDeSetup = '1'
from UserData.Attribute4 O 
inner join io.CONJUNTO_MONTAGEM C on c.CODIGO_OPERACAO = o.name

update UserData.Attribute4 set MatrizDeSetupRelacionada = ''
WHERE PossuiMatrizDeSetup = '0'

--SELECIONA OPERAÇÕES PARA RELACIONAR A MATRIZ VÁLIDA
IF OBJECT_ID('tempdb..#TMP_MATRIZ_CONTROL') IS NOT NULL DROP TABLE #TMP_MATRIZ_CONTROL
SELECT NAME, Description, MatrizDeSetupRelacionada 
	INTO #TMP_MATRIZ_CONTROL
FROM UserData.Attribute4
WHERE PossuiMatrizDeSetup = '1'
--select * from #TMP_MATRIZ_CONTROL

--GERA DADOS BASE PARA ATUALIZAÇÃO DE RELACIONAMENTO DE MATRIZES
IF OBJECT_ID('tempdb..#TMP_MATRIZES') IS NOT NULL DROP TABLE #TMP_MATRIZES
select 'Matriz1' as Matriz, '1' AS ID into #TMP_MATRIZES
insert into #TMP_MATRIZES values('Matriz2', '2')
insert into #TMP_MATRIZES values('Matriz3', '3')
insert into #TMP_MATRIZES values('Matriz4', '4')
insert into #TMP_MATRIZES values('Matriz5', '5')
--select * from #TMP_MATRIZES order by id

--ATUALIZA VALORES DE MATRIZES RELACIONADAS
DECLARE @Contador AS INTEGER
SET		@Contador = 0
WHILE	@Contador < 10
BEGIN 
	UPDATE #TMP_MATRIZ_CONTROL SET MatrizDeSetupRelacionada = A.Matriz
	FROM
		(
		SELECT TOP 1 Matriz FROM #TMP_MATRIZES M
		LEFT JOIN #TMP_MATRIZ_CONTROL C ON M.Matriz = C.MatrizDeSetupRelacionada
		WHERE C.Name IS NULL
		ORDER BY ID
		)A
	WHERE Name IN 
		(
		 select min(cast(Name as int)) 
		 from #TMP_MATRIZ_CONTROL 
		 WHERE MatrizDeSetupRelacionada = ''
		 )
SET @Contador = @Contador +1	
END

--ATUALIZA NA TABELA DO PREACTOR AS MATRIZES RELACIONADAS AS OPERAÇÕES
update UserData.Attribute4 set MatrizDeSetupRelacionada = B.MatrizDeSetupRelacionada
from UserData.Attribute4 A 
inner join #TMP_MATRIZ_CONTROL B on A.Name = B.Name



------validar atualização de valores para após integração de atributos.
/*
------------------------------------------------------------------------------
------------------------------------------------------------------------------
--INSERE 

SELECT * FROM UserData.Attribute4Matriz1

--LIMPA VALORES DE MATRIZES
UPDATE UserData.Attribute4Matriz1 SET Matriz1 = NULL
UPDATE UserData.Attribute4Matriz2 SET Matriz2 = NULL
UPDATE UserData.Attribute4Matriz3 SET Matriz3 = NULL
UPDATE UserData.Attribute4Matriz4 SET Matriz4 = NULL
UPDATE UserData.Attribute4Matriz5 SET Matriz5 = NULL



-------------------------------
--REVISAR PARA FEY ------------

--UPDATE UserData.Matriz1 
--SET Excluir =
--	( CASE 
--		WHEN 'Matriz1' IN (SELECT Tabela FROM UserData.MatrizControl WHERE ID_Matriz IS NULL)
--		THEN '1' ELSE '0' END)

--UPDATE UserData.Matriz2 
--SET Excluir =
--	( CASE 
--		WHEN 'Matriz2' IN (SELECT Tabela FROM UserData.MatrizControl WHERE ID_Matriz IS NULL)
--		THEN '1'ELSE '0' END)

--UPDATE UserData.Matriz3 
--SET Excluir =
--	( CASE 
--		WHEN 'Matriz3' IN (SELECT Tabela FROM UserData.MatrizControl WHERE ID_Matriz IS NULL)
--		THEN '1'ELSE '0' END)

--UPDATE UserData.Matriz4
--SET Excluir =
--	( CASE 
--		WHEN 'Matriz4' IN (SELECT Tabela FROM UserData.MatrizControl WHERE ID_Matriz IS NULL)
--		THEN '1'ELSE '0' END)

--UPDATE UserData.Matriz5
--SET Excluir =
--	( CASE 
--		WHEN 'Matriz5' IN (SELECT Tabela FROM UserData.MatrizControl WHERE ID_Matriz IS NULL)
--		THEN '1'ELSE '0' END)

		
--IF OBJECT_ID('tempdb..#TMP_DADOS') IS NOT NULL DROP TABLE #TMP_DADOS
--SELECT distinct A.*, MC.ID_Matriz, B.* 
--INTO #TMP_DADOS
--FROM
--(
--	SELECT 'Matriz1' AS ID, NAME FROM UserData.Matriz1 M1
--	UNION ALL
--	SELECT 'Matriz2' AS ID, NAME FROM UserData.Matriz2 M2
--	UNION ALL
--	SELECT 'Matriz3' AS ID, NAME FROM UserData.Matriz3 M3
--	UNION ALL
--	SELECT 'Matriz4' AS ID, NAME FROM UserData.Matriz4 M4
--	UNION ALL
--	SELECT 'Matriz5' AS ID, NAME FROM UserData.Matriz5 M5
--)A 
--	INNER JOIN UserData.MatrizControl MC ON A.ID = MC.Tabela
--	LEFT JOIN 
--		(
--		SELECT ID_Matriz AS ID_Matriz_io, CAST(Centro AS varchar)+'.'+CAST(Atributo_De AS varchar) AS Atributo 
--		FROM IO.Setups
--		)B ON MC.ID_Matriz = B.ID_Matriz_io AND a.[NAME] = b.Atributo
--WHERE B.Atributo IS NULL


UPDATE UserData.Matriz1 SET Excluir = '1'
WHERE [Name] IN (SELECT Name FROM #TMP_DADOS WHERE ID = 'Matriz1')
UPDATE UserData.Matriz2 SET Excluir = '1'
WHERE [Name] IN (SELECT Name FROM #TMP_DADOS WHERE ID = 'Matriz2')
UPDATE UserData.Matriz3 SET Excluir = '1'
WHERE [Name] IN (SELECT Name FROM #TMP_DADOS WHERE ID = 'Matriz3')
UPDATE UserData.Matriz4 SET Excluir = '1'
WHERE [Name] IN (SELECT Name FROM #TMP_DADOS WHERE ID = 'Matriz4')
UPDATE UserData.Matriz5 SET Excluir = '1'
WHERE [Name] IN (SELECT Name FROM #TMP_DADOS WHERE ID = 'Matriz5')



/*
-------------------------------------------------------------------------------
ROTINAS PARA ATUALIZAÇÃO DE VALORES DE TEMPOS NAS MATRIZES DOS DEVIDOS RECURSOS
-------------------------------------------------------------------------------
*/

--Matriz 1
UPDATE UserData.ResourcesMatriz1
SET Matriz1 = (A.Tempo/60)/24
FROM UserData.ResourcesMatriz1 RM1
INNER JOIN 
	(SELECT 
		CT.Centro+'.'+Centro_Trabalho AS Centro_Trabalho, 
		R.ResourcesId, 
		Matriz_Familia,
		Tabela, 
		Atributo_De, 
		M1DE.Matriz1Id ID_De,
		Atributo_Para, 
		M1PARA.Matriz1Id ID_Para,
		Tempo
	FROM [IO].Centros_Trabalho CT
	INNER JOIN [IO].Setups S ON CT.Matriz_Familia = S.ID_Matriz 
	INNER JOIN UserData.MatrizControl MC ON S.ID_Matriz = MC.ID_Matriz
	INNER JOIN UserData.Resources R ON CT.Centro+'.'+Centro_Trabalho = R.[Name]
	INNER JOIN UserData.Matriz1 M1DE ON S.Centro+'.'+S.Atributo_De = M1DE.[Name]
	INNER JOIN UserData.Matriz1 M1PARA ON S.Centro+'.'+S.Atributo_Para = M1PARA.[Name]
	WHERE Tabela = 'Matriz1'
	)A
ON RM1.ResourcesId = A.ResourcesId AND RM1.XAxisMatriz1 = A.ID_De AND RM1.YAxisMatriz1 = A.ID_Para

--Matriz 2
UPDATE UserData.ResourcesMatriz2
SET Matriz2 = (A.Tempo/60)/24
--SELECT *  
FROM UserData.ResourcesMatriz2 RM2
INNER JOIN 
	(SELECT 
		CT.Centro+'.'+Centro_Trabalho AS Centro_Trabalho, 
		R.ResourcesId, 
		Matriz_Familia,
		Tabela, 
		Atributo_De, 
		M2DE.Matriz2Id ID_De,
		Atributo_Para, 
		M2PARA.Matriz2Id ID_Para,
		Tempo
	FROM [IO].Centros_Trabalho CT
	INNER JOIN [IO].Setups S ON CT.Matriz_Familia = S.ID_Matriz 
	INNER JOIN UserData.MatrizControl MC ON S.ID_Matriz = MC.ID_Matriz
	INNER JOIN UserData.Resources R ON CT.Centro+'.'+Centro_Trabalho = R.[Name]
	INNER JOIN UserData.Matriz2 M2DE ON S.Centro+'.'+S.Atributo_De = M2DE.[Name]
	INNER JOIN UserData.Matriz2 M2PARA ON S.Centro+'.'+S.Atributo_Para = M2PARA.[Name]
	WHERE Tabela = 'Matriz2'
	)A
ON RM2.ResourcesId = A.ResourcesId AND RM2.XAxisMatriz2 = A.ID_De AND RM2.YAxisMatriz2 = A.ID_Para

--Matriz 3
UPDATE UserData.ResourcesMatriz3
SET Matriz3 = (A.Tempo/60)/24
FROM UserData.ResourcesMatriz3 RM3
INNER JOIN 
	(SELECT 
		CT.Centro+'.'+Centro_Trabalho AS Centro_Trabalho, 
		R.ResourcesId, 
		Matriz_Familia,
		Tabela, 
		Atributo_De, 
		M3DE.Matriz3Id ID_De,
		Atributo_Para, 
		M3PARA.Matriz3Id ID_Para,
		Tempo
	FROM [IO].Centros_Trabalho CT
	INNER JOIN [IO].Setups S ON CT.Matriz_Familia = S.ID_Matriz 
	INNER JOIN UserData.MatrizControl MC ON S.ID_Matriz = MC.ID_Matriz
	INNER JOIN UserData.Resources R ON CT.Centro+'.'+Centro_Trabalho = R.[Name]
	INNER JOIN UserData.Matriz3 M3DE ON S.Centro+'.'+S.Atributo_De = M3DE.[Name]
	INNER JOIN UserData.Matriz3 M3PARA ON S.Centro+'.'+S.Atributo_Para = M3PARA.[Name]
	WHERE Tabela = 'Matriz3'
	)A
ON RM3.ResourcesId = A.ResourcesId AND RM3.XAxisMatriz3 = A.ID_De AND RM3.YAxisMatriz3 = A.ID_Para


--Matriz 4
UPDATE UserData.ResourcesMatriz4
SET Matriz4 = (A.Tempo/60)/24
FROM UserData.ResourcesMatriz4 RM4
INNER JOIN 
	(SELECT 
		CT.Centro+'.'+Centro_Trabalho AS Centro_Trabalho, 
		R.ResourcesId, 
		Matriz_Familia,
		Tabela, 
		Atributo_De, 
		M4DE.Matriz4Id ID_De,
		Atributo_Para, 
		M4PARA.Matriz4Id ID_Para,
		Tempo
	FROM [IO].Centros_Trabalho CT
	INNER JOIN [IO].Setups S ON CT.Matriz_Familia = S.ID_Matriz 
	INNER JOIN UserData.MatrizControl MC ON S.ID_Matriz = MC.ID_Matriz
	INNER JOIN UserData.Resources R ON CT.Centro+'.'+Centro_Trabalho = R.[Name]
	INNER JOIN UserData.Matriz4 M4DE ON S.Centro+'.'+S.Atributo_De = M4DE.[Name]
	INNER JOIN UserData.Matriz4 M4PARA ON S.Centro+'.'+S.Atributo_Para = M4PARA.[Name]
	WHERE Tabela = 'Matriz4'
	)A
ON RM4.ResourcesId = A.ResourcesId AND RM4.XAxisMatriz4 = A.ID_De AND RM4.YAxisMatriz4 = A.ID_Para


--Matriz 5
UPDATE UserData.ResourcesMatriz5
SET Matriz5 = (A.Tempo/60)/24
FROM UserData.ResourcesMatriz5 RM5
INNER JOIN 
	(SELECT 
		CT.Centro+'.'+Centro_Trabalho AS Centro_Trabalho, 
		R.ResourcesId, 
		Matriz_Familia,
		Tabela, 
		Atributo_De, 
		M5DE.Matriz5Id ID_De,
		Atributo_Para, 
		M5PARA.Matriz5Id ID_Para,
		Tempo
	FROM [IO].Centros_Trabalho CT
	INNER JOIN [IO].Setups S ON CT.Matriz_Familia = S.ID_Matriz 
	INNER JOIN UserData.MatrizControl MC ON S.ID_Matriz = MC.ID_Matriz
	INNER JOIN UserData.Resources R ON CT.Centro+'.'+Centro_Trabalho = R.[Name]
	INNER JOIN UserData.Matriz5 M5DE ON S.Centro+'.'+S.Atributo_De = M5DE.[Name]
	INNER JOIN UserData.Matriz5 M5PARA ON S.Centro+'.'+S.Atributo_Para = M5PARA.[Name]
	WHERE Tabela = 'Matriz5'
	)A
ON RM5.ResourcesId = A.ResourcesId AND RM5.XAxisMatriz5 = A.ID_De AND RM5.YAxisMatriz5 = A.ID_Para


--EXCLUI REGISTROS NAS TABELAS 'MATRIZx'

DELETE UserData.Matriz1 WHERE Excluir = '1'
DELETE UserData.Matriz2 WHERE Excluir = '1'
DELETE UserData.Matriz3 WHERE Excluir = '1'
DELETE UserData.Matriz4 WHERE Excluir = '1'
DELETE UserData.Matriz5 WHERE Excluir = '1'

--EXCLUI REGISTROS NA TABELA ASSOCIADA DE RECURSOS ONDE NÃO MAIS EXISTE O ATRIBUTO
DELETE UserData.Attribute4Matriz1
WHERE XAxisMatriz1 NOT IN (SELECT Matriz1Id FROM UserData.Matriz1)
OR YAxisMatriz1 NOT IN (SELECT Matriz1Id FROM UserData.Matriz1)

DELETE UserData.Attribute4Matriz2
WHERE XAxisMatriz2 NOT IN (SELECT Matriz2Id FROM UserData.Matriz2)
OR YAxisMatriz2 NOT IN (SELECT Matriz2Id FROM UserData.Matriz2)

DELETE UserData.Attribute4Matriz3
WHERE XAxisMatriz3 NOT IN (SELECT Matriz3Id FROM UserData.Matriz3)
OR YAxisMatriz3 NOT IN (SELECT Matriz3Id FROM UserData.Matriz3)

DELETE UserData.Attribute4Matriz4
WHERE XAxisMatriz4 NOT IN (SELECT Matriz4Id FROM UserData.Matriz4)
OR YAxisMatriz4 NOT IN (SELECT Matriz4Id FROM UserData.Matriz4)

DELETE UserData.Attribute4Matriz5
WHERE XAxisMatriz5 NOT IN (SELECT Matriz5Id FROM UserData.Matriz5)
OR YAxisMatriz5 NOT IN (SELECT Matriz5Id FROM UserData.Matriz5)


--ATUALIZA ORDENS ONDE O ATRIBUTO NÃO MAIS EXISTE
UPDATE UserData.Orders SET Matriz1 = NULL 
WHERE Matriz1 NOT IN (SELECT Matriz1Id FROM UserData.Matriz1)

UPDATE UserData.Orders SET Matriz2 = NULL 
WHERE Matriz2 NOT IN (SELECT Matriz2Id FROM UserData.Matriz2)

UPDATE UserData.Orders SET Matriz3 = NULL 
WHERE Matriz3 NOT IN (SELECT Matriz3Id FROM UserData.Matriz3)

UPDATE UserData.Orders SET Matriz4 = NULL 
WHERE Matriz4 NOT IN (SELECT Matriz4Id FROM UserData.Matriz4)

UPDATE UserData.Orders SET Matriz5 = NULL 
WHERE Matriz5 NOT IN (SELECT Matriz5Id FROM UserData.Matriz5)

*/