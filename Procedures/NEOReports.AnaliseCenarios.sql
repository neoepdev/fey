ALTER PROCEDURE NEOReports.AnaliseCenarios

(
@cenario VARCHAR(MAX),
@tipoproduto VARCHAR(MAX),
@ABC VARCHAR(MAX),
@Entrega1 DATETIME,
@Entrega2 DATETIME

)
AS

--DECLARE @cenario VARCHAR(MAX)     SET @cenario='Atual'
--DECLARE @tipoproduto VARCHAR(MAX) SET @tipoproduto='Todos'
--DECLARE @ABC VARCHAR(MAX)		  SET @ABC='Todos'
--DECLARE @Entrega1 DATETIME        SET @Entrega1 = GETDATE() - 15
--DECLARE @Entrega2 DATETIME		  SET @Entrega2 = GETDATE() + 7


--###############################################################################################
-- TEMPORÁRIA E LOOP PARA IDENTIFCAÇÃO DOS DIAS DA SEMANA POR SEMANA
--###############################################################################################



IF OBJECT_ID ('TEMPDB..#TMP_DATES') IS NOT NULL DROP TABLE #TMP_DATES
SELECT
	DATEADD(yyyy, DATEDIFF(yyyy,0,GETDATE()), 0) AS INI
	,DATEADD(YY,1,DATEADD(YYYY, DATEDIFF(yyyy,0,GETDATE()), 0))-1 AS FIM
INTO #TMP_DATES
--SELECT * FROM #TMP_DATES

IF OBJECT_ID ('TEMPDB..#DATES') IS NOT NULL DROP TABLE #DATES
SELECT 
	CAST(FLOOR(CAST(INI AS FLOAT)) AS DATETIME) AS REFDATE
INTO #DATES
FROM #TMP_DATES
--SELECT * FROM #DATES

-- LOOPING START

WHILE (SELECT MAX(REFDATE)+1 FROM #DATES) < (SELECT MAX(CAST(FLOOR(CAST(FIM AS FLOAT)) AS DATETIME)) +1 FROM #TMP_DATES)

BEGIN
	INSERT INTO #DATES
	SELECT 
		MAX(REFDATE)+1 
	FROM #DATES 
END

IF OBJECT_ID ('TEMPDB..#FINAL') IS NOT NULL DROP TABLE #FINAL
SELECT
	*
	,DATEPART(WK,REFDATE) AS Semana
	,DATEPART(DW,REFDATE) AS Dia
INTO #FINAL
--SELECT *
FROM #DATES

--SELECT * FROM #FINAL
--###############################################################################################
-- CONSULTA INICIAL DAS ORDENS DE PRODUÇÃO PROGRAMADAS
--###############################################################################################
IF OBJECT_ID ('tempdb..#ORDENS') IS NOT NULL DROP TABLE #ORDENS
SELECT 
	OD.name AS Cenario
	, O.DatasetId 
	, O.BelongsToOrderNo
	, DENSE_RANK() OVER (PARTITION BY O.DatasetID, O.Orderno ORDER BY O.SetupStart) AS [Rank]
	, ISNULL(P.Name, 'Não especificado') AS CurvaABC
	, PT.[Description] AS TipoProduto
	, O.PartNo AS Produto
	, O.OrderNo AS Ordem
	, O.OpNo AS Operacao
	, DATEPART(wk, O.DueDate) AS SemanaEntrega
	, F.REFDATE AS UltimoDiaSemanaEntrega
	, DATEPART(wk, O.EndTime) AS SemanaProduzida
	--, O.SetupStart
	, O.TotalSetupTime
	, O.DueDate 
	, O.SetupStart
	, O.StartTime
	, O.EndTime
	, O.OrderStart 
	, O.OrderEnd
	, O.TotalProcessTime
	, CASE 
		WHEN O.OrderEnd > O.DueDate
		THEN 'ATRASO'
		ELSE 'NO PRAZO'
	  END AS SituacaoOP
	, CASE 
		WHEN O.OrderEnd > O.DueDate
		THEN 0
		ELSE 1
	  END AS SituacaoOPNumerica
	, CASE 
		WHEN O.OrderEnd > F.REFDATE
		THEN 0
		ELSE 1
	  END AS SituacaoOpNumericaSemana
	, CASE 
		WHEN O.OrderEnd > F.REFDATE 
		THEN ROUND(CAST(O.OrderEnd - F.REFDATE AS FLOAT),2)
		ELSE 0
	  END AS DelayDataEntregaSemana
	, CASE
		WHEN O.OrderEnd > O.DueDate
		THEN ROUND(CAST(O.OrderEnd - O.Duedate AS FLOAT),2)
		ELSE 0
	  END AS DelayDataEntrega
	, CASE 
		WHEN (O.TotalProcessTime + O.TotalSetupTime) > 0
		THEN ROUND(((O.TotalSetupTime / (O.TotalProcessTime + O.TotalSetupTime)) * 100),2)
	  ELSE 0
	  END AS PercentSetup
INTO #ORDENS
FROM UserData.Orders O
	INNER JOIN UserData.Orders_Dataset OD 
		ON O.DatasetId = OD.DatasetId
	LEFT JOIN UserData.Planner P
		ON O.Planner = P.PlannerId
	LEFT JOIN UserData.ProductType PT
		ON O.ProductType = PT.ProductTypeId
	INNER JOIN #FINAL F 
		ON F.Semana =  DATEPART(wk, O.DueDate)
		AND F.Dia = 7
WHERE O.OrderEnd IS NOT NULL
AND O.DueDate >= @Entrega1
AND O.DueDate <= @Entrega2
ORDER BY 
	  PT.[Description]
	, o.OrderNo


-- OUT: SELECT * FROM #ORDENS WHERE CENARIO = 'Atual' ORDER BY ORDEM 

--###############################################################################################
-- CONSULTA PARA CALCULAR TEMPO DE ESPERA ENTRE OPERAÇÕES DE UMA ORDEM DE PRODUÇÃO
--###############################################################################################
IF OBJECT_ID ('tempdb..#PRE_WIP') IS NOT NULL DROP TABLE #PRE_WIP
SELECT 
	  O.Cenario
	, O.Ordem
	, O.Operacao
	, O.[Rank]
--CAMPOS PARA VALIDAÇÃO DOS CÁLCULOS
	--, O.SetupStart
	--, O.StartTime
	--, O.EndTime
	, CASE 
		WHEN O2.Operacao IS NULL
		THEN -1
		ELSE O2.Operacao
	  END AS Operacao2
	, CASE 
		WHEN O2.Rank IS NULL
		THEN -1 
		ELSE O2.Rank
	  END AS Rank2
--CAMPOS PARA VALIDAÇÃO DOS CÁLCULOS
	--, O2.SetupStart
	--, O2.StartTime
	--, O2.EndTime
	, CASE 
		WHEN (CAST(O.SetupStart AS FLOAT) - CAST(O2.EndTime AS FLOAT)) < 0
		THEN 0
		WHEN (CAST(O.SetupStart AS FLOAT) - CAST(O2.EndTime AS FLOAT)) IS NULL
		THEN 0
		ELSE (CAST(O.SetupStart AS FLOAT) - CAST(O2.EndTime AS FLOAT)) 
	  END AS WIP
	--,*
INTO #PRE_WIP
FROM #ORDENS O
	LEFT JOIN #ORDENS O2
		ON O.DatasetId = O2.DatasetId
		AND O.Ordem = O2.Ordem
		AND O.[Rank] = O2.Rank + 1
ORDER BY O.Ordem

--SELECT * FROM #PRE_WIP


--###############################################################################################
-- CONSULTA PARA CALCULAR TEMPO DE ESPERA ENTRE OPERAÇÕES DE UMA ORDEM DE PRODUÇÃO
--###############################################################################################
IF OBJECT_ID ('tempdb..#WIP') IS NOT NULL DROP TABLE #WIP
SELECT 
      Cenario
	, Ordem
	, SUM(WIP) AS WIP
INTO #WIP
FROM #PRE_WIP
GROUP BY 
	Cenario
	,Ordem

--SELECT * FROM #WIP

--###############################################################################################
-- CONSULTA FINAL
--###############################################################################################

SELECT 
	O.Cenario
	, DatasetId
	, TipoProduto
	, ISNULL(CurvaABC, 'Não especificado') AS CurvaABC
	, '' as Produto
	, O.Ordem
	, COUNT(*) AS TotalOperacoes
	, SituacaoOP 
	, SituacaoOPNumerica AS EntregaNoPrazo
	, SituacaoOpNumericaSemana AS EntregaNoPrazoSemana
	--, SUM(SituacaoOPNumerica) AS TotalOrdensAtrasadas
	--, (COUNT(*) - SUM(SituacaoOPNumerica)) AS TotalOrdensNoPrazo
	--, SUM(SituacaoOpNumericaSemana) AS TotalOrdensAtrasadasSemana
	--, (COUNT(*) - SUM(SituacaoOpNumericaSemana)) AS TotalOrdensNoPrazoSemana
	, SUM(o.TotalSetupTime) AS TotalSetupTime
	, SUM(TotalProcessTime) AS TotalTempoProcesso
	, AVG(DelayDataEntrega) AS DelayMedioEntrega
	, AVG(DelayDataEntregaSemana) AS DelayMedioDataEntregaSemana
	--, AVG(PercentSetup)
	, ROUND(WIP.WIP, 2) AS WIP
	, O.OrderEnd AS FinalPrevisto
	, O.DueDate AS DataEntrega
--SELECT *
FROM #ORDENS O
	INNER JOIN #WIP WIP
		ON O.Cenario = WIP.Cenario
		AND O.Ordem = WIP.Ordem
WHERE 
	(CAST(O.Cenario AS VARCHAR(MAX)) IN (select val from FUN_String_To_Table (@cenario, ',', 0)))
	AND 
	(@tipoproduto = 'Todos' OR TipoProduto IN (select val from FUN_String_To_Table (@tipoproduto, ',', 0)))
	AND 
	(@ABC = 'Todos' OR CurvaABC IN (select val from FUN_String_To_Table (@ABC, ',', 0)))
	
GROUP BY 
	  O.Cenario
	, DatasetId
	, TipoProduto
	, CurvaABC
	, O.Ordem
	, WIP.WIP
	, SituacaoOP
	, SituacaoOPNumerica
	, O.OrderEnd
	, O.DueDate
	, SituacaoOpNumericaSemana
ORDER BY 
	  CurvaABC