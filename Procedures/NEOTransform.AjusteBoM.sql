ALTER PROCEDURE NEOTransform.AjusteBoM

AS

--------------------------------------------------------------------------------------------
IF OBJECT_ID ('tempdb..#ORDENS') IS NOT NULL DROP TABLE #ORDENS
SELECT
	OrderNo
	,OpNo
	,OpId
	,A4.Name AS Operacao
	,OperationName
	,BoMID
	--,* 
INTO #ORDENS
--SELECT *
FROM UserData.Orders O 
	LEFT JOIN UserData.Attribute4 A4
		ON O.Attribute4 = A4.Attribute4Id
WHERE DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')
--AND OperationName IN (SELECT Description FROM UserData.Attribute4 WHERE InsumosListaTecnica = 1)
ORDER BY O.OperationName

--SELECT * FROM #ORDENS
----------------------------------------------------------------------------------------------
IF OBJECT_ID ('tempdb..#BOM') IS NOT NULL DROP TABLE #BOM
SELECT
	BoMID
	,RequiredPartNo
	,RequiredPartDescription
	,RequiredQuantity
	,OpNo
	--,*
INTO #BOM
FROM UserData.BillOfMaterials 
WHERE DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')
--SELECT * FROM #BOM
----------------------------------------------------------------------------------------------

IF OBJECT_ID ('tempdb..#TEMP_ATUALIZA_OPERACOES') IS NOT NULL DROP TABLE #TEMP_ATUALIZA_OPERACOES
SELECT
	O.OrderNo
	,O.OpNo
	,E.CODIGO_OPERACAO
	,E.DESCRICAO_FILHO
	,B.OpNo AS OpNoBoM
INTO #TEMP_ATUALIZA_OPERACOES
FROM #ORDENS O
	INNER JOIN IO.ESTRUTURA E
		ON O.OrderNo = E.ORDEM_ITEM_PAI
		AND O.Operacao = E.CODIGO_OPERACAO
	INNER JOIN #BOM B
		ON B.BoMID = O.OrderNo
		AND B.RequiredPartDescription = E.DESCRICAO_FILHO
--AND O.OrderNo = '28383-44'

UPDATE B 
	SET B.OpNo = T.OpNo
--SELECT
--*
FROM UserData.BillOfMaterials B
	INNER JOIN #TEMP_ATUALIZA_OPERACOES T
		ON B.BoMID = T.OrderNo
		AND B.RequiredPartDescription = T.DESCRICAO_FILHO
WHERE B.DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')
