ALTER PROCEDURE dbo.PRO_POS_IO_ESTRUTURAS
AS


------------------------------------------------
--DELETA RELACIONAMENTOS DE OPERAÇÕES CONCLUIDAS, AS QUAIS JÁ CONSUMIRAM MATERIAL
------------------------------------------------

DELETE userdata.BillOfMaterials
--select *
FROM userdata.BillOfMaterials D
INNER JOIN
	(
	SELECT * FROM io.estrutura E --where ORDEM_ITEM_PAI = '600282-33'
	LEFT JOIN 
		(
		SELECT DISTINCT 
			O.orderno, A.name as OP_Cod
		FROM userdata.orders O 
		INNER JOIN userdata.Orders_Dataset D on o.datasetid = d.datasetid and D.name = 'Atual'
		INNER JOIN userdata.attribute4 A on o.Attribute4 = A.Attribute4Id
		)B
	on e.ORDEM_ITEM_PAI = B.orderno and B.OP_Cod = e.codigo_operacao
	WHERE orderno is null
	)C
on D.BoMID = c.ORDEM_ITEM_PAI and d.requiredpartno = c.codigo_filho
