ALTER PROCEDURE dbo.RPT_SUMARY(
	@visao VARCHAR(10),
	@capacidade VARCHAR(10),
	@indicador VARCHAR(10),
	@dataset VARCHAR(100),
	@planejador VARCHAR(1000)
)
AS
BEGIN




--DECLARE	@visao AS VARCHAR(10)
--DECLARE	@capacidade AS VARCHAR(10)
--DECLARE	@indicador VARCHAR(10)
--DECLARE	@dataset VARCHAR(100)
--DECLARE	@planejador VARCHAR(1000)

--SET @visao = 'Mensal'
--SET @capacidade = 'Finita'
--SET @indicador = 'Quantidade'
--SET @dataset = 'Atual'
--SET @planejador = 'Todas'

	SET NOCOUNT ON;

	SELECT
		Dataset,
		Planner,
		ProductTypeName,
		ProductTypeDescription,
		PartNo,
		Product,
		CASE
			WHEN @indicador = 'Quantidade' 
			THEN Quantity
			ELSE OrderValue
		END AS ValorQuantidadeOrdem,
		
			CASE	
				WHEN @visao = 'Diária' AND @capacidade = 'Finita'
				THEN CONVERT(VARCHAR, OrderEnd, 102)
				
				WHEN @visao = 'Semanal' AND @capacidade = 'Finita'
				THEN CONVERT(VARCHAR, YEAR(OrderEnd)) +''+ RIGHT('00'+CONVERT(VARCHAR, MONTH(OrderEnd)), 2) +''+ RIGHT('00'+CONVERT(VARCHAR, DATEPART(wk, OrderEnd)), 2)
				
				WHEN @visao = 'Mensal' AND @capacidade = 'Finita'
				THEN CONVERT(VARCHAR, YEAR(OrderEnd)) +''+ RIGHT('00'+CONVERT(VARCHAR, MONTH(OrderEnd)), 2)
				
				WHEN @visao = 'Diária' AND @capacidade = 'Infinita'
				THEN CONVERT(VARCHAR, DueDate, 102)
				
				WHEN @visao = 'Semanal' AND @capacidade = 'Infinita'
				THEN CONVERT(VARCHAR, YEAR(DueDate)) +''+ RIGHT('00'+CONVERT(VARCHAR, MONTH(DueDate)), 2) +''+ RIGHT('00'+CONVERT(VARCHAR, DATEPART(wk, DueDate)), 2)
				
				WHEN @visao = 'Mensal' AND @capacidade = 'Infinita'
				THEN CONVERT(VARCHAR, YEAR(DueDate)) +''+ RIGHT('00'+CONVERT(VARCHAR, MONTH(DueDate)), 2)
			
			END AS VisaoDataOrdem,
			
			CASE
				WHEN @visao = 'Diária' AND @capacidade = 'Finita'
				THEN dbo.FUN_DATE_TO_DAYMONTH_STR(OrderEnd)			
				
				WHEN @visao = 'Semanal' AND @capacidade = 'Finita'
				THEN dbo.FUN_DATE_TO_WEEK_STR(OrderEnd)
				
				WHEN @visao = 'Mensal' AND @capacidade = 'Finita'
				THEN dbo.FUN_DATE_TO_MONTH_NAME_STR(OrderEnd)
				
				WHEN @visao = 'Diária' AND @capacidade = 'Infinita'
				THEN dbo.FUN_DATE_TO_DAYMONTH_STR(DueDate)			
				
				WHEN @visao = 'Semanal' AND @capacidade = 'Infinita'
				THEN dbo.FUN_DATE_TO_WEEK_STR(DueDate)
				
				WHEN @visao = 'Mensal' AND @capacidade = 'Infinita'
				THEN dbo.FUN_DATE_TO_MONTH_NAME_STR(DueDate)
			
			END AS VisaoDataTexto

	INTO #TMP_DATA
	FROM VIW_ORDERS_DATA
	WHERE 
		ResourceName IS NOT NULL
		AND Dataset = @dataset
		AND
		(
		Planner IN (select val from FUN_String_To_Table (@planejador, ',', 0)) or @planejador = 'Todas'
		)
	AND parent = '-1'

	SELECT 
	Dataset, 
	ProductTypeName,
	ProductTypeDescription,
	Planner, 
	PartNo,
    Product, 
	SUM(ValorQuantidadeOrdem) as ValorQuantidadeOrdem, 
	VisaoDataOrdem, 
	VisaoDataTexto 
	FROM #TMP_DATA
	GROUP BY Dataset, Planner, ProductTypeName, Product, PartNo, VisaoDataOrdem, VisaoDataTexto,ProductTypeDescription

END








