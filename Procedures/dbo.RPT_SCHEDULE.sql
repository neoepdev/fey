ALTER PROCEDURE dbo.RPT_SCHEDULE
(
@dataset		VARCHAR(MAX),
@workcenter		VARCHAR(MAX),
@recursos		VARCHAR(MAX),
@dataini		DATETIME,
@datafim		DATETIME
)
AS
BEGIN
	SET NOCOUNT ON;

		SELECT DISTINCT
			D.Name AS Dataset,
			O.OrderNo,
			O.PartNo,
			O.Product,
			PT.Name AS ProductTypeName,
			O.EarliestStartDate,
			O.DueDate,
			dbo.FUN_DATE_TO_MONTH_STR(O.DueDate) as DeliveryMonth,
			dbo.FUN_DATE_TO_WEEK_STR(O.DueDate) as DeliveryWeek,
			O.Quantity,
			O.OpNo,
			O.OperationName AS OperationName,
			W.Name AS WorkcenterName,
			RG.Name AS ResourceGroupName,
			R.Name AS ResourceName,
			R.DisplaySequenceNumber,
			--PTE.Name AS ResourceCalendar,
			R.DisplayResourceNext,
			R.DisplayResourceBefore,
			ATT.SystemName as DisplayAttribute,
			O.OrderStart,
			O.OrderEnd,
			O.SetupStart,
			O.StartTime,
			O.EndTime,
			dbo.FUN_DATE_TO_MONTH_STR(O.EndTime) as ScheduleMonth,
			dbo.FUN_DATE_TO_WEEK_STR(O.EndTime) as ScheduleWeek,	
			O.Complete,
			O.TotalSetupTime,
			O.TotalProcessTime,
			O.WaitingTime,
			CASE 
				WHEN O.DueDate IS NULL
				THEN NULL
				ELSE (O.OrderEnd - O.DueDate) 
			END AS Delay,
			PA1.Name AS Attribute1Name,
			PA2.Name AS Attribute2Name,
			PA3.Name AS Attribute3Name,
			PA4.Name AS Attribute4Name,
			PA5.Name AS Attribute5Name,
			CAST(D.name AS VARCHAR)+ '.' + CAST(O.OrderNo AS VARCHAR) + '.' + CAST(DENSE_RANK()OVER(PARTITION BY O.OrderNo ORDER BY O.OpNo) AS VARCHAR) AS ID,
			OST.OrderStatusName as StatusDaOrdem,
			O.MaterialShortage,
			ResourceNextDesc as ResourceNext,
			ResourceBeforeDesc as ResourceBefore
		FROM Userdata.Orders O
			INNER JOIN Userdata.Orders_Dataset D
				ON O.DatasetId = D.DatasetId
			INNER JOIN Userdata.ResourceGroup RG
				ON O.ResourceGroup = RG.ResourceGroupId
			INNER JOIN Userdata.Resources R
				ON O.Resource = R.ResourcesId
			LEFT JOIN Userdata.Workcenter W
				ON R.Workcenter = W.WorkcenterId
			LEFT JOIN Userdata.ProductType PT
				ON O.ProductType = PT.ProductTypeId
			LEFT JOIN Userdata.Attribute1 PA1
				ON O.Attribute1 = PA1.Attribute1Id
			LEFT JOIN Userdata.Attribute2 PA2
				ON O.Attribute2 = PA2.Attribute2Id
			LEFT JOIN Userdata.Attribute3 PA3
				ON O.Attribute3 = PA3.Attribute3Id
			LEFT JOIN Userdata.Attribute4 PA4
				ON O.Attribute4 = PA4.Attribute4Id
			LEFT JOIN Userdata.Attribute5 PA5
				ON O.Attribute5 = PA5.Attribute5Id
            LEFT JOIN Calendar.PrimaryCalendarPeriods  PCP
                ON R.ResourcesId = PCP.ResourceId
            LEFT JOIN Calendar.PrimaryTemplates PTE
				ON PCP.TemplateId = PTE.Id
			LEFT JOIN Userdata.Attributes ATT
				ON R.DisplayAttribute = ATT.AttributesID
			LEFT JOIN Userdata.OrderStatus OST
				ON O.OrderStatus = OST.OrderStatusId
			WHERE   
			--(PCP.IsException = 0 and TemplateID IS NOT NULL and FromDate IS NULL and ToDate IS NULL)
			--AND
			(D.Name = @dataset)
			AND 
			ISNULL(Complete,0) = 0
			AND 
			(SetupStart >= @dataini or @dataini IS NULL)
			AND 
			(EndTime <= @datafim or @datafim IS NULL)
			AND
			(
			R.NAME IN (select val from FUN_String_To_Table (@recursos, ',', 0)) or @recursos = 'Todos'
			)
			AND
			(W.Name = @workcenter OR @workcenter = 'Todos')		



	UNION ALL

	SELECT DISTINCT 
			TMP.Dataset AS DatasetName,
			Calendar.CalendarStates.Name as OrderNo,
			'' as PartNo,
			'' as Product,
			'' as ProductTypeName,
			'' as EarliestStartDate,
			'' as DueDate,
			'' as DeliveryMonth,
			'' as DeliveryWeek,
			'' as Quantity,
			'' as OpNo,
			'' as OperationName,
			'Calendar' as WorkcenterName,
			Calendar.CalendarStates.Color as ResourceGroupName,
			Calendar.PrimaryResources.Name as ResourceName,
			0 as DisplaySequenceNumber,
			--'' as ResourceCalendar,
			'' as DisplayResourceNext,
			'' as DisplayResourceBefore,
			'' as DisplayAttribute,
			'' as OrderStart,
			'' AS OrderEnd,
			Calendar.PrimaryCalendarPeriods.FromDate AS SetupStart,
			Calendar.PrimaryCalendarPeriods.FromDate AS StartTime,
			Calendar.PrimaryCalendarPeriods.ToDate AS EndTime,
			'' AS ScheduleMonth,
			'' AS ScheduleWeek,
			'' AS Complete,
			'' AS TotalSetupTime,
			'' AS TotalProcessTime,
			'' AS WaitingTime,
			'' AS Delay,
			'' AS Attribute1Name,
			'' AS Attribute2Name,
			'' AS Attribute3Name,
			'' AS Attribute4Name,
			'' AS Attribute5Name,
			'' AS ID,
			'' AS StatusDaOrdem,
			'' AS MaterialShortage,
			'' as ResourceNext,
			'' as ResourceBefore
			FROM         
					Calendar.PrimaryCalendarPeriods INNER JOIN
					Calendar.PrimaryResources ON Calendar.PrimaryCalendarPeriods.ResourceId = Calendar.PrimaryResources.Id INNER JOIN
					Calendar.CalendarStates ON Calendar.PrimaryCalendarPeriods.StateId = Calendar.CalendarStates.Id
			INNER JOIN
			(
			select
			Dataset, ResourceName as RecBase, WorkcenterName, Min(SetupStart) as MinSchedule, Max(EndTime) as MaxSchedule 
			from viw_orders_data
			where Dataset = @dataset
			group by dataset, Workcentername, ResourceName
			) TMP
			ON TMP.RecBase = Calendar.PrimaryResources.Name
			WHERE   (Calendar.PrimaryCalendarPeriods.IsException = 1 AND Calendar.PrimaryCalendarPeriods.TemplateId IS NULL )
			and     (@recursos = 'Todos' or Calendar.PrimaryResources.Name = @recursos)
			and     (WorkcenterName = @workcenter OR @workcenter = 'Todos')
			and		Calendar.PrimaryCalendarPeriods.FromDate >
					(
					SELECT MIN (O.SETUPSTART) 
					FROM Userdata.Orders O INNER JOIN 
						 Userdata.Orders_Dataset D ON O.DatasetId = D.DatasetId
					WHERE D.NAME = @dataset
					)
			and
			Calendar.PrimaryCalendarPeriods.FromDate >= MinSchedule
			and 
			Calendar.PrimaryCalendarPeriods.ToDate <= MaxSchedule
			and
			(Calendar.PrimaryCalendarPeriods.FromDate IS NOT NULL and Calendar.PrimaryCalendarPeriods.ToDate IS NOT NULL )
			and
			Calendar.CalendarStates.Efficiency = 0



END

















