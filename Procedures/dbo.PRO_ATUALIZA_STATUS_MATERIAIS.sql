ALTER  PROCEDURE dbo.PRO_ATUALIZA_STATUS_MATERIAIS
AS


----------------------------------
--ATUALIZA DEMAND E SUPPLY DELAY--
----------------------------------

UPDATE userdata.orders SET DemandDelay = DATEDIFF(dd, endtime, demanddate) WHERE demanddate IS NOT NULL AND endtime IS NOT NULL
UPDATE userdata.orders SET SupplyDelay = DATEDIFF(dd, endtime, supplydate) WHERE supplydate IS NOT NULL AND endtime IS NOT NULL

-------------------------------------
--ATUALIZA DATA DE INICIO MAIS CEDO--
-------------------------------------

UPDATE O
SET EarliestStartDate = DATEADD(DAY,MinDIMC,DataCadastro)
FROM UserData.Orders O
INNER JOIN 
	(
	SELECT 
		ResourceGroupId
		,MIN(R.DIMC) AS MinDIMC
	FROM UserData.ResourceGroupResources RGR 
		INNER JOIN Userdata.Resources R on rgr.Resources = r.ResourcesId
	GROUP BY ResourceGroupId
	)A
ON o.ResourceGroup = A.ResourceGroupId
INNER JOIN UserData.Orders_Dataset D ON O.DatasetId = D.DatasetId
WHERE d.name = 'Atual'

----------

UPDATE userdata.orders SET ActualEarliestStartDate = 
	CASE 
		WHEN (SELECT IgnoreSupplyDate FROM Userdata.PeggingRuleSet) = '1' THEN EarliestStartDate
		WHEN (SELECT IgnoreSupplyDate FROM Userdata.PeggingRuleSet) = '0' AND ISNULL(DemandDate,0) > ISNULL(EarliestStartDate,0) THEN DemandDate
		WHEN (SELECT IgnoreSupplyDate FROM Userdata.PeggingRuleSet) = '0' AND ISNULL(DemandDate,0) <= ISNULL(EarliestStartDate,0) THEN EarliestStartDate
	END 

----------------------------
--ATUALIZA DATA DE ENTREGA--
----------------------------

--UPDATE UserData.Orders SET ActualDueDate = 
--CASE 
--	WHEN DueDate IS NULL AND SupplyDate = NULL THEN NULL
--	WHEN DueDate > 0 AND  SupplyDate > DueDate THEN DueDate
--	WHEN SupplyDate > 0 AND  SupplyDate <= DueDate THEN SupplyDate
--	WHEN DueDate IS NULL THEN SupplyDate
--	WHEN SupplyDate IS NULL THEN DueDate
--END 

UPDATE UserData.Orders SET ActualDueDate = ISNULL(SupplyDate, DueDate)

------------------------------
-- ATUALIZA STATUS DA DEMANDA --
------------------------------

UPDATE UserData.Orders SET DemandStatus = 
	CASE
		WHEN MaterialControlComplete = '0' AND MaterialShortage = '0' THEN '2'
		WHEN MaterialControlComplete = '1' AND MaterialShortage = '0' AND MaterialOverSupply = '0' THEN '3'
		WHEN MaterialControlComplete = '1' AND MaterialShortage = '1' AND MaterialOverSupply = '0' THEN '4'
		WHEN MaterialControlComplete = '0' AND MaterialShortage = '1' AND MaterialOverSupply = '0' THEN '5'
		WHEN MaterialControlComplete = '1' AND MaterialShortage = '0' AND MaterialOverSupply = '1' THEN '6'
		WHEN MaterialControlComplete = '0' AND MaterialShortage = '1' AND MaterialOverSupply = '1' THEN '7'
		WHEN MaterialControlComplete = '1' AND MaterialShortage = '1' AND MaterialOverSupply = '1' THEN '8'	
	END

-------------------------------------
-- ATUALIZA INDICADOR DE SUPRIMENTOS --
-------------------------------------

UPDATE UserData.Orders SET MaterialSourceView = NULL

UPDATE UserData.Orders SET MaterialSourceView = MaterialSourceViewId
FROM UserData.Orders O INNER JOIN 
	(
	SELECT DatasetId, OrderNo, I.MaterialSourceViewId 
	FROM
		(
		SELECT DISTINCT 
			O.DatasetId, 
			O.OrdersId,
			O.orderNO, 
			MAX(I.[Rank]) AS RANK
		FROM UserData.Orders O
		INNER JOIN UserData.OrderLinks L 
			ON O.DatasetId = L.DatasetId 
			AND O.OrdersId = L.ToInternalDemandOrder
		INNER JOIN UserData.Supply S 
			ON L.DatasetId = S.DatasetId 
			AND L.FromExternalSupplyOrder = S.SupplyId
		INNER JOIN UserData.MaterialSourceView I 
			ON S.MaterialSource = I.MaterialSourceViewId
		GROUP BY O.OrdersId, O.DatasetId,O.OrderNO
		)A
	INNER JOIN UserData.MaterialSourceView I ON a.[RANK] = I.[Rank]
	)B ON O.DatasetId = B.DatasetId AND O.OrderNo = B.OrderNo
	
UPDATE UserData.Orders SET MaterialSourceView = (SELECT MaterialSourceViewId FROM userdata.MaterialSourceView WHERE [Name] = 'N')
WHERE MaterialSourceView IS NULL

-------------------------------------
--        CRIA DADOS GERAIS        --
-------------------------------------

if object_id ('tempdb..#TMP_LINK') is not null DROP TABLE #TMP_LINK
SELECT
	D.DatasetId,
	O.OrdersId,
	O.OrderNo,
	O.MaterialShortage,
	O.Quantity AS OrderQty,
	MIN(O.StartTime) AS OrderStartTime,
	--ESTRUTURA
	B.RequiredQuantity AS RequiredProp,
	CASE WHEN B.IgnoreShortages = 1 THEN '1' ELSE '0' END AS IgnoreShortages,
	ISNULL(B.RequiredQuantity,'0') * O.Quantity AS RequiredQty,
	ISNULL(SUM(L.Quantity),'0') AS LinkedQty, 
	(ISNULL(B.RequiredQuantity,'0') * O.Quantity) - ISNULL(SUM(L.Quantity),'0') AS ShortageQty,
	--FORNECIMENTO
	S.SupplyId,
	S.SupplyDate,
	DATEDIFF(dd, S.SupplyDate, MIN(O.StartTime)) as SupplyDelay,
	I.Name as SuprimentosID,
	ISNULL(I.Rank,99) as MaterialSupplyTypeDetRank
INTO #TMP_LINK
FROM UserData.Orders_Dataset D 
INNER JOIN UserData.Orders O ON  D.datasetId = O.DatasetId 
LEFT OUTER JOIN UserData.BillOfMaterials B ON O.DatasetId = B.DatasetId 
	AND O.BoMID= B.BoMID 
	AND	O.OpNo = B.OpNo
LEFT OUTER JOIN UserData.OrderLinks L ON O.DatasetId = L.DatasetId 
	AND O.OrdersId = L.ToInternalDemandOrder 
	AND (B.RequiredPartNo = L.PartNo OR B.RequiredPartNo IS NULL)
LEFT OUTER JOIN	UserData.Supply S ON O.DatasetId = S.DatasetId 
	AND L.FromExternalSupplyOrder = S.SupplyId
LEFT OUTER JOIN Userdata.MaterialSourceView I ON I.MaterialSourceViewId = S.MaterialSource
WHERE 
	B.RequiredPartNo IS NOT NULL
	AND D.name = 'Atual'
GROUP BY 
	d.DatasetID,
    O.OrderNo,
	O.OrdersId,
	S.SupplyID,
	S.SupplyDate,
	S.OrderType,
	B.RequiredPartNo, 
	B.RequiredQuantity,
	B.IgnoreShortages,
	O.Quantity, 
	O.OpNo,
	O.MaterialShortage,
	I.NAME,
	I.Rank

--OUT:SELECT * FROM #TMP_LINK

UPDATE UserData.Orders SET SupplyDelay = A.SupplyDelay
FROM UserData.Orders 
INNER JOIN
	(
	SELECT DatasetId, OrdersId, OrderNo, MIN(SupplyDelay) AS SupplyDelay
	FROM #TMP_LINK
	WHERE SupplyDelay IS NOT NULL
	GROUP BY DatasetId, OrdersId, OrderNo
	)A
ON UserData.Orders.DatasetId = A.DatasetId AND UserData.Orders.OrderNo = A.OrderNo


-------------------------------------
--  ATUALIZA STATUS QUANTIDADES    --
-------------------------------------

-- CRIA TEMP DE DADOS
if object_id ('tempdb..#TMP_DADOS') is not null DROP TABLE #TMP_DADOS
SELECT DISTINCT DatasetID, ORDERSID, ORDERNO, MaterialShortage, ShortageQty,IgnoreShortages, 'Não Especificado ' AS ID
INTO #TMP_DADOS
FROM #TMP_LINK T
	--AJUSTA DADOS EM FUNÇÃO DO CONSUMO DE MATERIAIS
UPDATE #TMP_DADOS SET ID = A.ID
FROM #TMP_DADOS D INNER JOIN 
    (
    SELECT DatasetID, ORDERSID, ORDERNO, 'QTD OK' AS ID
	FROM #TMP_LINK T
		WHERE MaterialShortage = '0'
	GROUP BY DatasetID, ORDERSID, ORDERNO
	
	UNION ALL
	
    SELECT DatasetID, ORDERSID, ORDERNO, 
		--CASE 
		--	WHEN MIN(ShortageQty) > 0 THEN 'QTD NOK'
		--	WHEN MIN(ShortageQty) = 0 THEN 'QTD PARCIAL'
		--END AS ID
		'QTD NOK' AS ID
	FROM #TMP_LINK T
	WHERE MaterialShortage = '1'
	GROUP BY DatasetID, ORDERSID, ORDERNO
	)A
ON D.DatasetID = A.DatasetID AND D.ORDERNO = A.ORDERNO
WHERE D.ID = 'Não Especificado '

--CORRIGE ITENS JIT
UPDATE #TMP_DADOS SET ID = A.ID
FROM #TMP_DADOS D
INNER JOIN 
	(
	SELECT DISTINCT 
		DatasetID, 
		ORDERSID, 
		ORDERNO,
		MIN(IgnoreShortages) AS IgnoreShortage,
		CASE 
			--WHEN MIN(IgnoreShortages) = '1' AND MIN(ID) = 'QTD PARCIAL' THEN 'QTD PARCIAL JIT'
			WHEN MIN(IgnoreShortages) = '1' AND MIN(ID) = 'QTD NOK' THEN 'QTD OK EXCETO JIT'
		ELSE MIN(ID)
		END AS ID
	FROM #TMP_DADOS
	WHERE ShortageQty <> 0
	GROUP BY 	DatasetID, ORDERSID, ORDERNO
	)A
ON D.Orderno = A.ORDERNO AND D.Datasetid = A.Datasetid 

--ATUALIZA STATUS EM ORDENS
UPDATE UserData.Orders SET MaterialQuantityView = A.MaterialQuantityViewId
FROM UserData.Orders O INNER JOIN 
	(
	SELECT B.*, ss.MaterialQuantityViewId 
	FROM 
		(	
		    SELECT * FROM #TMP_DADOS
		)B	
	INNER JOIN UserData.MaterialQuantityView SS ON B.ID = SS.name
	)A 
ON O.Orderno = A.Orderno AND o.DatasetId = A.DatasetId

--ATUALIZA STATUS DE QUEM NÃO REQUISITA MATERIAIS DE ESTOQUES OU COMPRAS
UPDATE UserData.Orders SET MaterialQuantityView = (SELECT MaterialQuantityViewId FROM UserData.MaterialQuantityView WHERE [Name] = 'NÃO REQUISITA')
WHERE MaterialQuantityView IS NULL AND DatasetId IN (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')

-------------------------------------
--     ATUALIZA STATUS DATAS       --
-------------------------------------

UPDATE UserData.Orders SET MaterialDateView = NULL
WHERE DatasetId IN (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')

UPDATE UserData.Orders SET MaterialDateView = MaterialDateViewId
FROM UserData.Orders O 
INNER JOIN 
	(
	SELECT A.*, SD.[Name], SD.MaterialDateViewId FROM 
		(
		SELECT DISTINCT DatasetID, ORDERSID, ORDERNO, MIN(SupplyDelay) AS SupplyDelay
		FROM #TMP_LINK T
		WHERE SupplyDelay IS NOT NULL 
		GROUP BY DatasetID, ORDERSID, ORDERNO		
		)A
	LEFT JOIN UserData.MaterialDateView SD 
		ON A.SupplyDelay >= SD.FaixaDelayDe 
		AND A.SupplyDelay <= SD.FaixaDelayPara 
		AND sd.ativarfaixa = '1'
	)B
ON o.OrderNo = b.Orderno 
AND o.DatasetId = b.DatasetID

UPDATE UserData.Orders SET MaterialDateView = (SELECT MaterialDateViewId FROM UserData.MaterialDateView WHERE [Name] = 'Ordem Não Programada')
WHERE Resource IS NULL AND DatasetId IN (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')

UPDATE UserData.Orders SET MaterialDateView = (SELECT MaterialDateViewId FROM UserData.MaterialDateView WHERE [Name] = 'Fornecimento Inexistente')
WHERE MaterialDateView IS NULL AND DatasetId IN (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')


--------------------------------------------
--ATUALIZA ANÁLISE COMBINADA DE MATERIAIS --
--------------------------------------------

UPDATE UserData.Orders SET SupplyDelay = A.SupplyDelay
FROM UserData.Orders O
INNER JOIN
	(
	SELECT distinct DatasetId, Orderno, MIN(SupplyDelay) AS SupplyDelay FROM UserData.Orders
	GROUP BY DatasetId, Orderno
	)A
ON A.datasetid = o.datasetid AND A.Orderno = O.Orderno

if object_id ('tempdb..#TMP_ACOMB') is not null DROP TABLE #TMP_ACOMB
SELECT DISTINCT
	DatasetId, 
	OrdersId,
	ORDERNO,
	SupplyDelay,
	CASE 
		WHEN S.[Name] = 'N' AND Q.[Name] = 'QTD NOK' THEN 'FALTA TOTAL'
		WHEN S.[Name] = 'N' AND Q.[Name] <> 'QTD NOK' THEN 'NÃO REQUISITA'
		WHEN S.[Name] <> 'N' AND (Q.[Name] = 'QTD NOK' OR SupplyDelay < 0) THEN S.[Name]+'.MATERIAL NOK'
		WHEN S.[Name] <> 'N' AND Q.[Name] = 'QTD OK' THEN S.[Name]+'.MATERIAL OK'
		WHEN S.[Name] <> 'N' AND Q.[Name] LIKE '%JIT%' THEN S.[Name]+'.MATERIAL OK EXCETO JIT'
	END AS StatusMaterial,
	S.[Name] AS MaterialSourceView,
	Q.[Name] AS MaterialQuantityView, 
	D.[Name] AS MaterialDateView
INTO #TMP_ACOMB
FROM UserData.Orders O
LEFT JOIN UserData.MaterialSourceView S 
	ON O.MaterialSourceView = S.MaterialSourceViewId
LEFT JOIN UserData.MaterialDateView D 
	ON O.MaterialDateView = D.MaterialDateViewId
LEFT JOIN UserData.MaterialQuantityView Q  
	ON O.MaterialQuantityView = Q.MaterialQuantityViewId
--OUT: select * from #TMP_ACOMB where analisecomb = 'C.MATERIAL NOK' and MaterialDateView <> 'Atrasada'


UPDATE UserData.Orders SET MaterialCombinedView = B.MaterialCombinedViewId
FROM UserData.Orders O
INNER JOIN
	(
	 SELECT DatasetId, OrdersId, StatusMaterial, A.MaterialCombinedViewId, Orderno
	 FROM #TMP_ACOMB T
	 INNER JOIN UserData.MaterialCombinedView A 
		ON T.StatusMaterial = A.[Name]
	)B
ON O.DatasetId = B.DatasetId 
AND O.Orderno = B.Orderno


