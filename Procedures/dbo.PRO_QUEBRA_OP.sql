ALTER PROCEDURE dbo.PRO_QUEBRA_OP
AS

TRUNCATE TABLE dbo.QUANTIDADEPORPERIODO

IF OBJECT_ID ('TEMPDB..#ORDENS') IS NOT NULL DROP TABLE #ordens
SELECT 
	od.datasetid AS Cenario
	, o.OrdersId AS operation
	, R.ResourcesId AS ID_recurso
	, R.Name AS recurso
	, o.SetupStart AS inicio_setup
	, o.SetupStart AS inicio_oper
	, o.EndTime AS fim
INTO #ORDENS
FROM UserData.Orders o
	INNER JOIN UserData.Orders_Dataset od
		ON o.DatasetId = od.DatasetId AND od.name = 'Atual'
	INNER JOIN UserData.Resources r
		ON o.Resource = r.ResourcesId

TRUNCATE TABLE IO.estados_calendarios_recursos

IF OBJECT_ID ('TEMPDB..#TEMP_1') IS NOT NULL DROP TABLE #TEMP_1
SELECT ResourcesId, Name, data as Inicio 
INTO #TEMP_1
FROM FT_retorna_dias(CAST(CAST(GETDATE()-90 AS DATE) AS DATETIME) + 0.0001,360) AS INICIO
	CROSS JOIN Userdata.Resources
order by ResourcesId, data

IF OBJECT_ID ('TEMPDB..#TEMP_2') IS NOT NULL DROP TABLE #TEMP_2
SELECT ResourcesId, Name, data as Fim 
INTO #TEMP_2
FROM FT_retorna_dias(CAST(CAST(GETDATE()-90 AS DATE) AS DATETIME) + 0.9999,360) AS FIM 
	CROSS JOIN Userdata.Resources
order by ResourcesId, data

INSERT INTO IO.estados_calendarios_recursos
SELECT T1.ResourcesId, T1.Name, 'Turno', CAST('1900-01-01' AS DATETIME), Inicio, Fim
FROM #TEMP_1 T1
	INNER JOIN #TEMP_2 T2
		ON T1.ResourcesId = T2.ResourcesId
		AND CONVERT(VARCHAR(10),T1.Inicio,120) = CONVERT(VARCHAR(10),T2.Fim,120) 


INSERT INTO QUANTIDADEPORPERIODO
SELECT DISTINCT
	OPERATION
	, E.RECURSO
	, E.ESTADO
	, E.INICIO_ESTADO
	, E.FIM_ESTADO
	, NULL
FROM #ORDENS O
	INNER JOIN IO.ESTADOS_CALENDARIOS_RECURSOS E 
		ON O.RECURSO = E.RECURSO
		AND
			(
				(E.INICIO_ESTADO BETWEEN O.inicio_setup AND O.fim) 
			OR 
				(E.FIM_ESTADO BETWEEN  O.inicio_setup AND O.fim)
			OR
				(
				(O.inicio_setup BETWEEN E.INICIO_ESTADO AND E.FIM_ESTADO) 
				AND (O.Fim BETWEEN E.INICIO_ESTADO AND E.FIM_ESTADO)
				 )
			 )
			 
-- WHERE e.recurso = '411.LIMPEZA POR JATEAMENTO'

 --SELECT * FROM #ORDENS O order by recurso, inicio_oper
 --SELECT * FROM io.ESTADOS_CALENDARIOS_RECURSOS order by recurso, inicio_estado
	 
-- select * from QUANTIDADEPORPERIODO where operacao = 374523 order by INICIO
-- select * from QUANTIDADEPORPERIODO where recurso = '2109.NEDSCHROEF BR 303' order by INICIO
-- select * from QUANTIDADEPORPERIODO where recurso = '411.LIMPEZA POR JATEAMENTO' order by INICIO

--select Quantity, * from userdata.orders
--where ordersid = 374523

--select CAST(CAST(GETDATE()-7 AS DATE) AS DATETIME) + 0.0001