ALTER PROCEDURE dbo.PRO_LIMPA_RECURSOS_ANTIGOS
AS

-- CRIA TEMPORÁRIA COM RECURSOS ANTIGOS
IF OBJECT_ID ('tempdb..#LimpaAtt17') IS NOT NULL DROP TABLE #LimpaAtt17
SELECT DISTINCT 
	YaxisResources
INTO #LimpaAtt17
FROM UserData.Attribute17ValidResources
WHERE YAxisResources NOT IN (SELECT DISTINCT ResourcesId from UserData.Resources)
 
-- LIMPA RECURSOS ANTIGOS DA MATRIZ
DELETE FROM UserData.Attribute17ValidResources
WHERE YaxisResources IN (SELECT * FROM #LimpaAtt17)