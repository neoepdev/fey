ALTER  PROCEDURE dbo.RPT_IMPACTOS_REPROG

(
@dataset_base			    VARCHAR(MAX),
@dataset_rescheduled		VARCHAR(MAX)
)

AS

BEGIN

--PREPARA DADOS DE ENTRADA
if object_id ('tempdb..#TMP_Dados') is not null DROP TABLE #TMP_Dados
SELECT     
	D.Name AS Dataset,
	o.idregistro AS Ordem,
	MAX(O.PartNo) AS Produto,
	MAX(O.Product) AS Descricao,
	PT.Name AS TipoProduto,    
	CONVERT(varchar(10),MIN(O.EarliestStartDate),103) as Data_Inicio, 
	CONVERT(varchar(10),MAX(O.DueDate),103) as Data_Entrega, 
	CONVERT(varchar(10),MIN(O.OrderStart),103) as Inicio_Ordem, 
	CONVERT(varchar(10),MAX(O.EndTime),103) as Fim_Ordem, 
	SUM(O.TotalSetupTime)*24 as HorasSetup, 
	SUM(O.TotalProcessTime)*24 as HorasProdução, 
	MAX(O.Quantity) as Qnt_Pecas, 
--CÁLCULO KPI DELAY
	CAST(MAX(O.DueDate) - MAX(O.EndTime) AS REAL) AS CalcDelayDias,
--CÁLCULO KPI LEADTIME
	CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL) AS CalcLeadtimeDias,
--CÁLCULO KPI AGREGAÇÃO DE VALOR
	CASE 
		WHEN CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL ) > 0
		THEN 100*(CAST(SUM(O.TotalProcessTime) AS REAL)/ CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL )) 
		ELSE 0
	END AS CalcAgregacaoPercentual,
--CÁLCULO KPI PARTICIPACAO DE SETUP
	CASE
		WHEN SUM(O.TotalProcessTime) + SUM(O.TotalSetupTime) > 0
		THEN 100*(SUM(O.TotalSetupTime)/(SUM(O.TotalProcessTime) + SUM(O.TotalSetupTime)))
	END AS CalcSetupPercentual,
--CÁLCULO KPI SITUACAO ENTREGA
   CASE  
         WHEN CAST(MAX(O.DueDate) - MAX(O.EndTime) AS REAL) < 0 
         THEN 'Atrasada'
         WHEN CAST(max(O.DueDate) - max(O.EndTime) AS REAL) > 0
         THEN 'Em Dia'
		 ELSE 'Não Programada'
	END AS SituacaoEntrega,
--CÁLCULO KPI ATRASADA
   CASE  
         WHEN  CAST(max(O.DueDate) - max(O.EndTime) AS REAL) < 0 THEN 0
         WHEN  CAST(max(O.DueDate) - max(O.EndTime) AS REAL) > 0 THEN 1
   ELSE ''
   END AS OrdemAtrasada
INTO #TMP_Dados
FROM USERDATA.ORDERS O
INNER JOIN Userdata.Orders_Dataset D ON O.DatasetId = D.DatasetId
LEFT JOIN Userdata.ProductType PT ON O.ProductType = PT.ProductTypeId
WHERE 
	O.EndTime IS NOT NULL AND D.NAME = @dataset_base
	OR
	O.EndTime IS NOT NULL AND D.NAME = @dataset_rescheduled
GROUP BY
	D.Name,
	o.idregistro,
	PT.Name


SELECT
	  OS1.*,
	  OS2.Dataset as Dataset2,
	  OS2.Ordem as Ordem2,
	  OS2.Produto as Produto2,
	  OS2.Descricao as Descricao2,
	  OS2.Tipoproduto as TipoProduto2,
	  OS2.Data_Inicio as Data_Inicio2,
	  OS2.Data_Entrega as Data_Entrega2,
	  OS2.Inicio_Ordem as Inicio_Ordem2,
	  OS2.Fim_Ordem as Fim_Ordem2,
	  OS2.HorasSetup as HorasSetup2,
	  OS2.HorasProdução as HorasProdução2,
	  OS2.Qnt_Pecas as Qnt_Pecas2,
	  OS2.CalcDelayDias as CalcDelayDias2,
	  OS2.CalcLeadTimeDias as CalcLeadTimeDias2,
	  OS2.CalcAgregacaoPercentual as CalcAgregacaoPercentual2,
	  OS2.SituacaoEntrega as SituacaoEntrega2,
	  OS2.OrdemAtrasada as OrdemAtrasada2,
	  CASE 
		WHEN cast (OS1.Ordematrasada as int)>  cast(OS2.Ordematrasada as int) THEN 'Atrasou'               
		WHEN cast(OS1.Ordematrasada as int) <  cast(OS2.Ordematrasada as int) THEN 'Adiantou'
		WHEN cast(OS1.Ordematrasada as int) =  cast(OS2.Ordematrasada as int) THEN  'NA'
	  END AS Status
FROM VIW_ORDERS_IMPACTS OS1
INNER JOIN VIW_ORDERS_IMPACTS OS2 
	ON OS1.Ordem = OS2.Ordem
	AND OS1.Produto = OS2.Produto 
	AND OS1.Dataset = @dataset_base
	AND OS2.Dataset = @dataset_rescheduled

END