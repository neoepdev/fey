ALTER PROCEDURE dbo.RPT_RESOURCE_LOAD

(
	@Dataset1 VARCHAR(200),
	@Dataset2 VARCHAR(200),
	@De DATETIME,
	@Ate DATETIME
)

AS
BEGIN

SELECT 
	Dataset,
	ResourceName,
	ISNULL(WorkcenterName,'Recursos Sem Workcenter') AS WorkcenterName,
	count(ReferenceDay) * 24 as TotalHorizonHs,
	sum(TotalAvailableHs) as TotalAvailableHs,
	sum(TotalLoadHs) as TotalLoadHs,
	sum(SetupLoadHs) as SetupLoadHs,
	sum(OperationLoadHs) as OperationLoadHs,
	CASE 
		WHEN sum(ocupationperc) > 0
		THEN sum(ocupationperc) / count (referencedate) 
		ELSE 0
	END as AvgOcupationPerc
INTO #RESOURCE_LOAD
FROM VIW_RESOURCE_LOAD
WHERE 
Dataset IN (@Dataset1, @Dataset2) 
and
ReferenceDate >= @De
and
ReferenceDate <= @Ate
GROUP BY
Dataset,
WorkcenterName,
ResourceName



SELECT 
Dataset,
ResourceName,
count(orderno) as NoOrders,
sum(Quantity) as TotalQuantity,
sum(WaitingTime) as TotalWaitingTime,
MIN(OrderStart) AS MinOrderStart,
MAX(OrderEnd) AS MaxOrderEnd
INTO #ORDERS_DATA
FROM VIW_ORDERS_DATA
WHERE
Dataset IN (@Dataset1, @Dataset2) 

GROUP BY
Dataset,
ResourceName


SELECT *
FROM #RESOURCE_LOAD RL
INNER JOIN #ORDERS_DATA OD
ON RL.Dataset = OD.Dataset and RL.ResourceName = OD.ResourceName


END





