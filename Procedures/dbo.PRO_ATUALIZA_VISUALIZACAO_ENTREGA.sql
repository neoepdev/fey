ALTER PROCEDURE dbo.PRO_ATUALIZA_VISUALIZACAO_ENTREGA

AS

BEGIN TRANSACTION 

UPDATE UserData.Orders SET DeliveryView = (SELECT DeliveryViewId FROM UserData.DeliveryView WHERE [Name] = 'Não programada')
WHERE  UserData.Orders.Resource = NULL

UPDATE UserData.Orders SET DeliveryView = 
	CASE 
		WHEN Datediff(day,ActualDueDate,OrderEnd) >= (SELECT QuantidadeDeDias from UserData.DeliveryView WHERE [Name] = 'Atraso Grande')
		THEN (SELECT DeliveryViewId from UserData.DeliveryView WHERE [Name] = 'Atraso Grande')
		--	
		WHEN Datediff(day,ActualDueDate,OrderEnd) >= (SELECT QuantidadeDeDias from UserData.DeliveryView WHERE [Name] = 'Atraso Médio')
		THEN (SELECT DeliveryViewId from UserData.DeliveryView WHERE [Name] = 'Atraso Médio')
		--
		WHEN Datediff(day,ActualDueDate,OrderEnd) >= (SELECT QuantidadeDeDias from UserData.DeliveryView WHERE [Name] = 'Atraso Pequeno')
		THEN (SELECT DeliveryViewId from UserData.DeliveryView WHERE [Name] = 'Atraso Pequeno')
		--	
		WHEN Datediff(day,ActualDueDate,OrderEnd) >= (SELECT QuantidadeDeDias from UserData.DeliveryView WHERE [Name] = 'Risco de Atraso')
		THEN (SELECT DeliveryViewId from UserData.DeliveryView WHERE [Name] = 'Risco de Atraso')
		--
		WHEN Datediff(day,ActualDueDate,OrderEnd) >= (SELECT QuantidadeDeDias from UserData.DeliveryView WHERE [Name] = 'No Prazo')
		THEN (SELECT DeliveryViewId from UserData.DeliveryView WHERE [Name] = 'No Prazo')
		--	
		WHEN Datediff(day,ActualDueDate,OrderEnd) >= (SELECT QuantidadeDeDias from UserData.DeliveryView WHERE [Name] = 'Antecipação Pequena')
		THEN (SELECT DeliveryViewId from UserData.DeliveryView WHERE [Name] = 'Antecipação Pequena')
		--
		WHEN Datediff(day,ActualDueDate,OrderEnd) >= (SELECT QuantidadeDeDias from UserData.DeliveryView WHERE [Name] = 'Antecipação Média')
		THEN (SELECT DeliveryViewId from UserData.DeliveryView WHERE [Name] = 'Antecipação Média')
		--
		WHEN Datediff(day,ActualDueDate,OrderEnd) >= (SELECT QuantidadeDeDias from UserData.DeliveryView WHERE [Name] = 'Antecipação Grande')
		THEN (SELECT DeliveryViewId from UserData.DeliveryView WHERE [Name] = 'Antecipação Grande')
	ELSE (SELECT DeliveryViewId FROM UserData.DeliveryView WHERE [Name] = 'Não programada')
	END

COMMIT