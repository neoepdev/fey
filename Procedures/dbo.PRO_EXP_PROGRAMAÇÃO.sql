ALTER procedure dbo.PRO_EXP_PROGRAMAÇÃO
AS

TRUNCATE TABLE IO.PROGRAMACAO_PRODUCAO

INSERT INTO IO.PROGRAMACAO_PRODUCAO
SELECT OrderNo, OpNo, R.cdigoRecurso, SetupStart, StartTime, EndTime, Quantity, SUM(QPP.Quantidade) AS WeekQuantity
FROM userdata.orders O
INNER JOIN UserData.Orders_Dataset D ON O.DatasetId = D.DatasetId AND D.name = 'Atual'
INNER JOIN UserData.Resources R ON O.Resource = R.ResourcesId
LEFT JOIN [dbo].[QuantidadePorPeriodo] QPP ON QPP.Operacao = O.OrdersId
AND DATEADD(day,-1-(DATEPART(dw,QPP.Inicio)+@@DATEFIRST-2)%7,CAST(QPP.Inicio AS DATE)) = DATEADD(day,-1-(DATEPART(dw,O.SetupStart)+@@DATEFIRST-2)%7,CAST(O.SetupStart as DATE))
GROUP BY OrderNo,OpNo,CdigoRecurso,SetupStart,StartTime,EndTime,Quantity
order by orderno
