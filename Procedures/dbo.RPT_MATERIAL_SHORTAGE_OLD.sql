ALTER PROCEDURE dbo.RPT_MATERIAL_SHORTAGE_OLD
(
@Dataset				VARCHAR(200),
@PartNoPlanner			VARCHAR(MAX),
@PartNoProductType		VARCHAR(MAX),
@Ordem					VARCHAR(MAX),
@ProgramacaoAte			DATETIME
)

AS

-- ///// Carrega Links de Reservas de Materiais ///
if object_id ('tempdb..#Orders_Link') is not null DROP TABLE #Orders_Link
SELECT   
*
INTO #Orders_Link
FROM VIW_ORDERS_LINK
WHERE 
Dataset = @Dataset
AND
(OrderStartTime <= @ProgramacaoAte OR OrderStartTime IS NULL or @ProgramacaoAte IS NULL)


-- ///// Identifica Resumo de Faltas por Ordem ////
if object_id ('tempdb..#Shortages') is not null DROP TABLE #Shortages
SELECT
Dataset,
OrderNo,
RequiredPartNo,
MAX(REQUIREDQTY) AS TotalRequiredQty,
MAX(REQUIREDQTY) - SUM(LINKEDQTY) AS TotalShortage
INTO #Shortages
FROM
#Orders_Link
WHERE
MaterialShortage = 1
GROUP BY
Dataset,
OrderNo,
RequiredPartNo


-- ///// Carga geral dos dados para relatório /////
SELECT
L.Dataset,
L.OrderNo,
L.PartNo,
L.PartNoDesc,
L.PartNoPlanner,
L.PartNoProductType,
L.OrderQty,
L.OrderStartTime,
L.RequiredPartNo,
L.RequiredPartDescription,
L.RequiredProp,
L.LinkedQty,
L.MaterialShortage,
L.SupplyOrderNo,
L.SupplyDate,
L.Company,
TMP.TotalRequiredQty,
TMP.TotalShortage
FROM
#Orders_Link L
INNER JOIN
#Shortages TMP
ON  L.DATASET = TMP.DATASET AND L.ORDERNO = TMP.ORDERNO AND L.REQUIREDPARTNO = TMP.REQUIREDPARTNO
WHERE 
MaterialShortage = 1
AND
TMP.TotalShortage > 0
AND
(
L.PartNoPlanner IN (select val from FUN_String_To_Table (@PartNoPlanner, ',', 1)) or @PartNoPlanner = 'Todos'
)
AND
(
L.PartNoProductType IN (select val from FUN_String_To_Table (@PartNoProductType, ',', 1)) or @PartNoProductType = 'Todos'
)
AND
(
L.OrderNo IN (select val from FUN_String_To_Table (@Ordem , ',', 1)) or @Ordem = 'Todos'
)






