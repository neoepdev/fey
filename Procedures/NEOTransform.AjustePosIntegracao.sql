ALTER PROCEDURE NEOTransform.AjustePosIntegracao

AS

UPDATE O
SET O.FolgaMnimaApsOperao = NULL
,EarliestStartDate = NULL
,ActualEarliestStartDate = NULL
,LockOperation = 0
FROM UserData.Orders O
WHERE DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')
AND OperationName = ('Conformar Quente')

IF OBJECT_ID ('tempdb..#TEMP_0') IS NOT NULL DROP TABLE #TEMP_0
SELECT * 
INTO #TEMP_0	
FROM UserData.Orders O
WHERE DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')
AND OperationName = ('Conformar Quente')


UPDATE O
SET O.FolgaMnimaApsOperao = 1
--SELECT OPERATIONNAME,*
FROM UserData.Orders O
WHERE DatasetId = 
(SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')
AND BelongsToOrderNo IN (SELECT OrdersId FROM #TEMP_0)
and OperationName = 'Jatear'

--######################################################################################################
--######################################################################################################
--######################################################################################################
--######################################################################################################
IF OBJECT_ID ('tempdb..#TEMP_OPERACOES') IS NOT NULL DROP TABLE #TEMP_OPERACOES
SELECT 
*
INTO #TEMP_OPERACOES
--SELECT *
FROM UserData.Attribute4
WHERE FolgaMnimaApsOperao IS NOT NULL
AND Description <> ('Conformar Quente')

--SELECT * FROM #TEMP_OPERACOES

--######################################################################################################
IF OBJECT_ID ('tempdb..#ORDENS') IS NOT NULL DROP TABLE #ORDENS
SELECT
	OrdersId
	,OrderNo
	,OpNo
	,OperationName
	,TOPER.FolgaMnimaApsOperao
INTO #ORDENS
FROM UserData.Orders O
	INNER JOIN #TEMP_OPERACOES TOPER
		ON O.OperationName = TOPER.Description
WHERE DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')

--######################################################################################################
IF OBJECT_ID ('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP
SELECT
	O.DatasetId
	,O.OrderNo
	,O.OpNo
	,O.OperationName
	,A4.Name
	,A4.FolgaMnimaApsOperao
	,DENSE_RANK () OVER (PARTITION BY O.OrderNo ORDER BY O.OpNo) as DNS_RNK
INTO #TEMP
FROM UserData.Orders O 
LEFT JOIN UserData.Attribute4 A4
	ON O.Attribute4 = A4.Attribute4Id
WHERE O.DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')
--AND O.OrderNo = '85751-16'
ORDER BY O.OrderNo, O.OpNo

--SELECT * FROM #TEMP
--######################################################################################################

IF OBJECT_ID ('tempdb..#ATUALIZACAO_ORDENS') IS NOT NULL DROP TABLE #ATUALIZACAO_ORDENS
SELECT 
	--*,
	T2.OrderNo
	,T2.OpNo
	,
	CASE 
		WHEN T.FolgaMnimaApsOperao IS NOT NULL
		THEN T.FolgaMnimaApsOperao
		ELSE NULL
	END AS FolgaMinAtualizada
INTO #ATUALIZACAO_ORDENS
FROM #TEMP T
	LEFT JOIN #TEMP T2
		ON T.OrderNo = T2.OrderNo
		AND T.DNS_RNK  + 1 = T2.DNS_RNK 


--SELECT * FROM #ATUALIZACAO_ORDENS

UPDATE O
SET 
	O.FolgaMnimaApsOperao = NULL
	,EarliestStartDate = NULL
	,ActualEarliestStartDate = NULL
	,LockOperation = 0
FROM UserData.Orders O
WHERE DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')
AND OrdersId IN (SELECT OrdersId FROM #ORDENS)


--######################################################################################################

UPDATE O
SET O.FolgaMnimaApsOperao = PA.FolgaMinAtualizada
--SELECT PA.*,*
FROM UserData.Orders O
	INNER JOIN #ATUALIZACAO_ORDENS PA
		ON O.OrderNo = PA.OrderNo
		AND O.OpNo = PA.OpNo
WHERE DatasetId = (SELECT DatasetId FROM UserData.Orders_Dataset WHERE NAME = 'Atual')