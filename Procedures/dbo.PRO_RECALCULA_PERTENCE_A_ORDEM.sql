ALTER PROCEDURE dbo.PRO_RECALCULA_PERTENCE_A_ORDEM

AS

-- LIMPA PAIS
update userdata.Orders set BelongsToOrderNo = OrdersId


-- REDEFINE O REGISTRO PAI
UPDATE O
SET O.BelongsToOrderNo = '-1'
FROM Userdata.Orders O
INNER JOIN Userdata.Orders_Dataset OD ON O.DatasetId = OD.DatasetId --AND OD.name = 'Atual'
WHERE CAST(o.DatasetId AS VARCHAR)+'.'+CAST(o.OrdersId AS VARCHAR) IN
	(
	SELECT CAST(a.DatasetId AS VARCHAR)+'.'+CAST(OrdersId AS VARCHAR) FROM
			(
			SELECT DISTINCT 
				o.DATASETID, OrderNo, 
				MIN (OpNo)AS Operacao 
			FROM UserData.Orders o
			INNER JOIN Userdata.Orders_Dataset OD ON o.DatasetId = OD.DatasetId --AND OD.name = 'Atual'
			GROUP BY o.DATASETID, OrderNo
			)A
	INNER JOIN UserData.Orders B ON A.OrderNo = B.OrderNo AND A.operacao = B.Opno
	)


--ATUALIZA FILHOS
update UserData.Orders set BelongsToOrderNo = A.ordersid
from UserData.Orders O
inner join
	(
	select o.datasetid, ordersid, OrderNo, BelongsToOrderNo 
	from userdata.orders O
	INNER JOIN Userdata.Orders_Dataset D ON o.DatasetId = D.DatasetId --AND D.name = 'Atual'
	where BelongsToOrderNo = -1
	)A on o.datasetid = a.DatasetId and o.OrderNo = a.OrderNo
where o.BelongsToOrderNo <> -1