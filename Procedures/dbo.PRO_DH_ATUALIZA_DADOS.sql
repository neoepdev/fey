ALTER PROCEDURE dbo.PRO_DH_ATUALIZA_DADOS
AS

select 
	o.IDRegistro,
	orderno,
	S.OrderStatusName as orderstatus,
	null as [ORDEM_FINALIZADA],
	opno,
	duedate,
	quantity,
	setupstart,
	starttime,
	endtime,
	r.name as Recurso,
	null as [QUANTIDADE_FINAL],
	null as [SETUP_START_FINAL],	
	null as [START_TIME_FINAL],
	null as [END_TIME_FINAL],	
	null as [RECURSO_FINAL]
from userdata.orders O
inner join userdata.orders_dataset D on O.datasetid = d.datasetid AND D.name = 'Atual'
inner join userdata.orderstatus S on o.OrderStatus = S.OrderStatusId
left join userdata.resources R on O.resource = R.resourcesid

