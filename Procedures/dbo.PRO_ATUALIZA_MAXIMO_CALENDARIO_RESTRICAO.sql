ALTER PROCEDURE dbo.PRO_ATUALIZA_MAXIMO_CALENDARIO_RESTRICAO 

AS

UPDATE Calendar.SecondaryCalendarPeriods
SET MaxValue = RS.MaxValue
FROM Calendar.SecondaryCalendarPeriods SCP
INNER JOIN Calendar.SecondaryResources SR ON SCP.ResourceId = SR.Id 
INNER JOIN UserData.SecondaryConstraints RS ON SR.[Name] = RS.Name
WHERE SCP.IsException = 0
AND SCP.TemplateId IS NULL
AND RS.MaxValue <> -1
