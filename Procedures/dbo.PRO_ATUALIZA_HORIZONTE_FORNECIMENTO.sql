ALTER PROCEDURE dbo.PRO_ATUALIZA_HORIZONTE_FORNECIMENTO AS

-- ATUALIZA COMPRAS PARA LIMITES DENTRO DO HORIZONTE
UPDATE USERDATA.SUPPLY
SET SUPPLYDATE = 1 + GETDATE() - (SELECT MAX(HistoricalPlanningHorizonDays) FROM UserData.SequencerConfiguration)
WHERE SUPPLYDATE < GETDATE() - (SELECT MAX(HistoricalPlanningHorizonDays) FROM UserData.SequencerConfiguration)

UPDATE USERDATA.SUPPLY
SET SUPPLYDATE =  GETDATE() + (SELECT MAX(FuturePlanningHorizonDays) FROM UserData.SequencerConfiguration)
WHERE SUPPLYDATE > GETDATE() + (SELECT MAX(FuturePlanningHorizonDays) FROM UserData.SequencerConfiguration)
