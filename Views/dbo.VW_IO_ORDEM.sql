ALTER VIEW dbo.VW_IO_ORDEM
AS

SELECT DISTINCT TOP 100 percent
	CAST(ORDEM_PRODUCAO AS VARCHAR) + '.' + CAST(SEQUENCIA_OPERACAO AS VARCHAR) AS id_registro
	, ORDEM_PRODUCAO AS pertence_ordem
	, ORDEM_PRODUCAO AS ordem
	, ORDEM_PRODUCAO AS bom_id
	, SEQUENCIA_OPERACAO AS operacao_seq
	, CODIGO_PRODUTO AS item_codigo
	, DESCRICAO_PRODUTO AS item_descricao
	, DATA_ENTREGA AS data_entrega
	, DATA_CADASTRO as data_cadastro
	, CAST(DATEPART(Yy, Data_Entrega)AS VARCHAR)+RIGHT('00' + CAST(DATEPART(week, Data_Entrega)AS VARCHAR(2)) ,2) AS semanaano
    , case 
		when cast((cast(DATA_CADASTRO AS DATETIME)+p.PADRODEDIMC)as varchar) is null
		then 'N�o Especificado'
		WHEN ISNULL(ORDEM_SERVICO,0) <> 0 
		then 'N�o Especificado'
		else convert(varchar, DATA_CADASTRO + p.PADRODEDIMC, 105)
	  end
    AS dimc
		--, CENTRO_CUSTO AS workcenter
	, o.CODIGO_OPERACAO
	, o.CODIGO_OPERACAO as codigo_operacao_setup
	, DESCRICAO_OPERACAO AS operacao_descricao
	, CASE WHEN TEMPO_PADRAO_PROCESSO = -1
		   THEN
				CAST(ORDEM_PRODUCAO AS VARCHAR) + '.' +cast(CODIGO_PRODUTO AS VARCHAR) + '.' + cast(O.codigo_operacao AS VARCHAR) 
		   ELSE CAST(ORDEM_PRODUCAO AS VARCHAR) + '.' + CAST(RECURSO as varchar)
	  END AS grupo_recurso
	, QUANTIDADE_PCS
	, CASE 
		WHEN a.unidadeMedida = 'KG' then cast(QUANTIDADE_KG AS varchar)
		WHEN a.unidadeMedida = 'PC' then cast(QUANTIDADE_PCS as varchar)
		ELSE cast(QUANTIDADE_PCS as varchar)
	  END AS quantidade_original
	, isnull(o.QUANTIDADE_PCS, QUANTIDADE_KG) as quantidade_valida --Ajustado para normalizar por pe�as em conjunto com o tempo no grupo
	, cast(QUANTIDADE_KG AS varchar) as QUANTIDADE_KG
	, a.unidadeMedida as unidade_medida
	, CASE	WHEN TEMPO_PADRAO_PROCESSO = -1 THEN cast('1.000000' as float) --Tempo por rec especifico
			WHEN TEMPO_PADRAO_PROCESSO <> -1 and QUANTIDADE_PADRAO_PROCESSO = 0 THEN isnull(o.QUANTIDADE_PCS, QUANTIDADE_KG) --'0' --Terceiros com tempo na ordem
			WHEN TEMPO_PADRAO_PROCESSO <> -1 and a.unidadeMedida = 'PC' then cast(QUANTIDADE_PADRAO_PROCESSO as varchar)
			when TEMPO_PADRAO_PROCESSO <> -1 and a.unidadeMedida = 'KG' then cast(QUANTIDADE_PADRAO_PROCESSO*(isnull(o.QUANTIDADE_PCS, QUANTIDADE_KG)/o.QUANTIDADE_KG) AS VARCHAR)
	  END AS quantidade_basica
	, CASE WHEN TEMPO_PADRAO_PROCESSO <> -1 --and a.unidadeMedida = 'PC'
			THEN CAST((TEMPO_PADRAO_PROCESSO / 1440)AS VARCHAR)
		   ELSE '-1'
	  END AS tempo_padrao
	, LINHA AS tipo_produto -- MP, PO OU PF
	, P.Description AS tipo_produto_descricao
	, CURVA AS curva_abc
	, L.rank as curva_abc_rank
	, BITOLA
	, ABERTURA
	, CURVATURA
	, G.GRUPOBITOLA
	, ESPECIE
	, ROSCA
	, PASSO_ROSCA
	, CLASSE_RESITENCIA
	, REVESTIMENTO
	, TIPO_ROSCA
	, E.CODIGO_FILHO AS arame
	, CASE WHEN TEMPERADO = 'S' THEN 'True'
		   WHEN TEMPERADO = 'N' THEN 'False'
		   ELSE NULL
	  END AS temperado
	, COMPRIMENTO
	, AA.NAME AS comprimento_rank 
	, RECEITA
	, TIPO_MATERIAL
	, ORIGEM_PREFERENCIAL
	, LISTA_PEDIDOS
	, LISTA_CLIENTES
	, CASE WHEN ISNULL(ORDEM_SERVICO,0) <> 0 
			THEN 'True' 
			ELSE 'False' 
	 END AS possui_os
	,CASE WHEN A.folgamnimaapsoperao IS NOT NULL
		THEN NULL
		ELSE QUANTIDADE_POR_CAIXA 
	 END AS transferencia
	,A.folgamnimaapsoperao as folga
	,ISNULL(B.possui_alternativo,'Sem Recursos Alternativos') AS possui_alternativo
    ,cast(rtrim(ltrim(O.ATRIBUTO_MATRIZ_SETUP)) as varchar) AS matriz1
	,cast(rtrim(ltrim(O.ATRIBUTO_MATRIZ_SETUP)) as varchar) AS matriz2
	,cast(rtrim(ltrim(O.ATRIBUTO_MATRIZ_SETUP)) as varchar) AS matriz3
	,cast(rtrim(ltrim(O.ATRIBUTO_MATRIZ_SETUP)) as varchar) AS matriz4
	,cast(rtrim(ltrim(O.ATRIBUTO_MATRIZ_SETUP)) as varchar) AS matriz5
	,chave
	,bitola_entrada
	,A17.name as bobina
	,A2.Rank as especie_rank
	,A20.NAME AS att_tipo_kit
FROM IO.ORDEM_PRODUCAO O
LEFT JOIN UserData.Attribute4 A ON O.CODIGO_OPERACAO = A.Name
LEFT JOIN USERDATA.PLANNER L ON  o.curva = L.Name
LEFT JOIN UserData.Attribute14 AA on O.COMPRIMENTO = AA.NAME
LEFT JOIN UserData.Attribute16 A16 on O.bitola_entrada = A16.NAME
LEFT JOIN UserData.Attribute17 A17 on A17.attribute17id = A16.bobina
LEFT JOIN Userdata.Attribute2 A2 ON especie = A2.name
LEFT JOIN UserData.ATTRIBUTE20 A20 ON A20.Name = O.TIPO_KIT
LEFT JOIN UserData.ProductType P ON O.LINHA = P.Name
LEFT JOIN
	(SELECT 
		Grupo_Recurso, 
		CASE WHEN cast(count(Recurso) as varchar) = '1'
		THEN 'Sem Recursos Alternativos'
		ELSE cast(count(Recurso) as varchar)+' Recursos Alternativos'
		end as possui_alternativo
		FROM 
			(
			select DISTINCT RG.Name AS Grupo_Recurso, RGR.Resources as Recurso
			from UserData.Orders O 
			INNER JOIN UserData.ResourceGroup RG ON O.ResourceGroup = RG.ResourceGroupId
			INNER JOIN UserData.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
			)A
	GROUP BY Grupo_Recurso
	)B
ON cast(O.CODIGO_PRODUTO AS VARCHAR)+'.'+cast(O.codigo_operacao AS VARCHAR) = B.Grupo_Recurso AND O.TEMPO_PADRAO_PROCESSO = -1
LEFT JOIN USERDATA.Attribute1 G ON O.BITOLA = G.Name
LEFT JOIN IO.ESTRUTURA E ON o.ORDEM_PRODUCAO = E.ORDEM_ITEM_PAI AND left(codigo_filho,2) in ('AR','LR')

WHERE data_entrega IS NOT NULL AND data_entrega > GETDATE()-365 

ORDER BY ORDEM_PRODUCAO, SEQUENCIA_OPERACAO





































GO


