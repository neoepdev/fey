ALTER VIEW dbo.VIW_ORDERS_SUMARY
AS
SELECT     
O.Dataset,
O.OrderNo,
O.PartNo,
O.Product,
O.ProductTypeName,
ISNULL(O.Planner,'N�o Especificado') AS Planner,
MIN(EarliestStartDate) as EarliestStartDate, 
MAX(DueDate) as DueDate, 
MIN(O.SetupStart) as OrderStart, 
MAX(O.EndTime) as OrderEnd, 
SUM(O.TotalSetupTime)*24 as TotalSetupTimeHs, 
SUM(O.TotalProcessTime)*24 as TotalProcessTimeHs, 
MAX(O.Quantity) as Quantity, 
MAX(O.OrderValue) AS OrderValue,
MAX(O.OrderCost) AS OrderCost,
--C�LCULO KPI DELAY
CAST(MAX(O.DueDate) - MAX(O.EndTime) AS REAL) AS CalcDelayDays,

--C�LCULO KPI LEADTIME
CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL) AS CalcLeadtimeDays,

--C�LCULO KPI AGREGA��O DE VALOR
CASE 
	WHEN CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL ) > 0
	THEN
		100*(CAST(SUM(O.TotalProcessTime) AS REAL)/ CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL )) 
	ELSE
		0
END AS CalcAgregPercent,

--C�LCULO KPI PARTICIPACAO DE SETUP
CASE
	WHEN SUM(O.TotalProcessTime) + SUM(O.TotalSetupTime) > 0
	THEN
		100*(SUM(O.TotalSetupTime)/(SUM(O.TotalProcessTime) + SUM(O.TotalSetupTime)))
END AS CalcSetupPerc,
       CASE  
             WHEN   
					CAST(max(O.DueDate) - max(O.EndTime) AS REAL) < 0 
             THEN 0
             WHEN   
					CAST(max(O.DueDate) - max(O.EndTime) AS REAL) < 0 
             THEN 0
             WHEN   
					CAST(max(O.DueDate) - max(O.EndTime) AS REAL) > 0
             THEN 1
         ELSE ''
      END AS LateOrder,
MAX(DemandDate) AS DemandDate,
MAX(DemandDelay) AS DemandDelay,
MAX(SupplyDate) AS SupplyDate,
MAX(SupplyDelay) AS SupplyDelay
FROM VIW_ORDERS_DATA O

WHERE 
O.EndTime IS NOT NULL
GROUP BY
O.Dataset,
O.OrderNo,
O.Product,
O.PartNo,
O.Planner,
ProductTypeName







GO


