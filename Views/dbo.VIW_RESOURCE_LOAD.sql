ALTER VIEW dbo.VIW_RESOURCE_LOAD
AS
SELECT 
	OD.Name AS Dataset,
	SA.Name as ResourceName,
	W.Name as WorkcenterName,
	SA.ReferenceDate, 
	CONVERT(varchar(10),SA.ReferenceDate,103) as ReferenceDay, 
	dbo.FUN_DATE_TO_WEEK_STR(ReferenceDate) as ReferenceWeek,
	dbo.FUN_DATE_TO_MONTH_STR(ReferenceDate) as ReferenceMonth,
	24*(100 - Unavailable)/100 AS TotalAvailableHs,
	24*(100 - Idle - Unavailable)/100 AS TotalLoadHs,
	24*(Setup)/100 AS SetupLoadHs,
	24*(100 - Idle - Setup - Unavailable)/100 AS OperationLoadHs,
	CASE WHEN Unavailable = 100 
		 THEN 100
		--else Utilization 
		ELSE (working+setup)/(working+setup+idle)*100
	END AS OcupationPerc,
	
	case when Unavailable = 100 
		then 1
		else ((working+setup)/(working+setup+idle))
		--Utilization/100 
	end AS Ocupation
	--(100 - Idle) AS OcupationPerc,
	--(100 - Idle)/100 AS Ocupation
FROM ScheduleAnalysis.PrimaryResourceDailyReport SA
	INNER JOIN UserData.Resources RE
		ON SA.Name = RE.Name
	LEFT JOIN Userdata.Workcenter W
		ON RE.Workcenter = W.WorkcenterId
	INNER JOIN Userdata.Orders_Dataset OD
		ON SA.DatasetId = OD.DatasetId






GO


