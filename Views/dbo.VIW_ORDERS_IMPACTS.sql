ALTER VIEW dbo.VIW_ORDERS_IMPACTS
AS


SELECT     
	D.Name AS Dataset,
	O.orderno AS Ordem,
	MAX(O.PartNo) AS Produto,
	MAX(O.Product) AS Descricao,
	ISNULL(PL.Name,'Sem Curva') AS TipoProduto,    
	CONVERT(varchar(10),MIN(O.EarliestStartDate),103) as Data_Inicio, 
	CONVERT(varchar(10),MAX(O.DueDate),103) as Data_Entrega, 
	CONVERT(varchar(10),MIN(O.OrderStart),103) as Inicio_Ordem, 
	CONVERT(varchar(10),MAX(O.EndTime),103) as Fim_Ordem, 
	SUM(O.TotalSetupTime)*24 as HorasSetup, 
	SUM(O.TotalProcessTime)*24 as HorasProdu��o, 
	MAX(O.Quantity) as Qnt_Pecas, 

--C�LCULO KPI DELAY
	CAST(MAX(O.DueDate) - MAX(O.EndTime) AS REAL) AS CalcDelayDias,

--C�LCULO KPI LEADTIME
	CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL) AS CalcLeadtimeDias,

--C�LCULO KPI AGREGA��O DE VALOR
CASE 
	WHEN CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL ) > 0
	THEN
		100*(CAST(SUM(O.TotalProcessTime) AS REAL)/ CAST(MAX(O.EndTime)- MIN(O.SetupStart) AS REAL )) 
	ELSE
		0
END AS CalcAgregacaoPercentual,

--C�LCULO KPI PARTICIPACAO DE SETUP
CASE
	WHEN SUM(O.TotalProcessTime) + SUM(O.TotalSetupTime) > 0
	THEN
		100*(SUM(O.TotalSetupTime)/(SUM(O.TotalProcessTime) + SUM(O.TotalSetupTime)))
END AS CalcSetupPercentual,

--C�LCULO KPI SITUACAO ENTREGA
       CASE  
             WHEN   
					CAST(MAX(O.DueDate) - MAX(O.EndTime) AS REAL) < 0 
             THEN 'Atrasada'
--             WHEN   
--					CAST(max(O.DueDate) - max(O.EndTime) AS REAL) < 0 
--             THEN 'Em Risco'
             WHEN   
					CAST(max(O.DueDate) - max(O.EndTime) AS REAL) > 0
             THEN 'Em Dia'
         ELSE 'N�o Programada'
      END AS SituacaoEntrega,

--C�LCULO KPI ATRASADA
       CASE  
             WHEN  CAST(max(O.DueDate) - max(O.EndTime) AS REAL) < 0 THEN 0
             WHEN  CAST(max(O.DueDate) - max(O.EndTime) AS REAL) > 0 THEN 1
         ELSE ''
      END AS OrdemAtrasada

FROM USERDATA.ORDERS O
			INNER JOIN Userdata.Orders_Dataset D
				ON O.DatasetId = D.DatasetId
			LEFT JOIN Userdata.ProductType PT
				ON O.ProductType = PT.ProductTypeId
			LEFT JOIN USERDATA.PLANNER PL ON O.PLANNER = PL.PLANNERID
WHERE 
O.EndTime IS NOT NULL


GROUP BY
D.Name,
Orderno,
PL.Name




GO


