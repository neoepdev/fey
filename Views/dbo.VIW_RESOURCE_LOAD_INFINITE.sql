ALTER VIEW dbo.VIW_RESOURCE_LOAD_INFINITE
AS
SELECT 
OD.Dataset, 
OD.ResourceName, 
OD.WorkcenterName, 
OD.DeliveryMonth,
TotalLoadHs,
TotalAvailableHs,
CASE 
	WHEN TotalAvailableHs > 0
	THEN 100*(TotalLoadHs / TotalAvailableHs)
	ELSE 0
END AS OcupationPerc
FROM
(
SELECT 
Dataset, 
ResourceName, 
WorkcenterName, 
DeliveryMonth, 
24*SUM(TotalProcessTime + TotalSetupTime) AS TotalLoadHs
FROM VIW_ORDERS_DATA OD
GROUP BY
Dataset, ResourceName, WorkcenterName, DeliveryMonth
) OD
INNER JOIN
(
SELECT 
Dataset, 
ResourceName, 
WorkcenterName, 
ReferenceMonth, 
SUM(TotalAvailableHs) AS TotalAvailableHs
FROM
VIW_RESOURCE_LOAD RS
GROUP BY Dataset, ResourceName, WorkcenterName, ReferenceMonth
) RS
ON OD.Dataset = RS.Dataset and OD.ResourceName = RS.ResourceName and OD.WorkcenterName = RS.WorkcenterName and OD.DeliveryMonth = RS.ReferenceMonth

GO


