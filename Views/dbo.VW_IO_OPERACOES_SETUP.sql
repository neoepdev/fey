ALTER VIEW dbo.VW_IO_OPERACOES_SETUP
AS

SELECT
	cast(Name as varchar) AS id_registro, 
	cast(Name as varchar) AS name, 
	description as descricao_operacao,
	matrizdesetuprelacionada
FROM UserData.Attribute4
WHERE PossuiMatrizDeSetup = '1'


GO


