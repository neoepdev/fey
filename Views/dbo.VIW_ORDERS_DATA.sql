ALTER VIEW dbo.VIW_ORDERS_DATA AS

SELECT
	D.Name AS Dataset,
	OS.OrderStatusName AS Status,
	O.BelongsToOrderNo as Parent,
	O.OrderNo,
	O.PartNo,
	O.Product,
	PT.Name AS ProductTypeName,
	PT.Description AS ProductTypeDescription,
	PL.Name AS Planner,
	O.EarliestStartDate,
	O.DueDate,
	dbo.FUN_DATE_TO_MONTH_STR(O.DueDate) as DeliveryMonth,
	dbo.FUN_DATE_TO_WEEK_STR(O.DueDate) as DeliveryWeek,
	O.Period,
	O.Quantity,
	O.OriginalQuantity,
	O.KgQuantity,
	O.OpNo,
	O.OperationName AS OperationName,
	O.OrderValue,
	O.OrderCost,
	O.OperationCost,
	W.Name AS WorkcenterName,
	RG.Name AS ResourceGroupName,
	R.Name AS ResourceName,
	O.ResourceNextDesc,
	O.ResourceBeforeDesc,
	O.OrderStart,
	O.OrderEnd,
	dbo.FUN_DATE_TO_MONTH_STR(O.EndTime) as ScheduleMonth,
	dbo.FUN_DATE_TO_WEEK_STR(O.EndTime) as ScheduleWeek,	
	O.SetupStart,
	O.StartTime,
	O.EndTime,
	O.Complete,
	O.TotalSetupTime,
	O.TotalProcessTime,
	O.WaitingTime,
	O.DemandDate as SupplyDate,
	O.DemandDelay as DemandDelay,
	O.SupplyDate as DemandDate,
	O.SupplyDelay as SupplyDelay,
	CASE WHEN O.DueDate IS NULL
	THEN NULL
	ELSE DATEDIFF(d,O.orderend, O.DueDate + 1) 
	END AS Delay,
	CAST(ISNULL(CAST(PA1.Name AS VARCHAR), 'N�o especificado') AS varchar) AS Attribute1Name,
	CAST(ISNULL(CAST(PA2.Name AS VARCHAR), 'N�o especificado') AS varchar) AS Attribute2Name,
	CAST(ISNULL(CAST(PA3.Name AS VARCHAR), 'N�o especificado') AS varchar) AS Attribute3Name,
	CAST(ISNULL(CAST(PA4.Name AS VARCHAR), 'N�o especificado') AS varchar) AS Attribute4Name,
	CAST(ISNULL(CAST(PA5.Name AS VARCHAR), 'N�o especificado') AS varchar) AS Attribute5Name,
	W.WorkcenterAbb
FROM Userdata.Orders O
	INNER JOIN Userdata.Orders_Dataset D
		ON O.DatasetId = D.DatasetId
	INNER JOIN Userdata.ResourceGroup RG
		ON O.ResourceGroup = RG.ResourceGroupId
	INNER JOIN Userdata.Resources R
		ON O.Resource = R.ResourcesId
	LEFT JOIN Userdata.OrderStatus OS
		ON O.OrderStatus = OS.OrderStatusId
	LEFT JOIN Userdata.Workcenter W
		ON R.Workcenter = W.WorkcenterId
	LEFT JOIN Userdata.ProductType PT
		ON O.ProductType = PT.ProductTypeId
	LEFT JOIN Userdata.Planner PL
		ON O.Planner = PL.PlannerId
	LEFT JOIN Userdata.Attribute1 PA1
		ON O.Attribute1 = PA1.Attribute1Id
	LEFT JOIN Userdata.Attribute2 PA2
		ON O.Attribute2 = PA2.Attribute2Id
	LEFT JOIN Userdata.Attribute3 PA3
		ON O.Attribute3 = PA3.Attribute3Id
	LEFT JOIN Userdata.Attribute4 PA4
		ON O.Attribute4 = PA4.Attribute4Id
	LEFT JOIN Userdata.Attribute5 PA5
		ON O.Attribute5 = PA5.Attribute5Id



















GO


