ALTER VIEW dbo.VW_MATRIZ_SETUP1
AS

select distinct top 999999999999 a.id_registro 
from
	(
	select distinct CODIGO_OPERACAO, CONJUNTO_ORIGEM as id_registro from io.CONJUNTO_MONTAGEM
	union
	select distinct CODIGO_OPERACAO, conjunto_destino as id_registro from io.CONJUNTO_MONTAGEM
	)A
inner join UserData.Attribute4 B on a.CODIGO_OPERACAO = B.Name
where b.MatrizDeSetupRelacionada = 'Matriz1'
order by id_registro




GO


