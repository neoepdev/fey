ALTER VIEW dbo.VW_LOG_ERROS AS



SELECT DISTINCT 'Estoques' AS Integracao
	,codigo_material AS ID_Registro
	,codigo_material AS Item
	,'Estoque do Item ' + CAST(codigo_material AS VARCHAR) + ' possui quantidade ' + CAST(quantidade AS VARCHAR) + '.' AS Erro
	,'ERRO - Estoque com quantidade negativa ou igual a zero.' AS TipoErro
FROM (
	SELECT TOP 100 PERCENT *
	FROM IO.ESTOQUE_MP
	WHERE QUANTIDADE <= 0
	ORDER BY QUANTIDADE
	) A


UNION ALL

SELECT DISTINCT 'Recursos' AS Integracao
	,RECURSO AS ID_Registro
	,'NA' AS Item
	,'Recurso ' + CAST(recurso AS VARCHAR) + ' consta na tabela de Grupos de Recursos e inexiste na tabela de Recursos.' AS Erro
	,'ERRO - Recurso consta na tabela de Grupos de Recursos e inexiste na tabela de Recursos.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT gr.RECURSO
	FROM IO.GRUPO_RECURSO gr
	LEFT JOIN IO.recurso r ON gr.recurso = r.RECURSO
	WHERE r.recurso IS NULL
	ORDER BY RECURSO
	) A

UNION ALL

SELECT DISTINCT 'Recursos' AS Integracao
	,RECURSO AS ID_Registro
	,'NA' AS Item
	,'Recurso' + CAST(RECURSO AS VARCHAR) + ' definido com efici�ncia igual a zero.' AS Erro
	,'ERRO - Recurso definido com efici�ncia igual a zero.' AS TipoErro
FROM (
	SELECT TOP 100 PERCENT *
	FROM IO.RECURSO
	WHERE EFICIENCIA = 0
	ORDER BY recurso
	) A

UNION ALL

SELECT DISTINCT 'Ordem Produ��o' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,codigo_produto AS Item
	,'Ordem de Produ��o' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' n�o possui data de entrega.' AS Erro
	,'ERRO - Ordem de Produ��o n�o possui data de entrega.' AS TipoErro
FROM (
	SELECT TOP 100 PERCENT *
	FROM IO.ORDEM_PRODUCAO
	WHERE DATA_ENTREGA IS NULL
	ORDER BY ORDEM_PRODUCAO
		,SEQUENCIA_OPERACAO
	) A

UNION ALL

SELECT DISTINCT 'Ordem Produ��o' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,codigo_produto AS Item
	,'Ordem de Produ��o ' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' n�o possui consumo na tabela Estrutura e n�o � Ordem de Servi�o.' AS Erro
	,'AVISO - Ordem de Produ��o n�o possui consumo na tabela Estrutura e n�o � Ordem de Servi�o.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT O.*
	FROM IO.ORDEM_PRODUCAO O
	WHERE ORDEM_PRODUCAO NOT IN (
			SELECT ORDEM_ITEM_PAI
			FROM IO.ESTRUTURA
			)
		AND ordem_servico = 0
	) A

UNION ALL

SELECT DISTINCT 'Ordem Produ��o' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,codigo_produto AS Item
	,'Ordem de Produ��o' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' sequencia opera��o' + CAST(sequencia_operacao AS VARCHAR) + ' definida como tempo dependente do recurso, no entanto n�o consta na tabela de grupo de recurso.' AS Erro
	,'ERRO - Ordem de Produ��o com tempo por recurso, mas que n�o consta na tabela de grupo de recurso.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT O.*
	FROM IO.ORDEM_PRODUCAO O
	LEFT JOIN IO.GRUPO_RECURSO G ON O.CODIGO_PRODUTO = G.CODIGO_PRODUTO
		AND O.CODIGO_OPERACAO = G.CODIGO_OPERACAO
	WHERE O.TEMPO_PADRAO_PROCESSO = '-1'
		AND G.CODIGO_PRODUTO IS NULL
	ORDER BY ORDEM_PRODUCAO
		,O.CODIGO_OPERACAO
	) A

UNION ALL

SELECT DISTINCT 'Ordem Produ��o' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,codigo_produto AS Item
	,'Ordem de Produ��o' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' sequencia opera��o' + CAST(sequencia_operacao AS VARCHAR) + ' definida com tempo padr�o e quantidade padr�o igual a zero.' AS Erro
	,'ERRO - Ordem de Produ��o com definida com tempo padr�o e quantidade padr�o igual a zero.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT O.*
	FROM IO.ORDEM_PRODUCAO O
	WHERE QUANTIDADE_PADRAO_PROCESSO = 0
		AND TEMPO_PADRAO_PROCESSO = 0
	ORDER BY ORDEM_PRODUCAO
		,O.CODIGO_OPERACAO
	) A

UNION ALL

SELECT DISTINCT 'Ordem Produ��o' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,codigo_produto AS Item
	,'Ordem de Produ��o' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' sequencia opera��o ' + CAST(sequencia_operacao AS VARCHAR) + ' definida para recurso ' + cast(a.recurso AS VARCHAR) + ' que n�o consta na tabela de recursos.' AS Erro
	,'ERRO - Ordem de Produ��o definida com tempo padr�o para recurso que n�o consta na tabela de recursos.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT O.*
	FROM IO.ORDEM_PRODUCAO O
	LEFT JOIN IO.RECURSO R ON o.RECURSO = r.RECURSO
	WHERE o.RECURSO IS NOT NULL
		AND r.RECURSO IS NULL
	ORDER BY ORDEM_PRODUCAO
		,O.CODIGO_OPERACAO
	) A

UNION ALL

SELECT DISTINCT 'Ordem Produ��o' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,codigo_produto AS Item
	,'Ordem de Produ��o' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' possui data de entrega ' + CAST(data_entrega AS VARCHAR) + 'menor que 30 dias no passado' AS Erro
	,'AVISO - Ordem de Produ��o com data de entrega menor que 30 dias no passado.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT O.*
	FROM IO.ORDEM_PRODUCAO O
	WHERE DATA_ENTREGA <= GETDATE() - 30
	ORDER BY ORDEM_PRODUCAO
		,O.CODIGO_OPERACAO
	) A

UNION ALL

SELECT DISTINCT 'Ordem Produ��o' AS Integracao
	,OrderNo AS ID_Registro
	,partno AS Item
	,'Ordem de Produ��o ' + CAST(OrderNo AS VARCHAR) + ' da curva ' + CAST(Curva AS VARCHAR) + ' possui ' + CAST(Delta AS VARCHAR) + ' dias de diferen�a em rela��o a data de entrega ' + CAST(DueDate AS VARCHAR) + ' e a data de in�cio mais cedo ' + cast(earlieststartdate AS VARCHAR) + ' .' AS Erro
	,'ERRO - Ordem de Produ��o com data de entrega menor que 7 dias ap�s data de inicio mais cedo.' AS TipoErro
FROM (
	SELECT TOP 100 PERCENT o.OrderNo
		,o.PartNo
		,o.earlieststartdate
		,o.DueDate
		,datediff(day, o.earlieststartdate, o.duedate) AS Delta
		,P.NAME AS Curva
	FROM userdata.orders O
	INNER JOIN userdata.orders_dataset D ON o.datasetid = d.datasetid
		AND d.NAME = 'Atual'
	LEFT JOIN userdata.planner P ON p.plannerid = o.Planner
	WHERE o.belongstoorderno = '-1'
		AND o.earlieststartdate IS NOT NULL
		AND datediff(day, o.earlieststartdate, o.duedate) < 7
	ORDER BY datediff(day, o.earlieststartdate, o.duedate)
	) A

UNION ALL

SELECT DISTINCT 'Ordem Produ��o' AS Integracao
	,OrderNo AS ID_Registro
	,partno AS Item
	,'Ordem de Produ��o ' + CAST(OrderNo AS VARCHAR) + ' da curva ' + CAST(Curva AS VARCHAR) + ' possui ' + CAST(Delta AS VARCHAR) + ' dias de diferen�a em rela��o a data de entrega ' + CAST(DueDate AS VARCHAR) + ' e a data de in�cio mais cedo ' + cast(earlieststartdate AS VARCHAR) + ' .' AS Erro
	,'ERRO - Ordem de Produ��o com data de entrega maior que 60 dias ap�s data de inicio mais cedo.' AS TipoErro
FROM (
	SELECT TOP 100 PERCENT o.OrderNo
		,o.PartNo
		,o.earlieststartdate
		,o.DueDate
		,datediff(day, o.earlieststartdate, o.duedate) AS Delta
		,P.NAME AS Curva
	FROM userdata.orders O
	INNER JOIN userdata.orders_dataset D ON o.datasetid = d.datasetid
		AND d.NAME = 'Atual'
	LEFT JOIN userdata.planner P ON p.plannerid = o.Planner
	WHERE o.belongstoorderno = '-1'
		AND o.earlieststartdate IS NOT NULL
		AND datediff(day, o.earlieststartdate, o.duedate) > 60
	ORDER BY datediff(day, o.earlieststartdate, o.duedate)
	) A

UNION ALL

SELECT DISTINCT 'Ordem Compra' AS Integracao
	,ordem_compra AS ID_Registro
	,codigo_material AS Item
	,'Ordem de Compra' + CAST(ORDEM_compra AS VARCHAR) + ' possui data de disponibilidade ' + CAST(data_disponibilidade AS VARCHAR) + 'no passado' AS Erro
	,'ERRO - Ordem de Compra com data de disponibilidade no passado.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT *
	FROM IO.ORDEM_COMPRA
	WHERE DATA_DISPONIBILIDADE < CONVERT(DATETIME, CAST(CONVERT(DATE, GETDATE(), 110) AS VARCHAR) + ' 00:00:00.000', 110)
	ORDER BY DATA_DISPONIBILIDADE
	) A

UNION ALL

SELECT DISTINCT 'Grupo Recurso' AS Integracao
	,cast(CODIGO_PRODUTO AS VARCHAR) + '.' + cast(CODIGO_OPERACAO AS VARCHAR) AS ID_Registro
	,Codigo_produto AS Item
	,'Grupo de Recurso ' + cast(CODIGO_PRODUTO AS VARCHAR) + '.' + cast(CODIGO_OPERACAO AS VARCHAR) + ' n�o consta na tabela de Ordens de Produ��o' AS Erro
	,'AVISO - Grupo de Recurso n�o consta na tabela Ordem de Produ��o.' AS TipoErro
FROM (
	SELECT g.*
	FROM IO.GRUPO_RECURSO G
	LEFT JOIN IO.ORDEM_PRODUCAO O ON G.CODIGO_PRODUTO = O.CODIGO_PRODUTO
		AND G.CODIGO_OPERACAO = O.CODIGO_OPERACAO
	WHERE O.CODIGO_PRODUTO IS NULL
	) A

UNION ALL

SELECT DISTINCT 'Grupo Recurso' AS Integracao
	,cast(CODIGO_PRODUTO AS VARCHAR) + '.' + cast(CODIGO_OPERACAO AS VARCHAR) AS ID_Registro
	,Codigo_produto AS Item
	,'Grupo de Recurso ' + cast(CODIGO_PRODUTO AS VARCHAR) + '.' + cast(CODIGO_OPERACAO AS VARCHAR) + ' possui tempo menor ou igual a zero para o recurso' + CAST(recurso AS VARCHAR) + ' .' AS Erro
	,'AVISO - Grupo de Recurso n�o consta na tabela Ordem de Produ��o.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT *
	FROM IO.GRUPO_RECURSO
	WHERE TEMPO_PADRAO_PROCESSO <= 0
	ORDER BY codigo_produto
		,codigo_operacao
	) A

UNION ALL

SELECT DISTINCT 'Grupo Recurso' AS Integracao
	,cast(CODIGO_PRODUTO AS VARCHAR) + '.' + cast(CODIGO_OPERACAO AS VARCHAR) AS ID_Registro
	,Codigo_produto AS Item
	,'Grupo de Recurso ' + cast(CODIGO_PRODUTO AS VARCHAR) + '.' + cast(CODIGO_OPERACAO AS VARCHAR) + ' apresenta diferentes unidades de medida para seus recursos alternativos.' AS Erro
	,'AVISO - Grupo de Recurso apresenta diferentes unidades de medida para seus recursos alternativos.' AS TipoErro
FROM (
	SELECT DISTINCT CODIGO_PRODUTO
		,CODIGO_OPERACAO
	FROM (
		SELECT GR.CODIGO_PRODUTO
			,gr.CODIGO_OPERACAO
			,r.UNIDADE_CALCULO
		FROM IO.GRUPO_RECURSO GR
		INNER JOIN IO.RECURSO R ON GR.RECURSO = R.RECURSO
		GROUP BY GR.CODIGO_PRODUTO
			,gr.CODIGO_OPERACAO
			,r.UNIDADE_CALCULO
		) A
	GROUP BY CODIGO_PRODUTO
		,CODIGO_OPERACAO
	HAVING COUNT(UNIDADE_CALCULO) > 1
	) A

UNION ALL

SELECT DISTINCT 'Apontamentos' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,'NA' AS Item
	,'Ordem de Produ��o ' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' possui apontamento, no entanto, n�o consta na tabela de Ordens de Produ��o' AS Erro
	,'AVISO - Ordem de Produ��o, possui apontamento, no entanto, n�o consta na tabela de Ordens de Produ��o' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT A.*
	FROM IO.APONTAMENTO A
	LEFT JOIN IO.ORDEM_PRODUCAO O ON A.ORDEM_PRODUCAO = O.ORDEM_PRODUCAO
		AND A.SEQUENCIA_OPERACAO = O.SEQUENCIA_OPERACAO
	WHERE O.ORDEM_PRODUCAO IS NULL
	ORDER BY ORDEM_PRODUCAO
		,A.Sequencia_operacao
	) A

UNION ALL

SELECT DISTINCT 'Apontamentos' AS Integracao
	,RECURSO_APONTADO AS ID_Registro
	,'NA' AS Item
	,'Recurso ' + CAST(recurso_apontado AS VARCHAR) + ' possui apontamento, mas n�o conta na tabela Recursos.' AS Erro
	,'ERRO - Recurso possui apontamento, no entanto, n�o consta na tabela de Recursos' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT A.*
	FROM IO.APONTAMENTO A
	LEFT JOIN IO.RECURSO R ON A.RECURSO_APONTADO = R.RECURSO
	WHERE R.RECURSO IS NULL
	ORDER BY DATA_APONTAMENTO
	) A

UNION ALL

SELECT DISTINCT 'Apontamentos' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,'Produto: ' + DESCRICAO_PRODUTO + ' - '  +  'Recurso: ' + RECURSO_APONTADO  + ' - '  + 'Curva ABC: ' + ISNULL(Curva, 'N/A') as Item
	,'Ordem de Produ��o ' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' possui apontamento com data ' + CAST(data_apontamento AS VARCHAR) + ', opera��o sequ�ncia '+CAST(sequencia_operacao AS VARCHAR)+' - '+ CAST(descricao_operacao AS VARCHAR)+'.' AS Erro
	,'AVISO - Opera��o est� sequenciada e possui apontamento com data menor (data mais antiga) que o par�metro de hist�rico definido.' AS TipoErro
FROM (
	SELECT DISTINCT TOP 100 PERCENT 
		A.*,
		O.descricao_operacao
		,O.CODIGO_PRODUTO
		,O.DESCRICAO_PRODUTO
		,P.Name AS Curva
	--SELECT *
	FROM IO.ORDEM_PRODUCAO O
	INNER JOIN UserData.Orders O2 
		ON O.ORDEM_PRODUCAO = O2.OrderNo
		and o2.OpNo = o.SEQUENCIA_OPERACAO
	INNER JOIN USERDATA.Orders_Dataset D
		ON O2.DatasetId = D.DatasetId
	LEFT JOIN UserData.Planner P 
		ON O2.Planner = P.PlannerId
	LEFT JOIN IO.APONTAMENTO A
		on A.ordem_producao = O.ordem_producao
		and A.sequencia_operacao = O.sequencia_operacao
	CROSS JOIN USERDATA.SEQUENCERCONFIGURATION S
	WHERE
	D.NAME = 'Atual'
	AND DATA_APONTAMENTO < GETDATE() - S.RPTLOGVALIDAPONTAMENTOSDIASNOPASSADO
	AND A.ORDEM_PRODUCAO+'.'+A.SEQUENCIA_OPERACAO
		NOT IN
		(
		 SELECT A.ORDEM_PRODUCAO +'.'+ A.SEQUENCIA_OPERACAO
		 FROM IO.APONTAMENTO A
	     CROSS JOIN USERDATA.SEQUENCERCONFIGURATION S
		 WHERE A.DATA_APONTAMENTO > GETDATE() - S.RPTLOGVALIDAPONTAMENTOSDIASNOPASSADO
		 )
	ORDER BY DATA_APONTAMENTO
	) A
	
UNION ALL

SELECT DISTINCT 'Apontamentos' AS Integracao
	,ORDEM_PRODUCAO AS ID_Registro
	,'Produto: ' + CODIGO_PRODUTO + ' - '  +  'Recurso: ' + RECURSO_APONTADO + ' - '  + 'Curva ABC: ' + ISNULL(Curva, 'N/A') AS Item
	,'Ordem de Produ��o ' + CAST(ORDEM_PRODUCAO AS VARCHAR) + ' possui opera��o programada para ' + CAST(data_apontamento AS VARCHAR) + ', opera��o sequ�ncia '+CAST(sequencia_operacao AS VARCHAR)+' - '+ CAST(descricao_operacao AS VARCHAR)+'.' AS Erro
	,'AVISO - Ordem de Produ��o possui apontamentos com data menor (data mais antiga) que o par�metro de hist�rico definido.' AS TipoErro
FROM (
				SELECT 
				 --ORDERSID
				 O.ORDERNO AS ORDEM_PRODUCAO
				 ,O.OPERATIONNAME AS DESCRICAO_OPERACAO
				 ,OPNO AS SEQUENCIA_OPERACAO
				 ,A.DATA_APONTAMENTO
				 ,O.Product AS CODIGO_PRODUTO
				 ,R.Name AS RECURSO_APONTADO
				 ,P.Name AS Curva
				 --SELECT *
				FROM UserData.Orders O
					INNER JOIN 
					  (
						SELECT 
							 ORDEM_PRODUCAO
							 ,MAX(DATA_APONTAMENTO) AS DATA_APONTAMENTO
						 --SELECT *
						FROM IO.APONTAMENTO
						GROUP BY 
					 	ORDEM_PRODUCAO
					 ) A
						ON O.ORDERNO = A.ORDEM_PRODUCAO
					INNER JOIN UserData.Resources R 
						ON R.ResourcesId = O.Resource
					LEFT JOIN UserData.Planner P
						ON P.PlannerId = O.Planner
				WHERE O.BelongsToOrderNo = -1
				AND A.DATA_APONTAMENTO < GETDATE() - (SELECT TOP 1 RPTLogValidApontamentosDiasNoPassado FROM UserData.SequencerConfiguration)
				AND O.StartTime IS NOT NULL
				AND DatasetId = (SELECT DatasetId from UserData.Orders_Dataset where name = 'Atual')

UNION ALL

SELECT 
	O.ORDERNO AS ORDEM_PRODUCAO
	,O.OPERATIONNAME AS DESCRICAO_OPERACAO
	,OPNO AS SEQUENCIA_OPERACAO
	,StartTime
	,O.Product AS CODIGO_PRODUTO
	,R.Name AS RECURSO_APONTADO
	,P.Name AS Curva
FROM UserData.Orders O
INNER JOIN UserData.Resources R 
	ON O.Resource = R.ResourcesId
LEFT JOIN UserData.Planner P
	ON P.PlannerId = O.Planner
WHERE ORDERNO NOT IN (SELECT DISTINCT ORDEM_PRODUCAO FROM IO.APONTAMENTO)
and O.BelongsToOrderNo <> -1
and o.BelongsToOrderNo IN 
	(
	SELECT 
		ORDERSID
	FROM UserData.Orders O
		INNER JOIN IO.APONTAMENTO A 
			ON O.OrderNo = A.ORDEM_PRODUCAO
		AND O.OPNO = A.SEQUENCIA_OPERACAO
	WHERE O.BelongsToOrderNo = -1
	AND A.DATA_APONTAMENTO < GETDATE() - (SELECT TOP 1 RPTLogValidApontamentosDiasNoPassado FROM UserData.SequencerConfiguration)
	AND O.StartTime IS NOT NULL
	AND DatasetId = (SELECT DatasetId from UserData.Orders_Dataset where name = 'Atual')  
	)
AND O.StartTime IS NOT NULL
AND DatasetId = (SELECT DatasetId from UserData.Orders_Dataset where name = 'Atual') 

UNION ALL

	SELECT DISTINCT TOP 100 PERCENT 
		O.ORDEM_PRODUCAO
		,O.DESCRICAO_OPERACAO
		,O.SEQUENCIA_OPERACAO
		,O2.StartTime AS data_apontamento
		,O.DESCRICAO_PRODUTO
		,O.RECURSO AS RECURSO_APONTADO
		,P.Name AS Curva
	--SELECT *
	FROM IO.ORDEM_PRODUCAO O
	INNER JOIN UserData.Orders O2 
		ON O.ORDEM_PRODUCAO = O2.OrderNo
		and o2.OpNo = o.SEQUENCIA_OPERACAO
	INNER JOIN USERDATA.Orders_Dataset D
		ON O2.DatasetId = D.DatasetId
	LEFT JOIN IO.APONTAMENTO A
		on A.ordem_producao = O.ordem_producao
	LEFT JOIN UserData.Planner P
		ON P.PlannerId = O2.Planner
	CROSS JOIN USERDATA.SEQUENCERCONFIGURATION S
	WHERE
	D.NAME = 'Atual'
	AND DATA_APONTAMENTO < GETDATE() - S.RPTLOGVALIDAPONTAMENTOSDIASNOPASSADO
	AND O2.StartTime < GETDATE() - S.RPTLOGVALIDAPONTAMENTOSDIASNOPASSADO
	ORDER BY DATA_APONTAMENTO
	) A2


UNION ALL

SELECT DISTINCT 'Estrutura' AS Integracao
	,ORDEM_ITEM_PAI AS ID_Registro
	,CODIGO_PAI AS Item
	,'Ordem de Produ��o ' + CAST(ORDEM_ITEM_PAI AS VARCHAR) + ' n�o conta na tabela de Ordens de Produ��o.' AS Erro
	,'AVISO - Ordem de Produ��o n�o conta na tabela de Ordens de Produ��o.' AS TipoErro
FROM (
	SELECT E.*
	FROM IO.ESTRUTURA E
	LEFT JOIN IO.ORDEM_PRODUCAO O ON E.ORDEM_ITEM_PAI = O.ORDEM_PRODUCAO
	WHERE O.ORDEM_PRODUCAO IS NULL
	) A

UNION ALL

SELECT DISTINCT 'Estrutura' AS Integracao
	,ORDEM_ITEM_PAI AS ID_Registro
	,CODIGO_PAI AS Item
	,'Ordem de Produ��o ' + CAST(ORDEM_ITEM_PAI AS VARCHAR) + ' possui quantidade menor ou igual a zero em rela��o ao consumo do material ' + CAST(codigo_filho AS VARCHAR) + ' .' AS Erro
	,'AVISO - Ordem de Produ��o possui quantidade menor ou igual a zero em rela��o ao consumo do item filho.' AS TipoErro
FROM (
	SELECT *
	FROM IO.ESTRUTURA E
	WHERE QUANTIDADE <= 0
	) A


UNION ALL

SELECT DISTINCT 'Grupo Recurso' AS Integracao
	,partno AS ID_Registro
	,orderno AS Item
	,'Opera��o n�mero ' + CAST(opno AS VARCHAR) + ' da OP ' + CAST(orderno AS VARCHAR) + ' est� sequenciada no recurso ' + CAST(NAME AS VARCHAR) + '.' AS Erro
	,'ERRO - Recurso sequenciado n�o consta no roteiro de produ��o.' AS TipoErro
--- Opera��o n�mero X da OP Y est� sequenciada no recurso Z e este n�o consta no roteiro de produ��o.
FROM (
	SELECT DISTINCT OrderNo
		,opno
		,O.PARTNO
		,operationname
		,R.NAME
	FROM UserData.Orders O
	INNER JOIN UserData.Orders_Dataset D ON O.DatasetId = D.DatasetId
	INNER JOIN USERDATA.RESOURCES R ON O.RESOURCE = R.RESOURCESID
	WHERE D.NAME = 'Atual'
		AND Resource IS NOT NULL
		
	EXCEPT

	SELECT
		OrderNo 
		,opno
		,PARTNO
		,operationname
		,R.NAME
	FROM UserData.Orders O
	INNER JOIN UserData.Orders_Dataset D ON O.DatasetId = D.DatasetId
	INNER JOIN Userdata.ResourceGroup RG ON RG.ResourceGroupId = O.ResourceGroup
	INNER JOIN Userdata.ResourceGroupResources RGR ON RG.ResourceGroupId = RGR.ResourceGroupId
		AND O.Resource = RGR.Resources
	INNER JOIN UserData.Resources R
		ON R.ResourcesId = RGR.Resources
	WHERE D.NAME = 'Atual'
		AND O.Resource IS NOT NULL
		AND O.RESOURCE = RGR.RESOURCES
		
	) A










GO


