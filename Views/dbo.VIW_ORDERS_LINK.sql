ALTER VIEW dbo.VIW_ORDERS_LINK AS

SELECT
D.name AS Dataset,
--ORDENS
O.OrderNo,
O.PartNo,
O.Product as PartNoDesc,
P.Name as PartNoPlanner,
PT.Name as PartNoProductType,
O.Quantity AS OrderQty,
--FORNECIMENTO
S.OrderNo AS SupplyOrderNo,
S.SupplyDate,
S.Company,
CASE
	WHEN S.OrderType = 0 THEN 'Produ��o'
	WHEN S.OrderType = 1 THEN 'Compra'
	WHEN S.OrderType = 2 THEN 'Venda'
	WHEN S.OrderType = 3 THEN 'Estoque'
	ELSE null
END AS OrderType,	
MAX(S.Quantity) AS SupplyQuantity,
--ESTRUTURA
B.RequiredPartNo,
B.RequiredPartDescription,
B.RequiredQuantity AS RequiredProp,
--ISNULL(B.RequiredQuantity,'0') * O.Quantity AS RequiredQty,
CASE
	WHEN B.MultiplyByOrderQuantity = 1
	THEN ISNULL(B.RequiredQuantity,'0') * O.Quantity
	ELSE ISNULL(B.RequiredQuantity,'0')
END AS RequiredQty,
ISNULL(SUM(L.Quantity),'0') AS LinkedQty, 
CASE
	WHEN B.MultiplyByOrderQuantity = 1
	THEN (ISNULL(B.RequiredQuantity,'0') * O.Quantity) - ISNULL(SUM(L.Quantity),'0') 
	ELSE ISNULL(B.RequiredQuantity,'0') - ISNULL(SUM(L.Quantity),'0') 
END AS ShortageQty,
MIN(O.StartTime) AS OrderStartTime,
O.MaterialShortage
FROM
UserData.Orders_Dataset D
INNER JOIN
UserData.Orders O 
ON  D.datasetId = O.DatasetId 
LEFT OUTER JOIN
UserData.BillOfMaterials B 
ON O.DatasetId = B.DatasetId AND 
O.BoMID= B.BoMID AND
O.OpNo = B.OpNo
LEFT OUTER JOIN
UserData.OrderLinks L 
ON O.DatasetId = L.DatasetId AND
O.OrdersId = L.ToInternalDemandOrder AND 
(B.RequiredPartNo = L.PartNo OR
B.RequiredPartNo IS NULL)
LEFT OUTER JOIN		
UserData.Supply S
ON O.DatasetId = S.DatasetId AND
L.FromExternalSupplyOrder = S.SupplyId
LEFT JOIN
UserData.Planner P
ON P.PlannerId = O.Planner
LEFT JOIN
UserData.ProductType PT
ON PT.ProductTypeId = O.ProductType
WHERE 
B.RequiredPartNo IS NOT NULL
--AND O.OrderNo = '18452-31'
GROUP BY 
D.name,
O.OrderNo, 
O.PartNo, 
O.Product, 
P.Name,
PT.Name,
S.OrderNo,
S.SupplyDate,
S.Company,
S.OrderType,
B.RequiredPartNo, 
B.RequiredPartDescription,
B.RequiredQuantity, 
O.Quantity, 
O.OpNo,
O.MaterialShortage,
B.MultiplyByOrderQuantity
--L.Quantity
HAVING        
(ISNULL(B.RequiredQuantity,0)* O.Quantity) - ISNULL(SUM(L.Quantity),0) > 0
--ORDER BY 
--DATASET, MATERIALSHORTAGE DESC, ORDERNO, REQUIREDPARTNO







GO


