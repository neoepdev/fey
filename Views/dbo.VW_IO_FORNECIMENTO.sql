ALTER VIEW dbo.VW_IO_FORNECIMENTO
AS

SELECT TOP 9999999999
*
FROM
	(
	SELECT DISTINCT
		'E.'+CODIGO_MATERIAL AS id_registro,
		'E.'+CODIGO_MATERIAL AS ordem_num,
		'Estoque' AS tipo,
		'E' AS identificador_suprimento,
		CODIGO_MATERIAL AS item_codigo,
		GETDATE() AS data,
		quantidade,
		descricao_material
	FROM [IO].ESTOQUE_MP
	WHERE QUANTIDADE > 0

	UNION ALL

	SELECT DISTINCT
		'C.'+ORDEM_COMPRA AS id_registro,
		'C.'+ORDEM_COMPRA AS ordem_num,
		'Ordem de Compra' AS tipo,
		'C' AS identificador_suprimento,
		CODIGO_MATERIAL AS item_codigo,
		DATA_DISPONIBILIDADE AS data,
		quantidade,
		descricao_material
	FROM [IO].ORDEM_COMPRA
	WHERE DATA_DISPONIBILIDADE >= GETDATE() AND DATA_DISPONIBILIDADE <= GETDATE()+120
	)A
ORDER BY item_codigo




GO


