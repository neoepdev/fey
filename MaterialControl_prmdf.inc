;
{CONDITIONAL},
   ELSE:pr_mc_end;
;
#label image EDIT_ExternalSupply:{EMBEDDED IMAGE PATH}/Supply.16.png
#label image EDIT_ExternalDemand:{EMBEDDED IMAGE PATH}/Demand.16.png
#label image EDIT_Orders:{EMBEDDED IMAGE PATH}/Resources.16.png
;
;MENU_Orders : {DIALOG},
;   Select Option from Menu,
;   Orders:
;   PUSHBUTTON,Supply Orders,EDIT_ExternalSupply:
;   PUSHBUTTON,Demand Orders,EDIT_ExternalDemand:
;   PUSHBUTTON,Works Orders,EDIT_Orders;
;
#label image EDIT_BillofMaterials:{EMBEDDED IMAGE PATH}/Products.16.png
#label image EDIT_Co-Products:{EMBEDDED IMAGE PATH}/Products.16.png
#label image EDIT_PeggingRules:{EMBEDDED IMAGE PATH}/Pegging.Rules.16.png
#label image EDIT_OrderQtyRules:{EMBEDDED IMAGE PATH}/Pegging.Rules.16.png
;
;   PUSHBUTTON,Co-Products,PMODE=500,EDIT_Co-Products:
MENU_MaterialControl : {DIALOG},
   Select Option from Menu,
   Material Control:
   PUSHBUTTON,Bill of Materials,EDIT_BillofMaterials:
   PUSHBUTTON,Pegging Rules,EDIT_PeggingRules;
;
;   PUSHBUTTON,Order Quantity Rules,EDIT_OrderQtyRules:
;
EDIT_ExternalSupply : PREDIT.EXE,
   "/FMT:Supply" /NDS Atual,
   Edit Supply Orders,
   Preactor Database Editor,
   Foreground,
   ALWAYS:{RETURN};
;
;
EDIT_ExternalDemand : PREDIT.EXE,
   "/FMT:Demand" /NDS Atual,
   Edit Demand Orders,
   Preactor Database Editor,
   Foreground,
   ALWAYS:{RETURN};
;
;
EDIT_BillofMaterials : PREDIT.EXE,
   "/FMT:Bill of Materials" /NDS Atual,
   Edit Bill of Materials,
   Preactor Database Editor,
   Foreground,
   ALWAYS:{RETURN};
;
;
EDIT_Co-Products : PREDIT.EXE,
   "/FMT:Co-products" /NDS Atual,
   Edit Co-Products,
   Preactor Database Editor,
   Foreground,
   ALWAYS:{RETURN};
;
;
EDIT_PeggingRules : PREDIT.EXE,
   "/FMT:Pegging Rules" /NDS /EFC,
   Edit Pegging Rules,
   Preactor Database Editor,
   Foreground,
   ALWAYS:{RETURN};
;
;
EDIT_MaterialControlConfiguration : PREDIT.EXE,
   "/FMT:Material Control Configuration" /NDS /ER:0,
   Edit Material Control Settings,
   Preactor Database Editor,
   Foreground,
   ALWAYS:{RETURN};
;
;
EDIT_OrderQtyRules : PREDIT.EXE,
   "/FMT:PBX Data" /NDS,
   Edit PBX Data,
   Preactor Database Editor,
   Foreground,
   ALWAYS:{RETURN};
;
;
;
pr_mc_end:{CONDITIONAL},
   ELSE:{NEXT};
;
