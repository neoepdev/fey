#VERSION 0
;
PTB POPUP HELP
50110 "Importa dados de recursos, grupos de recursos e restri��es.":
50120 "Importa dados de ordens, apontamentos, estrutura de produtos, estoques e compras.":
50105 "Executa PESP Script personalizado.":
50030 "Abre o Sequenciador do Preactor para visualizar e programar as opera��es atrav�s do gr�fico de Gantt interativo.":
50070 "Abre o menu de cadastro de dados do Preactor":
50100 "Op��es de importa��o e exporta��o de informa��es do Preactor.":
50150 "Exporta Ordens de Produ��o para o sistema ERP.":
53010 "Op��o para criar, editar ou excluir informa��es sobre Produtos.":
53015 "Op��o para criar, editar ou excluir informa��es sobre Tipos de Produtos. Essas informa��es s�o importantes para melhorar a visualiza��o e classifica��o dos Produtos":
53020 "Op��o para criar, editar ou excluir informa��es sobre Grupos Setup. Os Grupos de Setup s�o importantes para determinar Tempos de Setup dependentes da sequ�ncia para cada Recurso":
53030 "Op��o para criar, editar ou excluir informa��es sobre Grupos de Recursos, que define os recursos v�lidos para executar determinada opera��o.":
53040 "Op��o para criar, editar ou excluir informa��es sobre Recursos. Os Recursos Prim�rios podem ser definidos como capacidade Finita ou Infinita.":
53050 "Op��o para criar, editar ou excluir informa��es sobre Recursos Secund�rios, tamb�m chamadas de Restri��es Secund�rias.":
53060 "Op��o para criar, editar ou excluir informa��es sobre Rotas.":
53100 "Op��o para editar par�metros principais do Preactor, tais como o horizonte de programa��o.":
53110 "Op��o para criar, editar ou excluir informa��es sobre Clientes.":
53505 "Op��o para editar os nomes dos Atributos para o usu�rio final.":
53510 "Op��o para criar, editar ou excluir informa��es sobre Atributo 1. Os atributos e seus usos devem ser definidos de acordo com as caracter�sticas dos produtos e processos a serem utilizados.":
53520 "Op��o para criar, editar ou excluir informa��es sobre Atributo 2. Os atributos e seus usos devem ser definidos de acordo com as caracter�sticas dos produtos e processos a serem utilizados.":
53530 "Op��o para criar, editar ou excluir informa��es sobre Atributo 3. Os atributos e seus usos devem ser definidos de acordo com as caracter�sticas dos produtos e processos a serem utilizados.":
53540 "Op��o para criar, editar ou excluir informa��es sobre Atributo 4. Os atributos e seus usos devem ser definidos de acordo com as caracter�sticas dos produtos e processos a serem utilizados.":
53550 "Op��o para criar, editar ou excluir informa��es sobre Atributo 5. Os atributos e seus usos devem ser definidos de acordo com as caracter�sticas dos produtos e processos a serem utilizados.":
58510 "Libera a Programa��o para execu��o.":
58515 "Atualiza a Programa��o de acordo com informa��es de execu��o."
69100 "Op��o para configurar, publicar e visualizar gr�ficos de Gantt para web.":
69110 "Op��es de configura��o do PCO e par�metros de comunica��o.":
69120 "Op��es de configura��o dos scripts PESP, A��es Personalizadas e outros.":
69130 "Op��es de configura��o e execu��o dos scripts de Importa��o e Exporta��o.":
69140 "Op��es de configura��o do m�dulo de An�lise da Programa��o.":
70000 "Abre o link de acesso ao sistema de suporte da Accera.":
70010 "Abre o link de acesso � base de knowledge base da Accera na internet.":
70020 "Acessa a ferramenta de Business Inteligence da Accera com base nas informa��es do Preactor.":
70030 "Abre o guia de utiliza��o do usu�rio em formato PDF.":
70999 "Abre o gerenciador de pacote para gerar um arquivo de backup com arquivos de configura��o, banco de dados e relat�rios.":
;
